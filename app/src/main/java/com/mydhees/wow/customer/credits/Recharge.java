package com.mydhees.wow.customer.credits;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
//import com.google.android.gms.appindexing.Action;
//import com.google.android.gms.appindexing.AppIndex;
//import com.google.android.gms.common.api.GoogleApiClient;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.TransactionSummary;
import com.mydhees.wow.customer.VolleySingleton;
import com.mydhees.wow.customer.Constants;
//import com.mydhees.wow.customer.HotelRegistrationConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.juspay.ec.sdk.api.Environment;
import in.juspay.ec.sdk.checkout.MobileWebCheckout;
import in.juspay.ec.sdk.util.PaymentInstrument;
import in.juspay.juspaysafe.BrowserCallback;

/**
 * Created by jashan on 06-06-2016.
 */
public class Recharge extends AppCompatActivity {
    int flag, status;
    Button buy1, buy2, buy3, pay;
    TextView final_amount;
    CheckBox promo_code;
    EditText points;
    ProgressDialog dialog;
    int purchased_points = 0;
    int promotional_points = 0;
    int coupon = 0;
    int time=0;
    String order_id = "";

    SharedPreferences profile;
    SharedPreferences.Editor editor;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;

    View promo;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
//    private GoogleApiClient client;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
//    private GoogleApiClient client2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_wallet);
        points = (EditText) findViewById(R.id.points);
        buy1 = (Button) findViewById(R.id.one1000);
        buy2 = (Button) findViewById(R.id.a3000);
        buy3 = (Button) findViewById(R.id.a5000);
        final_amount = (TextView) findViewById(R.id.final_amount);
        pay = (Button) findViewById(R.id.recharge);
        promo = (CheckBox) findViewById(R.id.promo_code);
        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);

      //  volleySingleton = VolleySingleton.getinstance();
      //  requestQueue = volleySingleton.getrequestqueue();

        Environment.configure(Environment.SANDBOX, "anshuagrawal2791");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        promo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status == 0) {
                    LinearLayout hiddenLayout = (LinearLayout) findViewById(R.id.layout1);
                    if (hiddenLayout == null) {
                        LinearLayout myLayout = (LinearLayout) findViewById(R.id.promo_view);
                        View hiddenInfo = getLayoutInflater().inflate(R.layout.promocode_layout, myLayout, false);
                        myLayout.addView(hiddenInfo);
                        status = 1; //activated
                    }
                } else {
                    View myView = findViewById(R.id.layout1);
                    ViewGroup parent = (ViewGroup) myView.getParent();
                    parent.removeView(myView);
                    status = 0;

                }
            }
        });


        buy1.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                if (flag == 0) {
                    points.setText("1000");
                    buy1.setBackgroundColor(Color.rgb(139, 195, 74));
                    flag = 1;
                    buy2.setBackgroundColor(Color.RED);
                    buy3.setBackgroundColor(Color.RED);
                } else {
                    points.setText("1000");
                    buy1.setBackgroundColor(Color.rgb(139, 195, 74));

                    buy2.setBackgroundColor(Color.RED);
                    buy3.setBackgroundColor(Color.RED);

                }
            }
        });
        buy2.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                if (flag == 0) {
                    points.setText("3000");
                    buy2.setBackgroundColor(Color.rgb(139, 195, 74));
                    flag = 1;
                    buy1.setBackgroundColor(Color.RED);
                    buy3.setBackgroundColor(Color.RED);
                } else {
                    points.setText("3000");
                    buy2.setBackgroundColor(Color.rgb(139, 195, 74));
                    buy1.setBackgroundColor(Color.RED);
                    buy3.setBackgroundColor(Color.RED);

                }
            }
        });
        buy3.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                if (flag == 0) {
                    points.setText("5000");
                    buy3.setBackgroundColor(Color.rgb(139, 195, 74));
                    flag = 1;
                    buy2.setBackgroundColor(Color.RED);
                    buy1.setBackgroundColor(Color.RED);
                } else {
                    points.setText("5000");
                    buy3.setBackgroundColor(Color.rgb(139, 195, 74));
                    buy2.setBackgroundColor(Color.RED);
                    buy1.setBackgroundColor(Color.RED);

                }
            }
        });


//        pay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    buyPoints();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });


        points.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (points.getText().toString().matches("") || !points.getText().toString().matches("1000") || !points.getText().toString().matches("3000") || !points.getText().toString().matches("5000")) {

                    buy3.setBackgroundColor(Color.RED);
                    buy2.setBackgroundColor(Color.RED);
                    buy1.setBackgroundColor(Color.RED);
                }

                if(!s.toString().matches("")) {
                    purchased_points = Integer.parseInt(points.getText().toString()) + coupon;
                    final_amount.setText(purchased_points + "");
                    pay.setText("Pay Rs. " + points.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }


    private void buyPoints() throws JSONException {
        dialog = new ProgressDialog(Recharge.this);
        dialog.show();
        dialog.setTitle("Please wait...");
        dialog.setMessage("Please wait...");
        editor = profile.edit();
        StringRequest request = new StringRequest(Request.Method.POST, Constants.CREATE_ORDER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        Log.e("response", respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                            order_id = response.getString("order_id");
                            MobileWebCheckout checkout = new MobileWebCheckout(
                                    response.getString("order_id"),
                                    new String[]{"http://www.mydhees.com/.*"},
                                    new PaymentInstrument[]{PaymentInstrument.CARD, PaymentInstrument.WALLET, PaymentInstrument.NB}
                            );

                            checkout.startPayment(Recharge.this, new BrowserCallback() {
                                @Override
                                public void endUrlReached(String s) {
//                                        Log.e("s",s);
                                    if(time==0) {
                                        Uri url = Uri.parse(s);
                                        Log.e("url", url.toString());
                                        if (url.getQueryParameter("status").equals("CHARGED")) {
                                            try {
                                                time++;
                                                dialog.show();
                                                updateCredits();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                Log.e("error", e.toString());
                                            }
                                        }

                                        // Toast.makeText(getApplicationContext(), "done", Toast.LENGTH_LONG).show();
                                    }

                                }

                                @Override
                                public void ontransactionAborted() {
                                    Toast.makeText(getApplicationContext(), "aborted", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(Recharge.this, TransactionSummary.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.putExtra("order_id",order_id);
                                    intent.putExtra("aborted","true");
                                    startActivity(intent);


                                }
                            });


                        } catch (JSONException e) {
                            Log.e("error", e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(Recharge.this, "Error-Internet", Toast.LENGTH_LONG).show();
                Log.e("vollley error", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                if (!points.getText().toString().equals("")) {
                    params.put("customerId", profile.getString(Constants.ID, "default"));
                    params.put("amount", points.getText().toString());
                    params.put(Constants.EMAIL, profile.getString(Constants.EMAIL, "default"));
                    params.put(Constants.PHONE, profile.getString(Constants.PHONE, "default"));
//                    params.put(HotelRegistrationConstants.ADDRESS_LINE1, profile.getString(HotelRegistrationConstants.ADDRESS_LINE1, "default"));
//                    params.put(HotelRegistrationConstants.ADDRESS_LINE2, profile.getString(HotelRegistrationConstants.ADDRESS_LINE2, "default"));
//                    params.put(HotelRegistrationConstants.ADDRESS, profile.getString(HotelRegistrationConstants.ADDRESS, "default"));
//                    params.put(HotelRegistrationConstants.PIN, profile.getString(HotelRegistrationConstants.PIN, "default"));
//                    params.put(Constants.NAME, profile.getString(Constants.NAME, "default"));
//                    params.put("description", "Bought Points-wow wallet");
//
//                    params.put(HotelRegistrationConstants.CITY, profile.getString(HotelRegistrationConstants.CITY, "default"));


                } else {
                    points.setError("Required");

                }
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    private void updateCredits() throws JSONException {
        StringRequest request = new StringRequest(Request.Method.POST, Constants.ADD_CREDITS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        Log.e("response", respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                            if (response.getBoolean("res"))
                            {
//                                mFragmentManager = getSupportFragmentManager();
//                                mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out,R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
//                                mFragmentTransaction.replace(R.id.containerView,new Credits()).commit();
                                Intent intent = new Intent(Recharge.this, TransactionSummary.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("order_id",order_id);
                                intent.putExtra("aborted","false");
                                startActivity(intent);




                            }
                            else
                            {
                                Log.e("response",respons);
                            }

                        } catch (JSONException e) {
                            Log.e("error", e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(Recharge.this, "Error", Toast.LENGTH_LONG).show();
                Log.e("vollley error", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("customerId", profile.getString(Constants.ID, "default"));
                params.put("purchased", purchased_points + "");
                params.put("promotional",promotional_points+"");

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client2.connect();

    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

    }

    //For back button in toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}



