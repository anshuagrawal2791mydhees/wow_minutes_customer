package com.mydhees.wow.customer.myWish;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.customer.Constants;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.VolleySingleton;
import com.mydhees.wow.customer.adapters.PopulatHotelAdapter;
import com.mydhees.wow.customer.objects.popularHotelObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;

public class AllHotelWishList extends AppCompatActivity {

    RecyclerView recycler;
    ArrayList<popularHotelObject> popularh = new ArrayList<>();
    ArrayList<JSONObject> popular_hotel_jsons = new ArrayList<>();
    HashSet<String> popular_hotel_strings = new HashSet<>();
    CardView sortByCard;

    PopulatHotelAdapter myadapter;
    LinearLayoutManager manager;
    Uri uri;
    Button popular,review,mostWishFilled;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_list_hotel);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        volleySingleton = VolleySingleton.getinstance(getApplicationContext());
        requestQueue = volleySingleton.getrequestqueue();

        popular = (Button) findViewById(R.id.popular);
        review = (Button) findViewById(R.id.reviews);
        mostWishFilled = (Button) findViewById(R.id.most_wish);

        popular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popular.setBackgroundColor(Color.rgb(139, 195, 74));
                popular.setTextColor(Color.WHITE);

                mostWishFilled.setBackgroundColor(Color.WHITE);
                mostWishFilled.setTextColor(Color.rgb(139, 195, 74));

                review.setBackgroundColor(Color.WHITE);
                review.setTextColor(Color.rgb(139, 195, 74));

             }

        });

        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                review.setBackgroundColor(Color.rgb(139, 195, 74));
                review.setTextColor(Color.WHITE);

                popular.setBackgroundColor(Color.WHITE);
                popular.setTextColor(Color.rgb(139, 195, 74));

                mostWishFilled.setBackgroundColor(Color.WHITE);
                mostWishFilled.setTextColor(Color.rgb(139, 195, 74));
            }
        });

        mostWishFilled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostWishFilled.setBackgroundColor(Color.rgb(139, 195, 74));
                mostWishFilled.setTextColor(Color.WHITE);

                popular.setBackgroundColor(Color.WHITE);
                popular.setTextColor(Color.rgb(139, 195, 74));

                review.setBackgroundColor(Color.WHITE);
                review.setTextColor(Color.rgb(139, 195, 74));
            }
        });


        myadapter = new PopulatHotelAdapter(this);
        recycler = (RecyclerView) findViewById(R.id.recycler_popular);

        recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recycler.setAdapter(myadapter);
        recycler.setVisibility(View.VISIBLE);
        myadapter.clearAll();
        popular_hotel_jsons.clear();
        popular_hotel_strings.clear();


        try {
            fetchHotels();
        } catch (JSONException e) {
            e.printStackTrace();
        }










    }




    private void fetchHotels() throws JSONException {


        StringRequest request = new StringRequest(Request.Method.POST, Constants.HOTEL_UPDATE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        JSONObject response;
                        try {

                            response = new JSONObject(respons);

                            myadapter.clearAll();
                            if (response.getBoolean("res")) {

                                JSONArray data = response.getJSONArray("hotels");

                                 if (data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject rec = data.getJSONObject(i);
                                       // popularh.add(pop);
                                    }
                                } else {
                                     Toast.makeText(AllHotelWishList.this, "NO offers Released by you.", Toast.LENGTH_SHORT).show();

                                }


                            }

                        }
                        catch (JSONException e) {
                            Log.e("error",e.toString());
                        }

                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(AllHotelWishList.this, "Internet Problem", Toast.LENGTH_LONG).show();
                Log.e("error", error.toString());
            }
        });



    }


    //For back button in Appbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
