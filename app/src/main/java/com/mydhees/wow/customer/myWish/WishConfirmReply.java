package com.mydhees.wow.customer.myWish;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;



import android.content.Intent;
import android.support.v7.app.ActionBar;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.customer.Constants;
import com.mydhees.wow.customer.Home;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.TransactionSummary;
import com.mydhees.wow.customer.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.juspay.ec.sdk.checkout.MobileWebCheckout;
import in.juspay.ec.sdk.util.PaymentInstrument;
import in.juspay.juspaysafe.BrowserCallback;


public class WishConfirmReply extends AppCompatActivity {

    ImageView hotelPic;
    TextView hotelName,address,adults,child,finalPrice,payNowRs;
    EditText checkInDate,checkInTime,checkOutDate,checkOutTime,name,mobile;
    Button viewDetails;
    LinearLayout udLayout;
    Boolean isClick=true;
    LinearLayout nextToPay;
    String wishid,replyid,hotelid,cdate,cin,odate,otime,rooms,hotel,location,adult,child1,price,status;
    int time=0;
    String order_id = "";
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_confirm_reply);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        volleySingleton = VolleySingleton.getinstance(getApplicationContext());
        requestQueue = volleySingleton.getrequestqueue();

        hotelPic = (ImageView)findViewById(R.id.hotel_piccWCR);

        hotelName = (TextView)findViewById(R.id.hotel_nameWCR);
        address = (TextView)findViewById(R.id.addressWCR);
        adults = (TextView)findViewById(R.id.adultWCR);
        child = (TextView)findViewById(R.id.childWCR);
        finalPrice = (TextView)findViewById(R.id.final_priceWRC);
        payNowRs = (TextView)findViewById(R.id.wow_price_conWCR);

        checkInDate = (EditText)findViewById(R.id.checkin_dateWCR);
        checkInTime = (EditText)findViewById(R.id.checkin_timeWCR);
        checkOutDate = (EditText)findViewById(R.id.checkoutdateWRC);
        checkOutTime = (EditText)findViewById(R.id.checkout_timeWCR);
        name = (EditText)findViewById(R.id.name_WCR);
        mobile = (EditText)findViewById(R.id.mobile_WCR);
        nextToPay = (LinearLayout)findViewById(R.id.selectServiceWCR);

        udLayout = (LinearLayout)findViewById(R.id.linearLayoutUDWCR);



        viewDetails = (Button)findViewById(R.id.btn_view_detailsWCR);

        Intent start= getIntent();

        wishid= start.getStringExtra("wishId");
        cdate=start.getStringExtra("checkInDate");
        odate=start.getStringExtra("checkOutDate");
        otime=start.getStringExtra("cOutTime");
        cin=start.getStringExtra("cInTime");
        hotelid=start.getStringExtra("hotelId");
        hotel=start.getStringExtra("hotelName");
        replyid=start.getStringExtra("replyId");
        location=start.getStringExtra("address");
        price= start.getStringExtra("price");
        adult=start.getStringExtra("adults");
        child1=start.getStringExtra("child");
        rooms=start.getStringExtra("room");


        hotelName.setText(hotel);
        address.setText(location);
        adults.setText(adult);
        child.setText(child1);
        finalPrice.setText(price);
        payNowRs.setText(price);
        checkInDate.setText(cdate);
        checkInTime.setText(cin);
        checkOutDate.setText(odate);
        checkOutTime.setText(otime);

        try {
            confirmReply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isClick){
                    udLayout.setVisibility(View.VISIBLE);
                    viewDetails.setVisibility(View.GONE);
                }
                else{
                    udLayout.setVisibility(View.GONE);
                }
            }
        });

     nextToPay.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if(!( mobile.getText().toString().equals("")||  name.getText().toString().equals("")))
        {

            try {
                buywish();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            Toast.makeText(WishConfirmReply.this, "Enter your booking details", Toast.LENGTH_SHORT).show();
        }
    }
});



    }

    private void confirmReply() throws JSONException {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.show();
        dialog.setMessage("Confirming Wish details...");
        StringRequest request = new StringRequest(Request.Method.POST, Constants.CONFIRM_REPLY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                            Log.e("confirm",response.toString());
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),"Internet error",Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("wishId",wishid);
                params.put("replyId",replyid);

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }


    private void buywish() throws JSONException {
        final ProgressDialog dialog11 = new ProgressDialog(WishConfirmReply.this);
        dialog11.show();
        dialog11.setTitle("Please wait...");
        dialog11.setMessage("Please wait...");
        editor = profile.edit();
        StringRequest request = new StringRequest(Request.Method.POST, Constants.PAY_WISH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog11.dismiss();
                        Log.e("response offers", respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                            order_id = response.getString("order_id");
                            MobileWebCheckout checkout = new MobileWebCheckout(
                                    response.getString("order_id"),
                                    new String[]{"http://www.mydhees.com/.*"},
                                    new PaymentInstrument[]{PaymentInstrument.CARD, PaymentInstrument.WALLET, PaymentInstrument.NB}
                            );

                            checkout.startPayment(WishConfirmReply.this, new BrowserCallback() {

                                @Override
                                public void endUrlReached(String s) {
//                                        Log.e("s",s);
//                                    if(time==1) {
                                    //GodelTracker.getInstance().trackPaymentStatus(order_id, GodelTracker.PaymentStatus.SUCCESS);
                                    //getTransactionId

                                    Uri url = Uri.parse(s);
                                    Log.e("url", url.toString());
                                    if (url.getQueryParameter("status").equals("CHARGED")) {
                                        status="Success";


                                        time++;
                                        dialog11.show();


                                    }
                                    else {
                                        Toast.makeText(getApplicationContext(), url.getQueryParameter("status"), Toast.LENGTH_LONG).show();
                                         status="Failed";
                                    }
                                    Intent intent = new Intent(WishConfirmReply.this,TransactionSummary.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.putExtra("order_id",order_id);
                                    intent.putExtra("status",status);
                                    startActivity(intent);

                                }


                                @Override
                                public void ontransactionAborted() {
                                    Toast.makeText(getApplicationContext(), "aborted", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(WishConfirmReply.this, Home.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    //intent.putExtra("order_id",order_id);
                                    intent.putExtra("status","aborted");
                                    startActivity(intent);


                                }
                            });


                        } catch (JSONException e) {
                            Log.e("error", e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog11.dismiss();
                Toast.makeText(WishConfirmReply.this, "Error-Internet", Toast.LENGTH_LONG).show();
                Log.e("vollley error", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customerId", profile.getString(Constants.ID,"default"));
                params.put("wishId",wishid);



                // params.put(Constants.EMAIL, emailUd.getText().toString());
                params.put("bookingContact", mobile.getText().toString());
                params.put("bookingName", name.getText().toString());

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }




    //For back button in Appbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}