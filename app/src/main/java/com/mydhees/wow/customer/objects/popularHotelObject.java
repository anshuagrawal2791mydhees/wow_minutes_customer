package com.mydhees.wow.customer.objects;

import android.media.Image;

/**
 * Created by Abhishek on 16-Aug-16.
 */

public class popularHotelObject {
    String hotelName,address,rating,hotelStar;
    Image hotelImage;

    public popularHotelObject(String hotelName, String address, String rating, String hotelStar) {
        this.hotelName = hotelName;
        this.address = address;
        this.rating = rating;
        this.hotelStar = hotelStar;
    }

    public popularHotelObject(Image hotelImage) {
        this.hotelImage = hotelImage;
    }

    public void setHotelImage(Image hotelImage) {
        this.hotelImage = hotelImage;
    }

    public String getHotelName() {
        return hotelName;
    }

    public String getAddress() {
        return address;
    }

    public String getRating() {
        return rating;
    }

    public String getHotelStar() {
        return hotelStar;
    }
}
