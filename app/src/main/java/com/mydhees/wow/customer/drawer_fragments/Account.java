package com.mydhees.wow.customer.drawer_fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.mydhees.wow.customer.Constants;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.registrationAndLogin.first_screen;


/**
 * A simple {@link Fragment} subclass.
 */
public class Account extends Fragment {

    TextView logout;
    SharedPreferences firstTime;
    SharedPreferences.Editor editor;
    SharedPreferences.Editor editor2;
    SharedPreferences profile;

    public Account() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_account, container, false);
        hideKeyboard(getContext());
        logout = (TextView)v.findViewById(R.id.logout);
        firstTime = getActivity().getSharedPreferences(Constants.LAUNCH_TIME_PREFERENCE_FILE, Context.MODE_PRIVATE);
        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,Context.MODE_PRIVATE);

        editor = firstTime.edit();
        editor2 = profile.edit();
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean(Constants.FIRST_TIME,false);
                editor.commit();
                editor2.clear();
                editor2.commit();
                LoginManager.getInstance().logOut();
                Intent intent = new Intent(getContext(),first_screen.class );
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });




        return v;
    }
    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

}
