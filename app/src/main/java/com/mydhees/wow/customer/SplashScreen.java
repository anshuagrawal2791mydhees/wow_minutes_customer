package com.mydhees.wow.customer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreen extends AppCompatActivity {

  ImageView splash;
//    final SharedPreferences prefs = getApplicationContext().getSharedPreferences(
//            Constants.LAUNCH_TIME_PREFERENCE_FILE, Context.MODE_PRIVATE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
//        getActionBar().hide();
        setContentView(R.layout.activity_splash_screen);


        splash = (ImageView)findViewById(R.id.splash);



        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.welcome_animation);
        splash.setAnimation(animation);


        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                finish();

                final SharedPreferences prefs = getApplicationContext().getSharedPreferences(
                        Constants.LAUNCH_TIME_PREFERENCE_FILE, Context.MODE_PRIVATE);

                if (!prefs.getBoolean(Constants.FIRST_TIME,false)) {


                    // <---- run your one time code here
                    Intent intent = new Intent(SplashScreen.this,Home.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    startActivity(intent);


                }
                else{
                    // <---- run your one time code here
                    Intent intent = new Intent(SplashScreen.this,Home.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    startActivity(intent);

                }

            }


//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intent);



            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });




//        Thread timer = new Thread(){
//            public void run(){
//                try{
//                    sleep(2000);
//                }catch(InterruptedException e){
//                    e.printStackTrace();
//                }finally{
//
//
//                }
////            }
//        };
//        timer.start();

    }

}