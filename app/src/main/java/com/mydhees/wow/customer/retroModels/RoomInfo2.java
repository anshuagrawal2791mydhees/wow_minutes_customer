package com.mydhees.wow.customer.retroModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anshu on 01/07/16.
 */
public class RoomInfo2 {

    @SerializedName("ownerId")
    @Expose
    private String ownerId;
    @SerializedName("facilities")
    @Expose
    private Facilities facilities;
    @SerializedName("priceMRP")
    @Expose
    private Integer priceMRP;
    @SerializedName("hotelName")
    @Expose
    private String hotelName;
    @SerializedName("wowPrice")
    @Expose
    private Integer wowPrice;
    @SerializedName("adults")
    @Expose
    private Integer adults;
    @SerializedName("roomId")
    @Expose
    private String roomId;
    @SerializedName("children")
    @Expose
    private Integer children;
    @SerializedName("noOfRooms")
    @Expose
    private Integer noOfRooms;
    @SerializedName("roomType")
    @Expose
    private String roomType;
    @SerializedName("imagesS3")
    @Expose
    private ImagesS3 imagesS3;
    @SerializedName("ac")
    @Expose
    private Boolean ac;
    @SerializedName("salePrice")
    @Expose
    private Integer salePrice;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;

    /**
     *
     * @return
     * The ownerId
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     *
     * @param ownerId
     * The ownerId
     */
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    /**
     *
     * @return
     * The facilities
     */
    public Facilities getFacilities() {
        return facilities;
    }

    /**
     *
     * @param facilities
     * The facilities
     */
    public void setFacilities(Facilities facilities) {
        this.facilities = facilities;
    }

    /**
     *
     * @return
     * The priceMRP
     */
    public Integer getPriceMRP() {
        return priceMRP;
    }

    /**
     *
     * @param priceMRP
     * The priceMRP
     */
    public void setPriceMRP(Integer priceMRP) {
        this.priceMRP = priceMRP;
    }

    /**
     *
     * @return
     * The hotelName
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     *
     * @param hotelName
     * The hotelName
     */
    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    /**
     *
     * @return
     * The wowPrice
     */
    public Integer getWowPrice() {
        return wowPrice;
    }

    /**
     *
     * @param wowPrice
     * The wowPrice
     */
    public void setWowPrice(Integer wowPrice) {
        this.wowPrice = wowPrice;
    }

    /**
     *
     * @return
     * The adults
     */
    public Integer getAdults() {
        return adults;
    }

    /**
     *
     * @param adults
     * The adults
     */
    public void setAdults(Integer adults) {
        this.adults = adults;
    }

    /**
     *
     * @return
     * The roomId
     */
    public String getRoomId() {
        return roomId;
    }

    /**
     *
     * @param roomId
     * The roomId
     */
    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    /**
     *
     * @return
     * The children
     */
    public Integer getChildren() {
        return children;
    }

    /**
     *
     * @param children
     * The children
     */
    public void setChildren(Integer children) {
        this.children = children;
    }

    /**
     *
     * @return
     * The noOfRooms
     */
    public Integer getNoOfRooms() {
        return noOfRooms;
    }

    /**
     *
     * @param noOfRooms
     * The noOfRooms
     */
    public void setNoOfRooms(Integer noOfRooms) {
        this.noOfRooms = noOfRooms;
    }

    /**
     *
     * @return
     * The roomType
     */
    public String getRoomType() {
        return roomType;
    }

    /**
     *
     * @param roomType
     * The roomType
     */
    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    /**
     *
     * @return
     * The imagesS3
     */
    public ImagesS3 getImagesS3() {
        return imagesS3;
    }

    /**
     *
     * @param imagesS3
     * The imagesS3
     */
    public void setImagesS3(ImagesS3 imagesS3) {
        this.imagesS3 = imagesS3;
    }

    /**
     *
     * @return
     * The ac
     */
    public Boolean getAc() {
        return ac;
    }

    /**
     *
     * @param ac
     * The ac
     */
    public void setAc(Boolean ac) {
        this.ac = ac;
    }

    /**
     *
     * @return
     * The salePrice
     */
    public Integer getSalePrice() {
        return salePrice;
    }

    /**
     *
     * @param salePrice
     * The salePrice
     */
    public void setSalePrice(Integer salePrice) {
        this.salePrice = salePrice;
    }

    /**
     *
     * @return
     * The hotelId
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     *
     * @param hotelId
     * The hotelId
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    @Override
    public String toString() {
        return "RoomInfo2{" +
                "ownerId='" + ownerId + '\'' +
                ", facilities=" + facilities +
                ", priceMRP=" + priceMRP +
                ", hotelName='" + hotelName + '\'' +
                ", wowPrice=" + wowPrice +
                ", adults=" + adults +
                ", roomId='" + roomId + '\'' +
                ", children=" + children +
                ", noOfRooms=" + noOfRooms +
                ", roomType='" + roomType + '\'' +
                ", imagesS3=" + imagesS3 +
                ", ac=" + ac +
                ", salePrice=" + salePrice +
                ", hotelId='" + hotelId + '\'' +
                '}';
    }
}
