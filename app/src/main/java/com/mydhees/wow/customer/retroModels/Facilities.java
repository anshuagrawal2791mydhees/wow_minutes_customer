package com.mydhees.wow.customer.retroModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anshu on 01/07/16.
 */
public class Facilities {

    @SerializedName("hasWifi")
    @Expose
    private Boolean hasWifi;
    @SerializedName("hasDining")
    @Expose
    private Boolean hasDining;
    @SerializedName("hasTv")
    @Expose
    private Boolean hasTv;
    @SerializedName("hasGym")
    @Expose
    private Boolean hasGym;

    /**
     *
     * @return
     * The hasWifi
     */
    public Boolean getHasWifi() {
        return hasWifi;
    }

    /**
     *
     * @param hasWifi
     * The hasWifi
     */
    public void setHasWifi(Boolean hasWifi) {
        this.hasWifi = hasWifi;
    }

    /**
     *
     * @return
     * The hasDining
     */
    public Boolean getHasDining() {
        return hasDining;
    }

    /**
     *
     * @param hasDining
     * The hasDining
     */
    public void setHasDining(Boolean hasDining) {
        this.hasDining = hasDining;
    }

    /**
     *
     * @return
     * The hasTv
     */
    public Boolean getHasTv() {
        return hasTv;
    }

    /**
     *
     * @param hasTv
     * The hasTv
     */
    public void setHasTv(Boolean hasTv) {
        this.hasTv = hasTv;
    }

    /**
     *
     * @return
     * The hasGym
     */
    public Boolean getHasGym() {
        return hasGym;
    }

    /**
     *
     * @param hasGym
     * The hasGym
     */
    public void setHasGym(Boolean hasGym) {
        this.hasGym = hasGym;
    }

    @Override
    public String toString() {
        return "Facilities{" +
                "hasWifi=" + hasWifi +
                ", hasDining=" + hasDining +
                ", hasTv=" + hasTv +
                ", hasGym=" + hasGym +
                '}';
    }
}
