package com.mydhees.wow.customer.retroModels;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class offer_details {

    @SerializedName("roomInfo")
    @Expose
    private RoomInfo2 roomInfo;
    @SerializedName("offerInfo")
    @Expose
    private OfferInfo2 offerInfo;
    @SerializedName("hotelInfo")
    @Expose
    private HotelInfo2 hotelInfo;

    /**
     *
     * @return
     * The roomInfo
     */
    public RoomInfo2 getRoomInfo() {
        return roomInfo;
    }

    /**
     *
     * @param roomInfo
     * The roomInfo
     */
    public void setRoomInfo(RoomInfo2 roomInfo) {
        this.roomInfo = roomInfo;
    }

    /**
     *
     * @return
     * The offerInfo
     */
    public OfferInfo2 getOfferInfo() {
        return offerInfo;
    }

    /**
     *
     * @param offerInfo
     * The offerInfo
     */
    public void setOfferInfo(OfferInfo2 offerInfo) {
        this.offerInfo = offerInfo;
    }

    /**
     *
     * @return
     * The hotelInfo
     */
    public HotelInfo2 getHotelInfo() {
        return hotelInfo;
    }

    /**
     *
     * @param hotelInfo
     * The hotelInfo
     */
    public void setHotelInfo(HotelInfo2 hotelInfo) {
        this.hotelInfo = hotelInfo;
    }

    @Override
    public String toString() {
        return "offer_details{" +
                "roomInfo=" + roomInfo +
                ", offerInfo=" + offerInfo +
                ", hotelInfo=" + hotelInfo +
                '}';
    }
}


