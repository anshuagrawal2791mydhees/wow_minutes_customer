package com.mydhees.wow.customer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.siyamed.shapeimageview.CircularImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Feedback extends AppCompatActivity {

    TextView hotelName,personName,mail;
    EditText yourExperience;
    RatingBar frontOfficeStaff,restuAmb,restuStaff,houseKeeping,cleanRoom,quickChekin,likeToStayAgain,overAllRate;
    Switch yesNo;
    ImageView image;
    CircularImageView profile_pic;


    Button submit;
    boolean error=false;
    String metdat;
    JSONObject metadata;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;
    SharedPreferences profile;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        volleySingleton = VolleySingleton.getinstance(this);
        requestQueue = volleySingleton.getrequestqueue();
        Intent intent = getIntent();
        metdat = intent.getStringExtra("details");
        Log.e("metdat",metdat);
        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,MODE_PRIVATE);

        try {
            metadata = new JSONObject(metdat);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Metadata in feedback",metdat);

        hotelName = (TextView)findViewById(R.id.hotel_namef);
        try {
            hotelName.setText(metadata.getJSONObject("details").getJSONObject("booking").getString("hotelName"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        personName = (TextView)findViewById(R.id.person_namef);
        personName.setText(profile.getString(Constants.NAME,"default"));

        mail = (TextView)findViewById(R.id.person_emailf);
        mail.setText(profile.getString(Constants.EMAIL,"default"));
        
        yourExperience = (EditText)findViewById(R.id.your_experience);

        image = (ImageView)findViewById(R.id.image);

        frontOfficeStaff = (RatingBar)findViewById(R.id.front_off_staff);
        restuAmb = (RatingBar)findViewById(R.id.restu_amb);
        restuStaff = (RatingBar)findViewById(R.id.restu_staff);
        houseKeeping = (RatingBar)findViewById(R.id.hotel_keep);
        cleanRoom = (RatingBar)findViewById(R.id.clean_room);
        quickChekin = (RatingBar)findViewById(R.id.quick_chickin);
       likeToStayAgain = (RatingBar)findViewById(R.id.like_stay_again);
        overAllRate = (RatingBar)findViewById(R.id.over_all_rate);
        yesNo = (Switch)findViewById(R.id.yesNo);
        profile_pic =(CircularImageView)findViewById(R.id.prof_picf);
        profile_pic.setImageURI(Uri.parse(Constants.PROFILE_IMAGE_URI));

        submit = (Button)findViewById(R.id.submit_feedback);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  error = false;
//               if(yourExperience.getText().toString().equals("")) {
//                   yourExperience.setError("Required");
//               }

                final ProgressDialog dialog = new ProgressDialog(Feedback.this);
                dialog.show();

                   final float fo_staff = frontOfficeStaff.getRating();
                   final float rest_amb = restuAmb.getRating();
                   final float rest_staff = restuStaff.getRating();
                   final float house_keep = houseKeeping.getRating();
                   final float clean_room = cleanRoom.getRating();
                   final float quick_chkin = quickChekin.getRating();
                   final float like_stay_again = likeToStayAgain.getRating();
                   final float over_all = overAllRate.getRating();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.FEEDBACK,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                dialog.dismiss();
                                Toast.makeText(getApplicationContext(),"Submitted",Toast.LENGTH_LONG).show();
                               // Log.e("vollley error",error.toString());
                                Intent intent = new Intent(Feedback.this,Home.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                        Log.e("vollley error",error.toString());
                    }
                }){
                    @Override
                    protected Map<String,String> getParams(){
                        Map<String,String> params = new HashMap<String, String>();
                        params.put(Constants.FRONT_OFFICE_STAFF,fo_staff+"");
                        params.put(Constants.RESTAURANT_AMBIANCE,rest_amb+"");
                        params.put(Constants.RESTAURANT_STAFF,rest_staff+"");
                        params.put(Constants.HOUSE_KEEPING,house_keep+"");
                        params.put(Constants.CLEANLINESS,clean_room+"");
                        params.put(Constants.CHECK_IN_PROCEDURE,quick_chkin+"");
                        params.put(Constants.LIKELY_TO_STAY_AGAIN,like_stay_again+"");
                        params.put(Constants.OVERALL,over_all+"");
                        params.put(Constants.REVIEW,yourExperience.getText().toString());
                        params.put(Constants.CUSTOMERID,profile.getString(Constants.ID,"default"));
                        params.put(Constants.CUSTOMER_NAME,profile.getString(Constants.NAME,"default"));
                        try {
                            params.put(Constants.HOTELID,metadata.getJSONObject("details").getString("hotelId"));
                            params.put(Constants.ROOMID,metadata.getJSONObject("details").getString("roomId"));
                            params.put(Constants.ROOM_TYPE,metadata.getJSONObject("details").getJSONObject("booking").getString("roomType"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        



                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        10000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                requestQueue.add(stringRequest);


                //Toast.makeText(getApplicationContext(),"Successful", Toast.LENGTH_SHORT).show();
            }
        });










    }
    //For back button in Appbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
