package com.mydhees.wow.customer;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class InviteAndEarn extends AppCompatActivity {
    Button inviteEarn;
    TextView termsCondition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_and_earn);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        inviteEarn = (Button)findViewById(R.id.inviteEarn);
        termsCondition = (TextView)findViewById(R.id.termsCon);

        termsCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(InviteAndEarn.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                dialog.setContentView(R.layout.terms_condition_dialog);
                dialog.setCancelable(false);
                dialog.show();
                Button btn = (Button)dialog.findViewById(R.id.dialog_got_it);

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

//                 WindowManager.LayoutParams params2 = dialog.getWindow().getAttributes(); // change this to your dialog.
//                params2.x = -1;
//                params2.x = 1;
//                params2.y = 1;
//                params2.y = -1;// Here is the param to set your dialog position. Same with params.x
//                dialog.getWindow().setAttributes(params2);


            }
        });

        inviteEarn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Try out the WOW Minutes App and get Rs.100 worth WOW Money. Download link http://www.mydhees.com/index.php ";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });
    }
    //For back button in toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
