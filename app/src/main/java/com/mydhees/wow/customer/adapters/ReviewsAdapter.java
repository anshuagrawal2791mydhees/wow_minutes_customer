package com.mydhees.wow.customer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.objects.Review;

import java.util.ArrayList;

/**
 * Created by Abhishek on 22-Aug-16.
 */

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<Review> ReviewList = new ArrayList<>();


    public ReviewsAdapter(Context context){
        this.context=context;
        layoutInflater = layoutInflater.from(context);
    }

    public void history(Review history){
        ReviewList.add(history);
        notifyItemInserted(ReviewList.size());
    }

    public ArrayList<Review> getReviewlist()
    {
        return ReviewList;
    }
    public void clearAll(){
        ReviewList.clear();
    }
    public void addAll(ArrayList<Review> list){
        ReviewList.clear();
        ReviewList.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_row,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Review reviewList = ReviewList.get(position);
        holder.name.setText(reviewList.getName()+"");
        holder.review.setText(reviewList.getReview()+" ");
        holder.rating.setRating(reviewList.getRating());

    }

    @Override
    public int getItemCount() {
        return ReviewList.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView name,review;
        RatingBar rating;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.name_review);
            review = (TextView)itemView.findViewById(R.id.review_review);
            //rating.setEnabled(false);
            rating = (RatingBar) itemView.findViewById(R.id.review_rating);





        }
    }
}
