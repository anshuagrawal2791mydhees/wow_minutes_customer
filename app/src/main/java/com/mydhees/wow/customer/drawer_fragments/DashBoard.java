package com.mydhees.wow.customer.drawer_fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mydhees.wow.customer.ComingSoonTemporaryActivity;
import com.mydhees.wow.customer.Constants;
import com.mydhees.wow.customer.OffersPage;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.myWish.MyWishPageOne;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashBoard extends Fragment {
    Button myWish,minOffer;
   // Switch mySwitch;
    CardView hotels,bus,restuarants,movies,paying_guest,others;
    boolean isOfferPressed=true,isWishPressed=false;
    TextView wishMor;
    ViewPager mViewPager = null;
    Timer timer;
    int count=0;
    SharedPreferences sharedPreferences;
    Handler handler = new Handler();

    private ShareActionProvider mShareActionProvider;



    int[] mResources = {
            R.drawable.sliderimage_1,
             R.drawable.sliderimage_2,
             R.drawable.sliderimage_3,
            R.drawable.sliderimage_4
    };



    public DashBoard() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       // return inflater.inflate(R.layout.fragment_dash_board, container, false);
        View v =inflater.inflate(R.layout.fragment_dash_board, container, false);

        hotels = (CardView) v.findViewById(R.id.hotels);
        bus = (CardView) v.findViewById(R.id.bus);
        restuarants = (CardView) v.findViewById(R.id.restuarants);
        movies = (CardView) v.findViewById(R.id.movies);
        paying_guest = (CardView) v.findViewById(R.id.paying_guest);
        others = (CardView) v.findViewById(R.id.others);

        minOffer = (Button)v.findViewById(R.id.minOffer);
        myWish = (Button)v.findViewById(R.id.myWish);
       // mySwitch = (Switch)v.findViewById(R.id.switch1);

        // edited by Pranav
        SharedPreferences profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,Context.MODE_PRIVATE);
        sharedPreferences = getActivity().getSharedPreferences("MyData", Context.MODE_PRIVATE);
        String n = profile.getString(Constants.NAME,"User");
        String na = n;
        wishMor = (TextView)v.findViewById(R.id.wish);
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if(timeOfDay >= 0 && timeOfDay < 12){
            wishMor.setText("Good Morning,"+" "+na);
        }else if(timeOfDay >= 12 && timeOfDay < 16){
            wishMor.setText("Good Afternoon,"+" "+na);
        }else if(timeOfDay >= 16 && timeOfDay < 21){
            wishMor.setText("Good Evening,"+" "+na);
        }else if(timeOfDay >= 21 && timeOfDay < 24){
            wishMor.setText("Good Evening,"+" "+na);
        }




        // Inflate menu resource file.
//        getMenuInflater().inflate(R.menu.share_menu, menu);
//

//
//        // Locate MenuItem with ShareActionProvider
//        MenuItem item = menu.findItem(R.id.menu_item_share);
//
//        // Fetch and store ShareActionProvider
//        mShareActionProvider = (ShareActionProvider) item.getActionProvider();



        // Timer for auto sliding
        timer  = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(count<=6){
                            mViewPager.setCurrentItem(count);
                            count++;
                        }else{
                            count = 0;
                            mViewPager.setCurrentItem(count);
                        }
                    }
                });
            }
        }, 2000, 3000);




        minOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    minOffer.setBackgroundColor(Color.RED);
                    myWish.setBackgroundColor(Color.WHITE);
                    minOffer.setTextColor(Color.WHITE);
                isOfferPressed = true;
                isWishPressed = false;




                myWish.setTextColor(Color.RED);
//                Intent it= new Intent(getActivity(),MyWishPageOne.class);

//                it.putExtra("category",v.getTag().toString());
//                startActivity(it);

            }
        });

        myWish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    myWish.setBackgroundColor(Color.RED);
                    minOffer.setBackgroundColor(Color.WHITE);
                    myWish.setTextColor(Color.WHITE);

             minOffer.setTextColor(Color.RED);
                isOfferPressed = false;
                isWishPressed = true;
                // for test of Mywish
               // startActivity(new Intent(getContext(),MyWishPageOne.class));

            }

        });

        hotels.setTag("hotel");
        bus.setTag("bus");
        restuarants.setTag("restaurant");
        movies.setTag("movies");
        paying_guest.setTag("paying_guest");
        others.setTag("others");





        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                // edited by Pranav
                if (v.getTag().equals("hotel")){
                    if(isOfferPressed){
                        intent = new Intent(getActivity(),OffersPage.class);
                    }
                    else{
                        intent = new Intent(getActivity(),MyWishPageOne.class);
                    }
                    intent.putExtra("category",v.getTag().toString());
                    startActivity(intent);
                }
                else {
                    // clicked card is not the hotel tag
                    // display coming soon
                    intent = new Intent(getActivity(), ComingSoonTemporaryActivity.class);
                    startActivity(intent);
                }
            }
        };
        hotels.setOnClickListener(listener);
        bus.setOnClickListener(listener);
        restuarants.setOnClickListener(listener);
        movies.setOnClickListener(listener);
        paying_guest.setOnClickListener(listener);
        others.setOnClickListener(listener);



        CustomPagerAdapter mCustomPagerAdapter = new CustomPagerAdapter(getContext());

        mViewPager = (ViewPager)v.findViewById(R.id.pager);
        mViewPager.setAdapter(mCustomPagerAdapter);








            return v;


    }

    // Call to update the share intent
    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }




    //view pager
    public class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mResources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
            imageView.setImageResource(mResources[position]);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }


}
