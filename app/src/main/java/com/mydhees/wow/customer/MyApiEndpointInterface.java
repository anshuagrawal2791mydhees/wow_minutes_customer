package com.mydhees.wow.customer;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mydhees.wow.customer.retroModels.Offer;
import com.mydhees.wow.customer.retroModels.offer_details;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by anshu on 22/06/16.
 */
public interface MyApiEndpointInterface {
//    public static final String BASE_URL = "http://wowservices.ap-south-1.elasticbeanstalk.com";

    @FormUrlEncoded
    @POST("/get_offers")
    Call<ArrayList<Offer>> displayoffers(@Field("city") String city);
    @FormUrlEncoded
    @POST("/get_offer_details")
    Call<offer_details> displayofferdetails(@Field("city") String city,@Field("offerId") String offer_id);





    class Factory{
        private static MyApiEndpointInterface service;

        public static MyApiEndpointInterface getInstance(Context context){
            if(service==null)
            {
                Gson gson = new GsonBuilder()
                        .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                        .create();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.MAIN_SERVER_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson)).build();
                service = retrofit.create(MyApiEndpointInterface.class);
                return service;
            }
            else
                return service;
        }
    }
}
