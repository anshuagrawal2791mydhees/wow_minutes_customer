package com.mydhees.wow.customer.drawer_fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.credits.Balance;
import com.mydhees.wow.customer.credits.Purchased;

/**
 * A simple {@link Fragment} subclass.
 */
public class Credits extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 2 ;

    public Credits() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View x =  inflater.inflate(R.layout.fragment_credits, null);

        tabLayout = (TabLayout) x.findViewById(R.id.tab_credit);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager_credit);


        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));


        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
                tabLayout.getTabAt(0).setIcon(R.drawable.icon_balance);
                // tabLayout.getTabAt(one).setIcon(R.drawable.ic_reedem_history);
                tabLayout.getTabAt(1).setIcon(R.drawable.icon_purchase_history);

            }
        });



        return x;
    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position)
        {
            switch (position){
                case 0 : return new Balance();
                case 1 : return new Purchased();
                // case 2 : return new Purchased();

            }
            return null;
        }

        @Override
        public int getCount() {

            return 2;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return "Balance";

                case 1 :
                    return "Purchase History";

            }
            return null;
        }
    }

}
