package com.mydhees.wow.customer.retroModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anshu on 01/07/16.
 */
public class Facilities_ {

    @SerializedName("hasOutDoorgames")
    @Expose
    private Boolean hasOutDoorgames;
    @SerializedName("hasWifi")
    @Expose
    private Boolean hasWifi;
    @SerializedName("hasCoffeeShop")
    @Expose
    private Boolean hasCoffeeShop;
    @SerializedName("hasDining")
    @Expose
    private Boolean hasDining;
    @SerializedName("hasSwimming")
    @Expose
    private Boolean hasSwimming;
    @SerializedName("hasFrontDesk")
    @Expose
    private Boolean hasFrontDesk;
    @SerializedName("hasRoomService")
    @Expose
    private Boolean hasRoomService;
    @SerializedName("hasParking")
    @Expose
    private Boolean hasParking;
    @SerializedName("hasLaundry")
    @Expose
    private Boolean hasLaundry;
    @SerializedName("hasBusinessService")
    @Expose
    private Boolean hasBusinessService;
    @SerializedName("hasTravelAssistance")
    @Expose
    private Boolean hasTravelAssistance;
    @SerializedName("hasSpa")
    @Expose
    private Boolean hasSpa;
    @SerializedName("hasGym")
    @Expose
    private Boolean hasGym;
    @SerializedName("hasTv")
    @Expose
    private Boolean hasTv;

    /**
     *
     * @return
     * The hasOutDoorgames
     */
    public Boolean getHasOutDoorgames() {
        return hasOutDoorgames;
    }

    /**
     *
     * @param hasOutDoorgames
     * The hasOutDoorgames
     */
    public void setHasOutDoorgames(Boolean hasOutDoorgames) {
        this.hasOutDoorgames = hasOutDoorgames;
    }

    /**
     *
     * @return
     * The hasWifi
     */
    public Boolean getHasWifi() {
        return hasWifi;
    }

    /**
     *
     * @param hasWifi
     * The hasWifi
     */
    public void setHasWifi(Boolean hasWifi) {
        this.hasWifi = hasWifi;
    }

    /**
     *
     * @return
     * The hasCoffeeShop
     */
    public Boolean getHasCoffeeShop() {
        return hasCoffeeShop;
    }

    /**
     *
     * @param hasCoffeeShop
     * The hasCoffeeShop
     */
    public void setHasCoffeeShop(Boolean hasCoffeeShop) {
        this.hasCoffeeShop = hasCoffeeShop;
    }

    /**
     *
     * @return
     * The hasDining
     */
    public Boolean getHasDining() {
        return hasDining;
    }

    /**
     *
     * @param hasDining
     * The hasDining
     */
    public void setHasDining(Boolean hasDining) {
        this.hasDining = hasDining;
    }

    /**
     *
     * @return
     * The hasSwimming
     */
    public Boolean getHasSwimming() {
        return hasSwimming;
    }

    /**
     *
     * @param hasSwimming
     * The hasSwimming
     */
    public void setHasSwimming(Boolean hasSwimming) {
        this.hasSwimming = hasSwimming;
    }

    /**
     *
     * @return
     * The hasFrontDesk
     */
    public Boolean getHasFrontDesk() {
        return hasFrontDesk;
    }

    /**
     *
     * @param hasFrontDesk
     * The hasFrontDesk
     */
    public void setHasFrontDesk(Boolean hasFrontDesk) {
        this.hasFrontDesk = hasFrontDesk;
    }

    /**
     *
     * @return
     * The hasRoomService
     */
    public Boolean getHasRoomService() {
        return hasRoomService;
    }

    /**
     *
     * @param hasRoomService
     * The hasRoomService
     */
    public void setHasRoomService(Boolean hasRoomService) {
        this.hasRoomService = hasRoomService;
    }

    /**
     *
     * @return
     * The hasParking
     */
    public Boolean getHasParking() {
        return hasParking;
    }

    /**
     *
     * @param hasParking
     * The hasParking
     */
    public void setHasParking(Boolean hasParking) {
        this.hasParking = hasParking;
    }

    /**
     *
     * @return
     * The hasLaundry
     */
    public Boolean getHasLaundry() {
        return hasLaundry;
    }

    /**
     *
     * @param hasLaundry
     * The hasLaundry
     */
    public void setHasLaundry(Boolean hasLaundry) {
        this.hasLaundry = hasLaundry;
    }

    /**
     *
     * @return
     * The hasBusinessService
     */
    public Boolean getHasBusinessService() {
        return hasBusinessService;
    }

    /**
     *
     * @param hasBusinessService
     * The hasBusinessService
     */
    public void setHasBusinessService(Boolean hasBusinessService) {
        this.hasBusinessService = hasBusinessService;
    }

    /**
     *
     * @return
     * The hasTravelAssistance
     */
    public Boolean getHasTravelAssistance() {
        return hasTravelAssistance;
    }

    /**
     *
     * @param hasTravelAssistance
     * The hasTravelAssistance
     */
    public void setHasTravelAssistance(Boolean hasTravelAssistance) {
        this.hasTravelAssistance = hasTravelAssistance;
    }

    /**
     *
     * @return
     * The hasSpa
     */
    public Boolean getHasSpa() {
        return hasSpa;
    }

    /**
     *
     * @param hasSpa
     * The hasSpa
     */
    public void setHasSpa(Boolean hasSpa) {
        this.hasSpa = hasSpa;
    }

    /**
     *
     * @return
     * The hasGym
     */
    public Boolean getHasGym() {
        return hasGym;
    }

    /**
     *
     * @param hasGym
     * The hasGym
     */
    public void setHasGym(Boolean hasGym) {
        this.hasGym = hasGym;
    }

    /**
     *
     * @return
     * The hasTv
     */
    public Boolean getHasTv() {
        return hasTv;
    }

    /**
     *
     * @param hasTv
     * The hasTv
     */
    public void setHasTv(Boolean hasTv) {
        this.hasTv = hasTv;
    }

    @Override
    public String toString() {
        return "Facilities_{" +
                "hasOutDoorgames=" + hasOutDoorgames +
                ", hasWifi=" + hasWifi +
                ", hasCoffeeShop=" + hasCoffeeShop +
                ", hasDining=" + hasDining +
                ", hasSwimming=" + hasSwimming +
                ", hasFrontDesk=" + hasFrontDesk +
                ", hasRoomService=" + hasRoomService +
                ", hasParking=" + hasParking +
                ", hasLaundry=" + hasLaundry +
                ", hasBusinessService=" + hasBusinessService +
                ", hasTravelAssistance=" + hasTravelAssistance +
                ", hasSpa=" + hasSpa +
                ", hasGym=" + hasGym +
                ", hasTv=" + hasTv +
                '}';
    }
}
