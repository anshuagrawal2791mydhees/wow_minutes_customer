package com.mydhees.wow.customer.retroModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Offer {

    @SerializedName("images")
    @Expose
    public List<String> images = new ArrayList<String>();
    @SerializedName("offerInfo")
    @Expose
    public OfferInfo offerInfo;
    @SerializedName("hotelInfo")
    @Expose
    public HotelInfo2 hotelInfo;

    @Override
    public String toString() {
        return "Offer{" +
                "images=" + images +
                ", offerInfo=" + offerInfo +
                ", hotelInfo=" + hotelInfo +
                '}';
    }
}

