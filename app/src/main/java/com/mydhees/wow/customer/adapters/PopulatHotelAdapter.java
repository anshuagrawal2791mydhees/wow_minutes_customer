package com.mydhees.wow.customer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.objects.popularHotelObject;

import java.util.ArrayList;

/**
 * Created by Abhishek on 23-Jun-16.
 */

public class PopulatHotelAdapter extends RecyclerView.Adapter<PopulatHotelAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<popularHotelObject> Popular = new ArrayList<>();


    public PopulatHotelAdapter(Context context){
        this.context=context;
        layoutInflater = layoutInflater.from(context);
    }

    public void popular(popularHotelObject popular){
        Popular.add(popular);
        notifyItemInserted(Popular.size());
    }

    public ArrayList<popularHotelObject> getHistorylist()
    {
        return Popular;
    }
    public void clearAll(){
        Popular.clear();
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.popular_hotel_row,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        popularHotelObject popularHotelObject = Popular.get(position);
        holder.hotel_name.setText(popularHotelObject.getHotelName()+"");
        holder.hotel_address.setText(popularHotelObject.getAddress()+" ");
        holder.rating.setText(popularHotelObject.getRating()+" ");
//        holder.hotelStar.setText(popularHotelObject.getRating()+"");
//        holder.rating.setText(popularHotelObject.getRating()+"");

    }

    @Override
    public int getItemCount() {
        return Popular.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView hotel_name,hotel_address,rating;
        RatingBar hotelStar;
        FrameLayout hotelImage;


        public RecyclerViewHolder(View itemView) {
            super(itemView);
            hotel_name = (TextView)itemView.findViewById(R.id.hotel_namef);
            hotel_address = (TextView)itemView.findViewById(R.id.hotel_addressf);
            rating = (TextView)itemView.findViewById(R.id.ratingf);
            hotelStar = (RatingBar)itemView.findViewById(R.id.hotel_starf);
            hotelImage = (FrameLayout) itemView.findViewById(R.id.hotel_picp);



        }
    }
}
