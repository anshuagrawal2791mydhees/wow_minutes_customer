package com.mydhees.wow.customer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.objects.purchasedHistory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Abhishek on 28-Sep-16.
 */

public class PurchasedPointsAdapter extends RecyclerView.Adapter<PurchasedPointsAdapter.myviewholder> {
    Context context;
    LayoutInflater inflater;
    ArrayList<purchasedHistory> offers = new ArrayList<>();
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public PurchasedPointsAdapter(Context context) {
        this.context=context;
        inflater=LayoutInflater.from(context);
    }
    public void addoffer(purchasedHistory recievedoffer)
    {
        offers.add(0,recievedoffer);
        notifyItemInserted(offers.size());
    }
    public ArrayList<purchasedHistory> getofferlist()
    {
        return offers;
    }
    public void clearAll()
    {
        offers.clear();
    }

    @Override
    public myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.purchased_credits_row,parent,false);
        myviewholder viewholder = new myviewholder(v);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(myviewholder holder, int position) {
        purchasedHistory purchase_his = offers.get(position);

        holder.amount.setText(purchase_his.getAmount());
        holder.orderId.setText(purchase_his.getOrderId());
        holder.transationID.setText(purchase_his.getTransationID());
        holder.status.setText(purchase_his.getStatus());
        holder.paymentType.setText(purchase_his.getPaymentType());

    }

    @Override
    public int getItemCount() {
        return offers.size();
    }

    static class myviewholder extends RecyclerView.ViewHolder
    {
        ImageView imageView;
        TextView date,amount,orderId,transationID,status,paymentType;


        public myviewholder(View itemView) {
            super(itemView);

            imageView = (ImageView)itemView.findViewById(R.id.tick);
            date = (TextView) itemView.findViewById(R.id.date_ph);
            amount = (TextView) itemView.findViewById(R.id.amount_ph);
            orderId = (TextView) itemView.findViewById(R.id.order_id_ph);
            transationID = (TextView) itemView.findViewById(R.id.tran_id_ph);
            status = (TextView) itemView.findViewById(R.id.status_ph);
            paymentType = (TextView) itemView.findViewById(R.id.payment_type_ph);

        }
    }
}

