package com.mydhees.wow.customer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.objects.offer_details;

import java.util.ArrayList;

/**
 * Created by Abhishek on 23-Jun-16.
 */

public class MyWishHListAdapter extends RecyclerView.Adapter<MyWishHListAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<offer_details> HotelList = new ArrayList<>();


    public MyWishHListAdapter(Context context){
        this.context=context;
        layoutInflater = layoutInflater.from(context);
    }

    public void history(offer_details history){
        HotelList.add(history);
        notifyItemInserted(HotelList.size());
    }

    public ArrayList<offer_details> getHotellist()
    {
        return HotelList;
    }
    public void clearAll(){
        HotelList.clear();
        notifyDataSetChanged();
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mywish_hotel_list_row,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        offer_details hotelList = HotelList.get(position);
        holder.hotel_address.setText(hotelList.getHotel_name()+"");
        holder.hotel_name.setText(hotelList.getAddress()+"");

    }

    @Override
    public int getItemCount() {
        return HotelList.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView hotel_name,hotel_address;


        public RecyclerViewHolder(View itemView) {
            super(itemView);
            hotel_name = (TextView)itemView.findViewById(R.id.hotel_name_hl);
            hotel_address = (TextView)itemView.findViewById(R.id.hotel_address_hl);




        }
    }
}
