package com.mydhees.wow.customer.drawer_fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.customer.Constants;
import com.mydhees.wow.customer.Home;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.VolleySingleton;
import com.mydhees.wow.customer.adapters.WishDetailListAdapter;
import com.mydhees.wow.customer.objects.WishDetailObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */
public class WishReview extends Fragment {

    RecyclerView recycler;
    ArrayList<WishDetailListAdapter> wish = new ArrayList<>();
    WishDetailListAdapter myadapter;
    LinearLayoutManager manager;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;

    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    ArrayList<WishDetailObject> wisheslist;
    CardView sortByCard;
    Spinner sortBy;

    public WishReview() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_wish_review, container, false);
        recycler = (RecyclerView)v.findViewById(R.id.recyclerWR);
        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor = profile.edit();
        volleySingleton = VolleySingleton.getinstance(getContext());
        requestQueue = volleySingleton.getrequestqueue();
myadapter=new WishDetailListAdapter(getContext());
        recycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        recycler.setAdapter(myadapter);
        recycler.setVisibility(View.VISIBLE);
        parser.setTimeZone(TimeZone.getTimeZone("IST"));

        wisheslist = new ArrayList<>();
        sortBy = (Spinner)v.findViewById(R.id.sortBySpinner);
        sortByCard = (CardView)v.findViewById(R.id.card3);

        final ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getContext(),
                R.array.sortBy, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortBy.setAdapter(adapter2);


        sortBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position ==0){
                    Collections.sort(wisheslist, new Comparator<WishDetailObject>() {
                        @Override
                        public int compare(WishDetailObject lhs, WishDetailObject rhs) {
                            int a =0,b=0;
                            if(lhs.getSerious().equals("Testing"))
                                a=3;
                            else if (lhs.getSerious().equals("Casual"))
                                a=2;
                            else if(lhs.getSerious().equals("Serious"))
                                a=1;
                            else
                                a=0;

                            if(rhs.getSerious().equals("Testing"))
                                b=3;
                            else if (rhs.getSerious().equals("Casual"))
                                b=2;
                            else if(rhs.getSerious().equals("Serious"))
                                b=1;
                            else
                                b=0;
                            if(a<b)
                                return -1;
                            else if (a==b)
                                return 0;
                            else
                                return 1;

                        }
                    });
                    myadapter.addAll(wisheslist);
                }
                if(position==1){
                    Collections.sort(wisheslist, new Comparator<WishDetailObject>() {
                        @Override
                        public int compare(WishDetailObject lhs, WishDetailObject rhs) {
                            int a = Integer.parseInt(lhs.getMinPrice());
                            int b = Integer.parseInt(rhs.getMinPrice());
                            if(a<b)
                                return -1;
                            else if (a==b)
                                return 0;
                            else
                                return 1;

                        }
                    });
                    myadapter.addAll(wisheslist);
                }
                if(position==2){
                    Collections.sort(wisheslist, new Comparator<WishDetailObject>() {
                        @Override
                        public int compare(WishDetailObject lhs, WishDetailObject rhs) {
                            int a = Integer.parseInt(lhs.getMaxPrice());
                            int b = Integer.parseInt(rhs.getMaxPrice());
                            if(a<b)
                                return -1;
                            else if (a==b)
                                return 0;
                            else
                                return 1;
                        }
                    });
                    myadapter.addAll(wisheslist);
                }
                if(position==3){
                    Collections.sort(wisheslist, new Comparator<WishDetailObject>() {
                        @Override
                        public int compare(WishDetailObject lhs, WishDetailObject rhs) {
                            return (lhs.getCheckIn().compareTo(rhs.getCheckIn()));
                        }
                    });
                    myadapter.addAll(wisheslist);
                }
                if(position==4){
                    Collections.sort(wisheslist, new Comparator<WishDetailObject>() {
                        @Override
                        public int compare(WishDetailObject lhs, WishDetailObject rhs) {

                            return (lhs.getDate_check_out().compareTo(rhs.getDate_check_out()));
                        }
                    });
                    myadapter.addAll(wisheslist);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        try {
            getwishes();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return v;
    }

    private void getwishes() throws JSONException {
        myadapter.clearAll();
        wisheslist.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.GET_WISH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        Log.e("response",respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);

                            Log.e("resp of wish",response.toString());
                            // JSONArray data=response.getJSONArray("offers");
                            // Log.e("offers log",data.toString());

                            if ((response.getBoolean("res"))){
                                JSONArray data=response.getJSONArray("wishes");
                                Log.e("offers log",data.toString());
                                myadapter.clearAll();
                                if (data.length()>0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject rec = data.getJSONObject(i);
                                        Date cin=parser.parse(rec.getString("checkIn"));
                                        Date cout=parser.parse(rec.getString("checkOut"));
                                        Date created=parser.parse(rec.getString("createdAt"));
                                        sortByCard.setVisibility(View.VISIBLE);
                                        wisheslist.add(new WishDetailObject(rec.getString("seriousness"), rec.getString("city"),rec.getJSONObject("range").getString("start"),rec.getJSONObject("range").getString("end"), rec.getJSONObject("range").getString("start")+"-"+ rec.getJSONObject("range").getString("end"),rec.getString("noOfRooms"),rec.getString("adults"),rec.getString("children"),rec.getString("wishId"),cin,cout,created));
                                    }
                                    myadapter.addAll(wisheslist);
                                }else{
                                    sortByCard.setVisibility(View.GONE);
                                    Toast.makeText(getContext(), "No Wishes", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());

            }
        }
        ){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("customerId", profile.getString(Constants.ID,"default"));

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
