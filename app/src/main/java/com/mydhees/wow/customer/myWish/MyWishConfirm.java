package com.mydhees.wow.customer.myWish;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.customer.Constants;
import com.mydhees.wow.customer.Home;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MyWishConfirm extends AppCompatActivity {
    TextView city,desiredPrice,checkIn,checkOut,checkInTime,checkOutTime,adults,children,seriousness,roomc;
    Button cencel,confirm;
    String cty,checkInDate,checkTimeIn,checkOutdate,checkTimeOut,rooms,minPrice,maxPrice,adult,child,serious,checkin,checkout,category;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wish_confirm);

        city = (TextView)findViewById(R.id.cityConfirm);
        desiredPrice = (TextView)findViewById(R.id.priceConfirm);
        checkIn = (TextView)findViewById(R.id.checkin_confirm);
        checkOut = (TextView)findViewById(R.id.checkout_confirm);
        checkInTime = (TextView)findViewById(R.id.checkinTimeconfirm);
       checkOutTime = (TextView)findViewById(R.id.checkoutTimeconfirm);
       adults = (TextView)findViewById(R.id.no_adultsConfirm);
        children = (TextView)findViewById(R.id.no_childrenConfirm);
        roomc = (TextView)findViewById(R.id.roomc);
        seriousness = (TextView)findViewById(R.id.seriousConfirm);
        cencel =(Button)findViewById(R.id.cencelConfirm);
        confirm =(Button)findViewById(R.id.confirmConfirm);
        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        volleySingleton = VolleySingleton.getinstance(getApplicationContext());
        requestQueue = volleySingleton.getrequestqueue();

        //toobar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent wish=getIntent();
        cty= wish.getStringExtra("city");
        checkInDate=wish.getStringExtra("cindate");
        checkTimeIn=wish.getStringExtra("cintime");
        checkTimeOut=wish.getStringExtra("couttime");
        checkOutdate=wish.getStringExtra("coutdate");

        rooms=wish.getStringExtra("rooms");
        minPrice=wish.getStringExtra("minPrice");
        maxPrice= wish.getStringExtra("maxPrice");
        adult=wish.getStringExtra("adults");
        child=wish.getStringExtra("child");
        serious=wish.getStringExtra("serious");
        checkout=  wish.getStringExtra("checkoutdatestring");
        checkin=   wish.getStringExtra("checkindatestring");
        category=   wish.getStringExtra("category");

        city.setText(cty);
        checkIn.setText(checkInDate);
        checkInTime.setText(checkTimeIn);
        checkOut.setText(checkOutdate);
        checkOutTime.setText(checkTimeOut);
        roomc.setText(rooms);
        desiredPrice.setText(minPrice+"-"+maxPrice);
        adults.setText(adult);
        children.setText(child);
        seriousness.setText(serious);
confirm.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        try {
            createWish();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
});

    }
//server call to create wish

    private void createWish() throws JSONException {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.show();

        StringRequest request = new StringRequest(Request.Method.POST, Constants.CREATE_WISH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                            Log.e("respwish",response.toString());
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                         Toast.makeText(MyWishConfirm.this, "Success!!!", Toast.LENGTH_SHORT).show();
                                     startActivity(new Intent(getApplicationContext(),Home.class));



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),"Internet error",Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put(Constants.CUSTOMER_ID,profile.getString(Constants.ID,"default"));
                params.put(Constants.CITY,cty);
                params.put("noOfRooms",rooms);
                Log.e("cin",checkin);
                Log.e("cout",checkout);
                params.put("checkIn",checkin);
                params.put("checkOut",checkout);
                params.put("adults",adult);
                params.put("children",child);
                params.put("seriousness",serious);
                params.put("rangeStart",minPrice);
                params.put("rangeEnd",maxPrice);
                params.put("category",category);

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }
    //For back button in toolbar

    private void delete() throws JSONException {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.show();

        StringRequest request = new StringRequest(Request.Method.POST, Constants.DELETE_WISH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                            Log.e("respwish",response.toString());
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                        Toast.makeText(MyWishConfirm.this, "Success!!!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(),Home.class));



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),"Internet error",Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("wishId","4142525246-1-HksC5Ef5");

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
