package com.mydhees.wow.customer;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TransactionSummary extends AppCompatActivity {
    ProgressDialog dialog;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    String order_id,status;
    DateFormat simpledateformat1= new SimpleDateFormat("yyyy-MM-dd");
    TextView pname,bid,offername,bdate,hotl,room,tamount_paid,status1,orderProcessedNot;
    Button backhome;
    ImageView successFailImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_summary);
        volleySingleton = VolleySingleton.getinstance(getApplicationContext());
        requestQueue = volleySingleton.getrequestqueue();
        Intent intent = getIntent();
        order_id=intent.getStringExtra("order_id");
        status=intent.getStringExtra("status");
        Log.e("order_id_sent",order_id);
        successFailImg = (ImageView)findViewById(R.id.sucess_fail_img);

        bid = (TextView)findViewById(R.id.transtn_id);
        bdate = (TextView)findViewById(R.id.bidd);
        hotl = (TextView)findViewById(R.id.bhotel);
        room = (TextView)findViewById(R.id.broomtype);
        offername = (TextView)findViewById(R.id.pemail);
        tamount_paid= (TextView)findViewById(R.id.amount_paid);
        status1 = (TextView)findViewById(R.id.statusp);
        backhome = (Button)findViewById(R.id.backhome);
        orderProcessedNot = (TextView) findViewById(R.id.textView11); // your order processed or sorry please try again


        backhome.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("CommitTransaction")
            @Override
            public void onClick(View v) {
                finish();Intent it=new Intent(TransactionSummary.this,Home.class);
                startActivity(it);     }
        });

        dialog = new ProgressDialog(this);
        try {
            dialog.show();
            updatedatabase();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updatedatabase() throws JSONException {
        StringRequest request = new StringRequest(Request.Method.POST, Constants.STATUS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        Log.e("response", respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                            Log.e("response",response.toString());

                            if (status.equals("CHARGED")) {
                                bid.setText(response.get("order_id").toString());
                                offername.setText(response.get("offerName").toString());
                                Date date=simpledateformat1.parse(response.get("createdAt").toString());
                                bdate.setText(date.toString());
                                orderProcessedNot.setText("Congrats!!! You booked offer Successfully");
                                status1.setText("Payment Succesfull!!!");
                                hotl.setText(response.getJSONObject("booking").getString("hotelName"));
                                room.setText(response.getJSONObject("booking").getString("roomType"));
                                successFailImg.setBackgroundResource(R.drawable.successful);
                                tamount_paid.setText("Rs."+response.get("amount").toString());
                            }
                            else{
                                orderProcessedNot.setTextColor(Color.RED);
                                orderProcessedNot.setText("Your Order has been aborted");
                                status1.setText("Payment Failed!!!");
                                status1.setTextColor(Color.RED);
                                offername.setText(response.get("offerName").toString());
                                Date date=simpledateformat1.parse(response.get("createdAt").toString());
                                bdate.setText(date.toString());
                                bid.setText(response.get("order_id").toString());
                                hotl.setText(response.getJSONObject("booking").getString("hotelName"));
                                room.setText(response.getJSONObject("booking").getString("roomType"));
                                tamount_paid.setText("Transaction Aborted");

                                successFailImg.setBackgroundResource(R.drawable.crossout);



                            }

                        } catch (JSONException e) {
                            Log.e("error", e.toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(TransactionSummary.this, "Error", Toast.LENGTH_LONG).show();
                Log.e("vollley error", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                //params.put("customerId", "123456789");
                params.put("status",status );
                params.put("orderId", order_id);

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }
}
