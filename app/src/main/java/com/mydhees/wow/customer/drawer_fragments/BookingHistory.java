package com.mydhees.wow.customer.drawer_fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonArray;
import com.mydhees.wow.customer.Constants;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.VolleySingleton;
import com.mydhees.wow.customer.adapters.BookingHistoryAdapter;
import com.mydhees.wow.customer.objects.offer_details;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingHistory extends Fragment {
    ArrayList<BookingHistoryAdapter> bookHistory = new ArrayList<>();
    BookingHistoryAdapter myadapter;
    LinearLayoutManager manager;
    Uri uri;
    RecyclerView recycler;
    VolleySingleton volley;
    RequestQueue requestQueue;
    SharedPreferences profile;
    ProgressDialog dialog;
    DateFormat parser2 = new SimpleDateFormat("dd-MM-yyyy");
    DateFormat parser3 = new SimpleDateFormat("HH:mm");
    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

    public BookingHistory() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_booking_history, container, false);

        myadapter = new BookingHistoryAdapter(getContext());
        recycler = (RecyclerView)v.findViewById(R.id.recyclerp);
        recycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        recycler.setAdapter(myadapter);
        volley = VolleySingleton.getinstance(getContext());
        requestQueue = volley.getrequestqueue();
        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);

        dialog = new ProgressDialog(getContext());
        parser.setTimeZone(TimeZone.getTimeZone("IST"));
        dialog.show();
        getOrders(profile.getString(Constants.ID,"default"));










        return v;
    }

    private void getOrders(final String customerID) {
        dialog.setCancelable(false);
        StringRequest request = new StringRequest(Request.Method.POST, Constants.BOOKING_HISTORY, new Response.Listener<String>() {
            @Override
            public void onResponse(String respons) {

                dialog.dismiss();
                Log.e("response", respons.toString());
                JSONObject response = null;
                ArrayList<offer_details> orderslist = new ArrayList<>();
                try {
                    response = new JSONObject(respons);
                    if(response.getBoolean("res")) {
                        JSONArray orders = response.getJSONArray("orders");
                        Log.e("response",respons);
                        ArrayList<String> feedback_data = new ArrayList<>();
                        for (int i = 0; i < orders.length(); i++) {
                            JSONObject object = orders.getJSONObject(i).getJSONObject("booking");
                            JSONObject object2 = orders.getJSONObject(i);

                            Date checkIn = parser.parse(object.getString("checkIn"));
                            Date checkOut = parser.parse(object.getString("checkOut"));

                            feedback_data.add(object2.toString());
                            orderslist.add(new offer_details(object.getString("hotelName"), checkIn, Integer.parseInt(object2.getString("amount")), checkOut, object2.getString("status"), object2.getString("roomId"), object2.getString("hotelId"),orders.getJSONObject(i).getString("order_id")));
                        }
                        Log.e("feedback1",feedback_data.size()+"");
                        myadapter.addFeedbackData(feedback_data);
                        myadapter.addAll(orderslist);

                    }else{

                        Toast.makeText(getContext(), "No Bookings Found!!!", Toast.LENGTH_SHORT).show();
                    }

                    Log.e("response",response.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG).show();
                Log.e("vollley error", error.toString());

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.CUSTOMER_ID, customerID);

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);


    }

}
