package com.mydhees.wow.customer.retroModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anshu on 01/07/16.
 */
public class HotelInfo2 {

    @SerializedName("ownerId")
    @Expose
    private String ownerId;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("location")
    @Expose
    private List<Double> location = new ArrayList<>();
    @SerializedName("pin")
    @Expose
    private Integer pin;
    @SerializedName("address_line2")
    @Expose
    private String addressLine2;
    @SerializedName("hotelName")
    @Expose
    private String hotelName;
    @SerializedName("address_line1")
    @Expose
    private String addressLine1;
    @SerializedName("modifiedAt")
    @Expose
    private String modifiedAt;
    @SerializedName("policies")
    @Expose
    private List<String> policies = new ArrayList<String>();
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("noOfRooms")
    @Expose
    private Integer noOfRooms;
    @SerializedName("facilities")
    @Expose
    private Facilities_ facilities;
    @SerializedName("imagesS3")
    @Expose
    private ImagesS3_ imagesS3;
    @SerializedName("starHotel")
    @Expose
    private Integer starHotel;
    @SerializedName("hotelType")
    @Expose
    private String hotelType;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("reviews")
    @Expose
    private List<Review> reviews = new ArrayList<Review>();
    @SerializedName("userRatings")
    @Expose
    private UserRatings userRatings;
    @SerializedName("numberOfReviews")
    @Expose
    private Integer numberOfReviews;

    /**
     *
     * @return
     * The ownerId
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     *
     * @param ownerId
     * The ownerId
     */
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    /**
     *
     * @return
     * The mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     *
     * @param mobileNumber
     * The mobileNumber
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     *
     * @return
     * The address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The location
     */
    public List<Double> getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(List<Double> location) {
        this.location = location;
    }

    /**
     *
     * @return
     * The pin
     */
    public Integer getPin() {
        return pin;
    }

    /**
     *
     * @param pin
     * The pin
     */
    public void setPin(Integer pin) {
        this.pin = pin;
    }

    /**
     *
     * @return
     * The addressLine2
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     *
     * @param addressLine2
     * The address_line2
     */
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    /**
     *
     * @return
     * The hotelName
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     *
     * @param hotelName
     * The hotelName
     */
    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    /**
     *
     * @return
     * The addressLine1
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     *
     * @param addressLine1
     * The address_line1
     */
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    /**
     *
     * @return
     * The modifiedAt
     */
    public String getModifiedAt() {
        return modifiedAt;
    }

    /**
     *
     * @param modifiedAt
     * The modifiedAt
     */
    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    /**
     *
     * @return
     * The policies
     */
    public List<String> getPolicies() {
        return policies;
    }

    /**
     *
     * @param policies
     * The policies
     */
    public void setPolicies(List<String> policies) {
        this.policies = policies;
    }

    /**
     *
     * @return
     * The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The noOfRooms
     */
    public Integer getNoOfRooms() {
        return noOfRooms;
    }

    /**
     *
     * @param noOfRooms
     * The noOfRooms
     */
    public void setNoOfRooms(Integer noOfRooms) {
        this.noOfRooms = noOfRooms;
    }

    /**
     *
     * @return
     * The facilities
     */
    public Facilities_ getFacilities() {
        return facilities;
    }

    /**
     *
     * @param facilities
     * The facilities
     */
    public void setFacilities(Facilities_ facilities) {
        this.facilities = facilities;
    }

    /**
     *
     * @return
     * The imagesS3
     */
    public ImagesS3_ getImagesS3() {
        return imagesS3;
    }

    /**
     *
     * @param imagesS3
     * The imagesS3
     */
    public void setImagesS3(ImagesS3_ imagesS3) {
        this.imagesS3 = imagesS3;
    }

    /**
     *
     * @return
     * The starHotel
     */
    public Integer getStarHotel() {
        return starHotel;
    }

    /**
     *
     * @param starHotel
     * The starHotel
     */
    public void setStarHotel(Integer starHotel) {
        this.starHotel = starHotel;
    }

    /**
     *
     * @return
     * The hotelType
     */
    public String getHotelType() {
        return hotelType;
    }

    /**
     *
     * @param hotelType
     * The hotelType
     */
    public void setHotelType(String hotelType) {
        this.hotelType = hotelType;
    }

    /**
     *
     * @return
     * The phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *
     * @param phoneNumber
     * The phoneNumber
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     *
     * @return
     * The hotelId
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     *
     * @param hotelId
     * The hotelId
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }
    /**
     *
     * @return
     *     The reviews
     */
    public List<Review> getReviews() {
        return reviews;
    }

    /**
     *
     * @param reviews
     *     The reviews
     */
    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    /**
     *
     * @return
     *     The userRatings
     */
    public UserRatings getUserRatings() {
        return userRatings;
    }

    /**
     *
     * @param userRatings
     *     The userRatings
     */
    public void setUserRatings(UserRatings userRatings) {
        this.userRatings = userRatings;
    }
    /**
     *
     * @return
     *     The numberOfReviews
     */
    public Integer getNumberOfReviews() {
        return numberOfReviews;
    }

    /**
     *
     * @param numberOfReviews
     *     The numberOfReviews
     */
    public void setNumberOfReviews(Integer numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }


    @Override
    public String toString() {
        return "HotelInfo2{" +
                "ownerId='" + ownerId + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", address='" + address + '\'' +
                ", location=" + location +
                ", pin=" + pin +
                ", addressLine2='" + addressLine2 + '\'' +
                ", hotelName='" + hotelName + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", modifiedAt='" + modifiedAt + '\'' +
                ", policies=" + policies +
                ", city='" + city + '\'' +
                ", noOfRooms=" + noOfRooms +
                ", facilities=" + facilities +
                ", imagesS3=" + imagesS3 +
                ", starHotel=" + starHotel +
                ", hotelType='" + hotelType + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", hotelId='" + hotelId + '\'' +
                ", reviews=" + reviews +
                ", userRatings=" + userRatings +
                ", numberOfReviews=" + numberOfReviews +
                '}';
    }
}
