package com.mydhees.wow.customer.objects;

import android.net.Uri;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Abhishek on 13-Jun-16.
 */
public class offer_details {


    int discount;
    ArrayList<Uri> hotel_image=new ArrayList<>();
    int hotel_star;

    String hotel_name,hid;

    String address;
    Double hotel_like;

    Date time_remaining;
    Date checkin;

    int offer_price;
    int old_price;
    Date check_out;
    String status;
    String hotelId;
    String roomId;
    String order_id;

    public offer_details(String address, String hotel_name, String id) {
        this.address = address;
        this.hotel_name = hotel_name;
        this.hid = id;
    }

    public offer_details(int discount, ArrayList<Uri> hotel_image , int hotel_star, String hotel_name, String address, Double hotel_like, Date time_remaining, Date checkin, int offer_price, int old_price, Date check_out) {
        this.discount = discount;
        this.hotel_image.clear();
        this.hotel_image.addAll(hotel_image);
        this.hotel_star = hotel_star;
        this.hotel_name = hotel_name;
        this.address = address;
        this.hotel_like = hotel_like;
        this.time_remaining = time_remaining;
        this.checkin = checkin;
        this.offer_price = offer_price;
        this.old_price = old_price;
        this.check_out = check_out;
    }

    public offer_details(String hotel_name, Date checkin, int offer_price, Date check_out, String status,String roomId,String hotelId,String order_id) {
        this.hotel_name = hotel_name;

        this.checkin = checkin;
        this.offer_price = offer_price;
        this.check_out = check_out;
        this.status = status;
        this.roomId = roomId;
        this.hotelId = hotelId;
        this.order_id = order_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
    public ArrayList<Uri> getHotel_image() {
        return hotel_image;
    }
    public int getHotel_star() {
        return hotel_star;
    }

    public void setHotel_star(int hotel_star) {
        this.hotel_star = hotel_star;
    }

    public String getHotel_name() {
        return hotel_name;
    }

    public void setHotel_name(String hotel_name) {
        this.hotel_name = hotel_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getHotel_like() {
        return hotel_like;
    }

    public void setHotel_like(Double hotel_rating) {
        this.hotel_like = hotel_rating;
    }

    public Date getTime_remaining() {
        return time_remaining;
    }

    public void setTime_remaining(Date time_remaining) {
        this.time_remaining = time_remaining;
    }

    public Date getCheckin() {
        return checkin;
    }

    public void setCheckin(Date checkin) {
        this.checkin = checkin;
    }

    public int getOffer_price() {
        return offer_price;
    }

    public void setOffer_price(int offer_price) {
        this.offer_price = offer_price;
    }

    public int getOld_price() {
        return old_price;
    }

    public void setOld_price(int old_price) {
        this.old_price = old_price;
    }

    public Date getCheck_out() {
        return check_out;
    }

    public void setCheck_out(Date check_out) {
        this.check_out = check_out;
    }

    public String getHotelId() {
        return hotelId;
    }
}
