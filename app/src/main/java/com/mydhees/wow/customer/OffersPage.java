package com.mydhees.wow.customer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.customer.adapters.Offers;
import com.mydhees.wow.customer.drawer_fragments.DashBoard;
import com.mydhees.wow.customer.registrationAndLogin.user_details;
import com.mydhees.wow.customer.retroModels.Offer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersPage extends AppCompatActivity {

    String mYear;
    String mMonth;
    String mDay;
    String mHour,mHour1;
    String mMinute,mMinute1;
    Spinner spinner;

    Calendar c;
    DateFormat parser = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm");
    String checkindatestring,scheduleDatestring,scheduleTimestring,expiryTimestring;
    String checkoutdatestring;
    String checkintimestring;
    String checkouttimestring;

    Date sched_date;
    Date checkin_date;
    Date checkout_date;
    String checkinstring;
    String checkoutstring,scheduledatestring;


    RecyclerView recycler;
    ArrayList<Offers> offers = new ArrayList<>();
    Offers myadapter;
    LinearLayoutManager manager;
    Uri uri;
    ProgressDialog dialog;
    String service;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;

    ViewPager mViewPager = null;
    Timer timer;
    int count=0;
    ImageView no_off;
    Handler handler = new Handler();

    int[] mResources = {
            R.drawable.sliderimage_5,
            R.drawable.sliderimage_6,
            R.drawable.sliderimage_7,
            R.drawable.sliderimage_8
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        Intent intent = getIntent();
        service = intent.getStringExtra("category");
//        Toast.makeText(this,intent.getStringExtra("category"),Toast.LENGTH_LONG).show();
        setContentView(R.layout.activity_offers_page);

        myadapter = new Offers(this);
        recycler = (RecyclerView) findViewById(R.id.recycler);
        spinner = (Spinner) findViewById(R.id.city_spinner);
        recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recycler.setAdapter(myadapter);
        no_off = (ImageView)findViewById(R.id.no_offer);

        recycler.setVisibility(View.VISIBLE);

        volleySingleton = VolleySingleton.getinstance(this);
        requestQueue = volleySingleton.getrequestqueue();



//        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//                R.array.citiWish, android.R.layout.simple_spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(adapter);
        getcities(service);



            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                    if(position>0){
                        myadapter.clearAll();
                        dialog = new ProgressDialog(OffersPage.this);
                        dialog.setCancelable(false);
                        dialog.setMessage("Fetching...");
                        dialog.show();
                        MyApiEndpointInterface myApiEndpointInterface = MyApiEndpointInterface.Factory.getInstance(OffersPage.this);
                        myApiEndpointInterface.displayoffers(spinner.getSelectedItem().toString()).enqueue(new Callback<ArrayList<Offer>>() {
                            @Override
                            public void onResponse(Call<ArrayList<Offer>> call, Response<ArrayList<Offer>> response) {
                                dialog.dismiss();
                                Log.e("resp_code", response.code() + "");

                                if (response.code() == 200) {
                                    ArrayList<Offer> offers = response.body();
                                    Log.e("resp_code", response.code() + "");
                                    Log.e("resp", offers.toString());
                                    myadapter.add_offers(offers);
                                } else if (response.code() == 404) {
                                    Toast.makeText(getApplicationContext(), "No Offers!!! Try again Later.", Toast.LENGTH_LONG).show();
                                }


//                Log.e("resp_code",response.code()+"");
//                Offer current = offers.get(offers.size()-1);
//
//                myadapter.add_offer(new offer_details(30,uri,current.hotelInfo.starHotel,));
                            }

                        @Override
                        public void onFailure(Call<ArrayList<Offer>> call, Throwable t) {
                            dialog.dismiss();
                            Toast.makeText(getApplicationContext(), "no offers available. Try again later", Toast.LENGTH_LONG).show();

                                Log.e("er", t.toString());
                            }
                        });
                    }



                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        // Timer for auto sliding
        timer  = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(count<=6){
                            mViewPager.setCurrentItem(count);
                            count++;
                        }else{
                            count = 0;
                            mViewPager.setCurrentItem(count);
                        }
                    }
                });
            }
        }, 2000, 3000);

         OffersPage.CustomPagerAdapter mCustomPagerAdapter = new OffersPage.CustomPagerAdapter(this);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mCustomPagerAdapter);

    }




    //For back button in Appbar

    private void getcities(final String service) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.show();
        final ArrayList<String> cities_list = new ArrayList<>();
        StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_CITY_LIST,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        Log.e("response",respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                        try {
                            if(response.getBoolean("res"))
                            {
                                cities_list.add("Select City");
                                JSONArray cities = response.getJSONArray("cities");
                                for(int i=0;i<cities.length();i++)
                                    cities_list.add(cities.getString(i));

//                                String[] x = cities_list.toArray(new String[cities_list.size()]);
                                ArrayAdapter<String> adapter = new ArrayAdapter<>(OffersPage.this,
                                        android.R.layout.simple_spinner_item,cities_list);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner.setAdapter(adapter);


                            }
                            else{
                                Toast.makeText(getApplicationContext(),"This service is not operational yet",Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(OffersPage.this,Home.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }

                        } catch (JSONException e) {
                            Log.e("volley error",e.toString());
                            Intent intent = new Intent(OffersPage.this,Home.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
//                params.put(Constants.NAME,name.getText().toString());
                params.put("service",service);
//                params.put(Constants.PASSWORD, password.getText().toString());
//                params.put(Constants.PHONE, mobile_number.getText().toString());
//                params.put(Constants.CITY, city.getSelectedItem().toString());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //view pager
    public class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mResources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
            imageView.setImageResource(mResources[position]);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

}
