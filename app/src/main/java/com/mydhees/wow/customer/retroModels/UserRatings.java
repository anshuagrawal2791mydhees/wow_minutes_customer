
package com.mydhees.wow.customer.retroModels;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class UserRatings {

    @SerializedName("checkInProcedure")
    @Expose
    private Double checkInProcedure;
    @SerializedName("cleanliness")
    @Expose
    private Double cleanliness;
    @SerializedName("restaurantStaff")
    @Expose
    private Double restaurantStaff;
    @SerializedName("restaurantAmbiance")
    @Expose
    private Double restaurantAmbiance;
    @SerializedName("frontOfficeStaff")
    @Expose
    private Double frontOfficeStaff;
    @SerializedName("houseKeeping")
    @Expose
    private Double houseKeeping;
    @SerializedName("overall")
    @Expose
    private Double overall;
    @SerializedName("likelyToStayAgain")
    @Expose
    private Double likelyToStayAgain;

    /**
     * 
     * @return
     *     The checkInProcedure
     */
    public Double getCheckInProcedure() {
        return checkInProcedure;
    }

    /**
     * 
     * @param checkInProcedure
     *     The checkInProcedure
     */
    public void setCheckInProcedure(Double checkInProcedure) {
        this.checkInProcedure = checkInProcedure;
    }

    /**
     * 
     * @return
     *     The cleanliness
     */
    public Double getCleanliness() {
        return cleanliness;
    }

    /**
     * 
     * @param cleanliness
     *     The cleanliness
     */
    public void setCleanliness(Double cleanliness) {
        this.cleanliness = cleanliness;
    }

    /**
     * 
     * @return
     *     The restaurantStaff
     */
    public Double getRestaurantStaff() {
        return restaurantStaff;
    }

    /**
     * 
     * @param restaurantStaff
     *     The restaurantStaff
     */
    public void setRestaurantStaff(Double restaurantStaff) {
        this.restaurantStaff = restaurantStaff;
    }

    /**
     * 
     * @return
     *     The restaurantAmbiance
     */
    public Double getRestaurantAmbiance() {
        return restaurantAmbiance;
    }

    /**
     * 
     * @param restaurantAmbiance
     *     The restaurantAmbiance
     */
    public void setRestaurantAmbiance(Double restaurantAmbiance) {
        this.restaurantAmbiance = restaurantAmbiance;
    }

    /**
     * 
     * @return
     *     The frontOfficeStaff
     */
    public Double getFrontOfficeStaff() {
        return frontOfficeStaff;
    }

    /**
     * 
     * @param frontOfficeStaff
     *     The frontOfficeStaff
     */
    public void setFrontOfficeStaff(Double frontOfficeStaff) {
        this.frontOfficeStaff = frontOfficeStaff;
    }

    /**
     * 
     * @return
     *     The houseKeeping
     */
    public Double getHouseKeeping() {
        return houseKeeping;
    }

    /**
     * 
     * @param houseKeeping
     *     The houseKeeping
     */
    public void setHouseKeeping(Double houseKeeping) {
        this.houseKeeping = houseKeeping;
    }

    /**
     * 
     * @return
     *     The overall
     */
    public Double getOverall() {
        return overall;
    }

    /**
     * 
     * @param overall
     *     The overall
     */
    public void setOverall(Double overall) {
        this.overall = overall;
    }

    /**
     * 
     * @return
     *     The likelyToStayAgain
     */
    public Double getLikelyToStayAgain() {
        return likelyToStayAgain;
    }

    /**
     * 
     * @param likelyToStayAgain
     *     The likelyToStayAgain
     */
    public void setLikelyToStayAgain(Double likelyToStayAgain) {
        this.likelyToStayAgain = likelyToStayAgain;
    }

    public UserRatings() {
        super();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
