package com.mydhees.wow.customer.retroModels;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class OfferInfo {

    @SerializedName("ownerId")
    @Expose
    public String ownerId;
    @SerializedName("offerName")
    @Expose
    public String offerName;
    @SerializedName("modifiedBy")
    @Expose
    public String modifiedBy;
    @SerializedName("status")
    @Expose
    public boolean status;
    @SerializedName("__v")
    @Expose
    public int v;
    @SerializedName("modifiedAt")
    @Expose
    public String modifiedAt;
    @SerializedName("priceMRP")
    @Expose
    public int priceMRP;
    @SerializedName("checkIn")
    @Expose
    public String checkIn;
    @SerializedName("hotelName")
    @Expose
    public String hotelName;
    @SerializedName("_id")
    @Expose
    public String id;
    @SerializedName("wowPrice")
    @Expose
    public int wowPrice;
    @SerializedName("isVerified")
    @Expose
    public boolean isVerified;
    @SerializedName("createdAt")
    @Expose
    public String createdAt;
    @SerializedName("roomId")
    @Expose
    public String roomId;
    @SerializedName("noOfRooms")
    @Expose
    public int noOfRooms;
    @SerializedName("createdBy")
    @Expose
    public String createdBy;
    @SerializedName("roomType")
    @Expose
    public String roomType;
    @SerializedName("expiresAt")
    @Expose
    public String expiresAt;
    @SerializedName("scheduledAt")
    @Expose
    public String scheduledAt;
    @SerializedName("offerId")
    @Expose
    public String offerId;
    @SerializedName("hotelId")
    @Expose
    public String hotelId;
    @SerializedName("checkOut")
    @Expose
    public String checkOut;

    @Override
    public String toString() {
        return "OfferInfo{" +
                "ownerId='" + ownerId + '\'' +
                ", offerName='" + offerName + '\'' +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", status=" + status +
                ", v=" + v +
                ", modifiedAt='" + modifiedAt + '\'' +
                ", priceMRP=" + priceMRP +
                ", checkIn='" + checkIn + '\'' +
                ", hotelName='" + hotelName + '\'' +
                ", id='" + id + '\'' +
                ", wowPrice=" + wowPrice +
                ", isVerified=" + isVerified +
                ", createdAt='" + createdAt + '\'' +
                ", roomId='" + roomId + '\'' +
                ", noOfRooms=" + noOfRooms +
                ", createdBy='" + createdBy + '\'' +
                ", roomType='" + roomType + '\'' +
                ", expiresAt='" + expiresAt + '\'' +
                ", scheduledAt='" + scheduledAt + '\'' +
                ", offerId='" + offerId + '\'' +
                ", hotelId='" + hotelId + '\'' +
                ", checkOut='" + checkOut + '\'' +
                '}';
    }
}