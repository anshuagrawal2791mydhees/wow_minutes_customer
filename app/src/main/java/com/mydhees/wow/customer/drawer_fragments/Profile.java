package com.mydhees.wow.customer.drawer_fragments;


import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.mydhees.wow.customer.Constants;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.VolleySingleton;
import com.mydhees.wow.customer.objects.image;
import com.mydhees.wow.customer.utils.ImageCompressionAsyncTask;
import com.mydhees.wow.customer.utils.task;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
// random
public class Profile extends Fragment {
       Button edit,button_img;
       EditText name,dob,email,phone;
       SharedPreferences.Editor editor;
       CircularImageView prof_pic;

    // keep track of camera capture intent
    final int CAMERA_CAPTURE = 1;
    // keep track of cropping intent
    final int PIC_CROP = 2;
    Uri picUri;
    SharedPreferences profile;
    VolleySingleton volleySingleton;
    ImageLoader imageLoader;

    //SharedPreferences.Editor editor;


    public Profile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_profile, container, false);
        View v =inflater.inflate(R.layout.fragment_profile, container, false);

        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor = profile.edit();
        edit = (Button)v.findViewById(R.id.edit_button);
        edit.setVisibility(View.GONE);
        TextView name2 = (TextView)v.findViewById(R.id.textView7);
        name = (EditText)v.findViewById(R.id.name);
        dob = (EditText)v.findViewById(R.id.dob2);
        email = (EditText)v.findViewById(R.id.email2);
        phone = (EditText)v.findViewById(R.id.phone);
//        address = (EditText)v.findViewById(R.id.address);
        name.setEnabled(false);
        dob.setEnabled(false);
        email.setEnabled(false);
        phone.setEnabled(false);

        name.setText(profile.getString(Constants.NAME,"USER"));
        name2.setText(profile.getString(Constants.NAME,"USER"));
        dob.setText(profile.getString(Constants.DOB,"USER"));
        email.setText(profile.getString(Constants.EMAIL,"USER"));
        phone.setText(profile.getString(Constants.PHONE,"DEFAULT"));


        button_img = (Button)v.findViewById(R.id.button_img);
        prof_pic = (CircularImageView)v.findViewById(R.id.prof_pic3);

        if(!profile.getString(Constants.PROFILE_IMAGE_URI,"default").equals("default")){
            prof_pic.setImageBitmap(BitmapFactory.decodeFile(profile.getString(Constants.PROFILE_IMAGE_URI,"default")));
        }
        volleySingleton = VolleySingleton.getinstance(getContext());
        imageLoader = volleySingleton.getimageloader();
        if(!profile.getString(Constants.PROFILE_IMAGE_ID,"default").equals("default")){
            String url = "https://s3-us-west-2.amazonaws.com/wowimagesupload/resized/middle/"+profile.getString(Constants.PROFILE_IMAGE_ID,"default")+".jpg";
            imageLoader.get(url, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {

                    prof_pic.setImageBitmap(response.getBitmap());

                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("err",error.toString());

                }
            });
        }
        button_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, PIC_CROP);

//                    performCrop();
                    // use standard intent to capture an image
                    // Intent captureIntent = new Intent(
                    //MediaStore.ACTION_IMAGE_CAPTURE);
                    // we will handle the returned data in onActivityResult
                    //startActivityForResult(captureIntent, CAMERA_CAPTURE);
                } catch (ActivityNotFoundException anfe) {
                    // display an error message
                    String errorMessage = "Whoops - your device doesn't support capturing images!";
                    Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });







        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(name.isEnabled()==false)
                {
                    name.setEnabled(true);
                    //address.setEnabled(true);
                    //  phone.setEnabled(true);
                    dob.setEnabled(true);
                    phone.setEnabled(true);
                    email.setEnabled(true);


                    edit.setText("Save Details");
                    edit.setBackgroundColor(Color.GRAY);

                }
                else
                {

                    if(name.getText().toString().equals(""))
                        name.setError("Required");
                    else if (dob.getText().toString().equals(""))
                        dob.setError("Required");
//                    else if (address.getText().toString().equals(""))
//                        address.setError("Required");
                    else if (phone.getText().toString().equals(""))
                        phone.setError("Required");
                    else if (email.getText().toString().equals(""))
                        email.setError("Required");

                    else
                    {
                        editor.putString("company_name",name.getText().toString());
                       // editor.putString("address",address.getText().toString());
                        //editor.putString("phone",phone.getText().toString());
                        editor.putString("dob",dob.getText().toString());
                        editor.putString("email",email.getText().toString());
                        editor.putString("mobile",phone.getText().toString());


                        editor.commit();

                        name.setEnabled(false);
                       // address.setEnabled(false);
                        //  phone.setEnabled(false);
                        dob.setEnabled(false);
                        phone.setEnabled(false);
                        email.setEnabled(false);
                        //  display_name.setEnabled(false);
                        edit.setText("Edit Details");
                        edit.setBackgroundColor(Color.RED);



                    }

                }

            }
        });






        return v;

    }





    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK) {
            // user is returning from capturing an image using the camera
            if (requestCode == CAMERA_CAPTURE) {
                // get the Uri for the captured image
                //picUri = data.getData();
                // carry out the crop operation
                performCrop();
            }
            // user is returning from cropping the image
            else if (requestCode == PIC_CROP && data.getData() != null) {

                picUri = data.getData();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(),picUri);
                    //upload_image(bitmap);


                    final String filepath = new ImageCompressionAsyncTask(true,getContext()).execute(data.getDataString()).get();
                    Log.e("filepath",filepath);
//                                    Log.e("stringdata",data.getDataString());

                    image im = new image(profile.getString(Constants.NAME,"Wow"),filepath);

//                    images_uris.add(im);
                    ArrayList<image> images_uris = new ArrayList<>();
                    images_uris.add(im);
                    HashMap<String,String> extradata = new HashMap<>();
                    extradata.put("id",profile.getString(Constants.ID,"defualt"));

                    final ProgressDialog dialog = new ProgressDialog(getContext());
                    dialog.show();
                    dialog.setCancelable(false);
                    final Bitmap finalBitmap = bitmap;
                    task a = new task(getContext(),images_uris, Constants.UPLOAD_IMAGE,extradata){
                        @Override
                        protected void onPostExecute(Response response) {

                            dialog.dismiss();
                            try {
                                JSONObject resp = new JSONObject(response.body().string());
                                Log.e("resp",resp.toString());
                                if(resp.getString("res").equals("true")){
                                    prof_pic.setImageBitmap(BitmapFactory.decodeFile(filepath));
                                    editor.putString(Constants.PROFILE_IMAGE_ID,resp.getString("id"));
                                    editor.putString(Constants.PROFILE_IMAGE_URI,filepath);
                                    editor.commit();
                                    Toast.makeText(getContext(),"Updated",Toast.LENGTH_LONG).show();

                                }
                                else
                                    Toast.makeText(getContext(),"Failed",Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            super.onPostExecute(response);
                        }
                    };
                    a.execute();
                } catch (IOException e) {
                    Log.e("err",e.toString());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                // Log.d(TAG, String.valueOf(bitmap));

                // Log.d(TAG, String.valueOf(bitmap));

            }
            else {
                Log.e("here","here");
            }
        }
    }

    private void performCrop() {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent();
            // indicate image type and Uri
            cropIntent.setType("image/*");
            // set crop properties
            cropIntent.setAction(Intent.ACTION_GET_CONTENT);
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);

            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast
                    .makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }

    }


}
