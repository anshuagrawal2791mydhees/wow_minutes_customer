package com.mydhees.wow.customer.myWish;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.customer.Constants;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.VolleySingleton;
import com.mydhees.wow.customer.adapters.MyWishHListAdapter;
import com.mydhees.wow.customer.objects.offer_details;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class MyWishPageOne extends AppCompatActivity {


    Button plusNoRooms,minusNoRooms;
    Spinner spinner;
    Boolean error=false;

    RecyclerView recycler;
    ArrayList<MyWishHListAdapter> hotllist = new ArrayList<>();
    MyWishHListAdapter myadapter;
    LinearLayoutManager manager;

    LinearLayout llCateegoryHotel,llHotelList;

    EditText checkInDate,checkInTime,checkOutDate,checkOutTime,priceWish,maxprice;
    RadioGroup seriousness,cust_choice;
    RadioButton verySerious,serious,casual,testing;
    RadioButton hotels,categoryHotels,anything;
    String serious1="check";
    String checkouttimestring;
    Calendar c;
    DateFormat simpledateformat12= new SimpleDateFormat("dd-MM-yyyy");
    DateFormat simpledateformat1= new SimpleDateFormat("yyyy-MM-dd");
    DateFormat simpledateformat123= new SimpleDateFormat("HH:mm");
    TextView adults,children,tv;
    Button adultMinus,adultPlus,childMinus,childPlus;
    Button next,viewHotels;
    SeekBar myWishPrice,max;
    Button popular,review,mostWishFilled;
    RatingBar cust_choice_category;
    int category=0;
    int mYear;
    int status;
    int mMonth;
    int mDay;
    int mHour,mHour1;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;

    int mMinute,mMinute1;
    String checkindatestring,scheduleDatestring,scheduleTimestring,expiryTimestring="",expireStringDate;
    String checkoutdatestring;
    String checkintimestring;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wish_page_one);
        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        volleySingleton = VolleySingleton.getinstance(getApplicationContext());
        requestQueue = volleySingleton.getrequestqueue();

        plusNoRooms = (Button)findViewById(R.id.noRplus);
        minusNoRooms = (Button)findViewById(R.id.noRminus);

       // viewHotels = (Button)findViewById(R.id.viewHotels);

        popular = (Button)findViewById(R.id.popular);
        review = (Button)findViewById(R.id.reviews);
        mostWishFilled = (Button)findViewById(R.id.most_wish);

        next = (Button)findViewById(R.id.next);
        spinner = (Spinner)findViewById(R.id.select_city_spnr);

        adultMinus = (Button)findViewById(R.id.adult_minus);
        adultPlus = (Button)findViewById(R.id.adult_plus);
        childMinus = (Button)findViewById(R.id.child_minus);
        childPlus = (Button)findViewById(R.id.child_plus);
        myWishPrice = (SeekBar)findViewById(R.id.seekbar_mywish);
        max = (SeekBar)findViewById(R.id.seekbar1_mywish);


        checkInDate =(EditText)findViewById(R.id.checkin_dateWish);
        checkInTime =(EditText)findViewById(R.id.checkin_timeWish);
        checkOutDate =(EditText)findViewById(R.id.checkout_dateWish);
        checkOutTime =(EditText)findViewById(R.id.checkout_timeWish);

        priceWish =(EditText)findViewById(R.id.priceWish);
        maxprice =(EditText)findViewById(R.id.priceWish1);
        adults =(TextView)findViewById(R.id.no_adults);
        children =(TextView)findViewById(R.id.no_children);
        tv =(TextView)findViewById(R.id.tv);

        //seriousness
        seriousness = (RadioGroup)findViewById(R.id.seri_group);
        verySerious = (RadioButton)findViewById(R.id.seri_1);
        serious = (RadioButton)findViewById(R.id.seri_2);
        casual = (RadioButton)findViewById(R.id.seri_3);
        testing = (RadioButton)findViewById(R.id.seri_4);
        verySerious.performClick();

        //customer choice
        cust_choice = (RadioGroup)findViewById(R.id.cc_group);
        hotels = (RadioButton)findViewById(R.id.cc1);
        categoryHotels = (RadioButton)findViewById(R.id.cc2);
        anything = (RadioButton)findViewById(R.id.cc3);
        llCateegoryHotel = (LinearLayout)findViewById(R.id.llCateHotel);
        cust_choice_category = (RatingBar) findViewById(R.id.cust_choice_category);
       // llHotelList = (LinearLayout)findViewById(R.id.llHotelList);
       // llHotelList.setVisibility(View.GONE);

        //Hotel Recycler
        myadapter = new MyWishHListAdapter(this);
        recycler = (RecyclerView) findViewById(R.id.recyclerHList);
        recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recycler.setAdapter(myadapter);


        checkInDate.setKeyListener(null);
        checkInTime.setKeyListener(null);
        checkOutDate.setKeyListener(null);
        checkOutTime.setKeyListener(null);



//        viewHotels.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(MyWishPageOne.this,AllHotelWishList.class));
//            }
//        });


        priceWish.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(!priceWish.getText().toString().matches("")){
                    priceWish.setSelection(priceWish.getText().length());
                    myWishPrice.setProgress(Integer.parseInt(priceWish.getText().toString()));
                }

                else
                    myWishPrice.setProgress(0);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        maxprice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(!maxprice.getText().toString().matches("")){
                    maxprice.setSelection(maxprice.getText().length());
                    max.setProgress(Integer.parseInt(maxprice.getText().toString()));
                }
                else
                    max.setProgress(0);


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        plusNoRooms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int count= Integer.parseInt(tv.getText().toString());
                if(count==5){
                    Toast.makeText(getApplicationContext(), "Maximum is 5", Toast.LENGTH_SHORT).show();
                }
                else{
                    int count1 = count + 1;
                    String a= Integer.toString(count1);
                    tv.setText(a);
                }



            }
        });

        minusNoRooms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count= Integer.parseInt(tv.getText().toString());
                if(count<=1){

                    Toast.makeText(getApplicationContext(), "Minimum is 1", Toast.LENGTH_SHORT).show();

                }
                else {
                    int count1 = count - 1;
                    String a= Integer.toString(count1);
                    tv.setText(a);
                }
            }
        });

        anything.setChecked(true);

        //customer choice
        cust_choice_category.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                category=(int)v;
            }
        });

        cust_choice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                if (categoryHotels.isChecked() == true) {
                    llCateegoryHotel.setVisibility(View.VISIBLE);
                }else{
                    category=0;
                    llCateegoryHotel.setVisibility(View.GONE);
                }

//
//                if(hotels.isChecked() == true) {
//                    llHotelList.setVisibility(View.VISIBLE);
//                }else{
//                    llHotelList.setVisibility(View.GONE);
//                }

            }
        });





        hotels.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    startActivity(new Intent(MyWishPageOne.this,AllHotelWishList.class));
                 }
                    llCateegoryHotel.setVisibility(View.GONE);

            }
        });

        //select seriousness
        verySerious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serious1="Very Serious";
            }
        });

            serious.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    serious1="Serious";
                }
            });

        casual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serious1="Casual";
            }
        });
            testing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    serious1="testing";
                }
            });



//
//        popular.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(MyWishPageOne.this, AllHotelWishList.class));
//            }
//        });

        //city spinner
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.citiWish, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    gethotellist(spinner.getSelectedItem().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //toobar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


//        tv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                tv.setError(null);
//                show();
//            }
//        });


        myWishPrice.setMax(max.getProgress());
        myWishPrice.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                priceWish.setText(String.valueOf(progress));
                if(!(maxprice.getText().toString().equals(""))){
                    if(Integer.parseInt(maxprice.getText().toString()) < Integer.parseInt(priceWish.getText().toString())){
                     maxprice.setError("Change max price");
                        error=true;
                    }
                    else{
                        error=false;
                        maxprice.setError(null);
                    }
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        max.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                maxprice.setText(String.valueOf(progress));
                myWishPrice.setProgress(0);
                myWishPrice.setMax(progress);

                if(!(priceWish.getText().toString().equals(""))) {
                    if (Integer.parseInt(maxprice.getText().toString()) < Integer.parseInt(priceWish.getText().toString())) {
                        maxprice.setError("Max. price can't be less");
                        error = true;
                    } else {
                        error = false;
                        maxprice.setError(null);

                    }
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        adultPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int count= Integer.parseInt(adults.getText().toString());
                if(count==6){
                    Toast.makeText(getApplicationContext(), "Minimum is 1", Toast.LENGTH_SHORT).show();
                }
                else {
                    int count1 = count + 1;
                    String a = Integer.toString(count1);
                    adults.setText(a);
                }

            }
        });

        adultMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count= Integer.parseInt(adults.getText().toString());
                if(count==0){

                    Toast.makeText(getApplicationContext(), "Already 0!!! Can't be less.", Toast.LENGTH_SHORT).show();

                }
                else {
                    int count1 = count - 1;
                    String a= Integer.toString(count1);
                    adults.setText(a);
                }
            }
        });

        childPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count= Integer.parseInt(children.getText().toString());
                if(count==6){
                    Toast.makeText(getApplicationContext(), "Minimum is 1", Toast.LENGTH_SHORT).show();
                }
                else {
                    int count1 = count + 1;
                    String a = Integer.toString(count1);
                    children.setText(a);
                }
            }
        });

        childMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count= Integer.parseInt(children.getText().toString());
                if(count==0){

                    Toast.makeText(getApplicationContext(), "Already 0!!! Can't be less.", Toast.LENGTH_SHORT).show();

                }
                else {
                    int count1 = count - 1;
                    String a= Integer.toString(count1);
                    children.setText(a);

                }
            }
        });


        checkInDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                hideSoftKeyboard();

                // Get Current Date
                c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                checkInDate.setError(null);
                checkOutDate.setError(null);
                DatePickerDialog datePickerDialog = new DatePickerDialog(MyWishPageOne.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                //String month = (monthOfYear+1)+"";
                                String month = String.format("%02d", monthOfYear + 1);
                                String day = String.format("%02d", dayOfMonth);
                                // String day1 = String.format("%02d", dayOfMonth+1);

//                                    checkindatestring = day + "-" + month + "-" + year;
                                checkindatestring = year + "-" + month + "-" + day;

                                checkInDate.setText(day + "-" + month + "-" + year);
                                try {
                                    c.setTime(simpledateformat1.parse(checkindatestring));

                                    c.add(Calendar.DATE,1);
                                    checkoutdatestring = simpledateformat1.format(c.getTime());
                                    String preview_checkout=simpledateformat12.format(c.getTime());
                                    checkOutDate.setText(preview_checkout);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }



                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                //Toast.makeText(getContext(),mMonth+"",Toast.LENGTH_LONG).show();


            }



        });

        checkInTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                // Get Current Time
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                checkInTime.setError(null);
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(MyWishPageOne.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                String hour = String.format("%02d",hourOfDay);
                                String min = String.format("%02d",minute);
                                checkintimestring = hour + ":" + min;
                                checkInTime.setText(checkintimestring);

                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();

            }

        });


        checkOutDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideSoftKeyboard();
                // Get Current Date
                c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(MyWishPageOne.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String month = String.format("%02d",monthOfYear+1);
                                String day = String.format("%02d",dayOfMonth);

//                                    checkoutdatestring = day + "-" + month + "-" + year;
                                checkoutdatestring = year+"-"+month+"-"+day;
                                // checkoutdatestring = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                checkOutDate.setText(day+"-"+month+"-"+year);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }



        });

        checkOutTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                // Get Current Time
                c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                checkOutTime.setError(null);
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(MyWishPageOne.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                String hour = String.format("%02d",hourOfDay);
                                String min = String.format("%02d",minute);
                                checkouttimestring = hour + ":" + min;
                                checkOutTime.setText(checkouttimestring);

                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }

        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


    if(checkInDate.getText().toString().equals("")) {
        checkInDate.setError("Date");
        }
     if(checkInTime.getText().toString().equals("")){checkInTime.setError("Time"); error=true;}
     if(checkOutDate.getText().toString().equals("")){checkOutDate.setError("Date");error=true;}
     if(checkOutTime.getText().toString().equals("")){checkOutTime.setError("Time");error=true;}
     if(tv.getText().toString().equals("Click to add")){tv.setError("click to add");error=true;}
     if(max.getProgress()==0){ Toast.makeText(MyWishPageOne.this, "Please set max. price.", Toast.LENGTH_SHORT).show();error=true;}
     if(myWishPrice.getProgress()==0){ Toast.makeText(MyWishPageOne.this, "Please set min. price.", Toast.LENGTH_SHORT).show();error=true;}
if(spinner.getSelectedItem().toString().equals("Select City")){Toast.makeText(MyWishPageOne.this, "Select city", Toast.LENGTH_SHORT).show();error=true;}

if(serious1.equals("check")){Toast.makeText(MyWishPageOne.this, "Select your seriousness.", Toast.LENGTH_SHORT).show();error=true;}


                Log.e("error",error.toString());
        if(!error){
            checkInDate.setError(null);
            checkOutTime.setError(null);
            checkOutDate.setError(null);
            checkInTime.setError(null);
            tv.setError(null);
    Intent it = new Intent(MyWishPageOne.this, MyWishConfirm.class);
            it.putExtra("city",spinner.getSelectedItem().toString());
            it.putExtra("cindate",checkInDate.getText().toString());
            it.putExtra("cintime",checkInTime.getText().toString());
            it.putExtra("couttime",checkOutTime.getText().toString());
            it.putExtra("coutdate",checkOutDate.getText().toString());
            it.putExtra("rooms",tv.getText().toString());
            it.putExtra("minPrice",priceWish.getText().toString());
            it.putExtra("maxPrice",maxprice.getText().toString());
            it.putExtra("adults",adults.getText().toString());
            it.putExtra("child",children.getText().toString());
            it.putExtra("checkoutdatestring",checkoutdatestring+"T"+checkouttimestring);
            it.putExtra("checkindatestring",checkindatestring+"T"+checkintimestring);
            it.putExtra("serious",serious1);
            it.putExtra("category",category+"");
    startActivity(it);
}



            }
        });








    }

    private void hideSoftKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//            focusOnView(policy1);
        }


    }

    public void show()
    {

        final Dialog d = new Dialog(MyWishPageOne.this);
        d.setTitle("NumberPicker");
        d.setContentView(R.layout.dialog);

        Button b1 = (Button) d.findViewById(R.id.button1);
        Button b2 = (Button) d.findViewById(R.id.button2);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        np.setMaxValue(100); // max value 100
        np.setMinValue(0);   // min value 0
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // Save the value in the number picker
                np.setValue(newVal);
            }
        });;
        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                tv.setText(String.valueOf(np.getValue())); //set the value to textview
                d.dismiss();
            }
        });
        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
            }
        });
        d.show();


    }



    private void gethotellist(final String city) throws JSONException {
          StringRequest request = new StringRequest(Request.Method.POST, Constants.HOTEL_LIST,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String respons) {
                        JSONObject response = null;
                        try {

                            response = new JSONObject(respons);
                            Log.e("confirm",response.toString());
                            Log.e("con",city);
                            if(response.getBoolean("res")){

                                JSONArray data=response.getJSONArray("hotels");

                                myadapter.clearAll();
                                if (data.length()>0) {
                                    recycler.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject rec = data.getJSONObject(i);
                                        myadapter.history(new offer_details(rec.getString("hotelName"),rec.getString("location"),rec.getString("hotelId")));

                                    }
                                }else{
                                    recycler.setVisibility(View.GONE);
//                                    myadapter.history(new offer_details("No Data found.","",""));

                                }


                            }

                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Internet error",Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("city",city);

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    //For back button in toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
