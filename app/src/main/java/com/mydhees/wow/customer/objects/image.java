package com.mydhees.wow.customer.objects;

/**
 * Created by anshu on 19/05/16.
 */
public class image {
    String title;
    String url;

    public image(String title, String url) {
        this.title = title;
        this.url = url;
    }
    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
