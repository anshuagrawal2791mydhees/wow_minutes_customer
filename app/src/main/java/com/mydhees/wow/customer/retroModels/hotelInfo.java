package com.mydhees.wow.customer.retroModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class hotelInfo {

    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("address_line1")
    @Expose
    private String addressLine1;
    @SerializedName("address_line2")
    @Expose
    private String addressLine2;
    @SerializedName("starHotel")
    @Expose
    private Integer starHotel;
    @SerializedName("numberOfReviews")
    @Expose
    private Integer numberOfReviews;
    @SerializedName("userRatings")
    @Expose
    private UserRatings userRatings;

    /**
     *
     * @return
     *     The address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     *     The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     *     The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     *     The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     *     The addressLine1
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     *
     * @param addressLine1
     *     The address_line1
     */
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    /**
     *
     * @return
     *     The addressLine2
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     *
     * @param addressLine2
     *     The address_line2
     */
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    /**
     *
     * @return
     *     The starHotel
     */
    public Integer getStarHotel() {
        return starHotel;
    }

    /**
     *
     * @param starHotel
     *     The starHotel
     */
    public void setStarHotel(Integer starHotel) {
        this.starHotel = starHotel;
    }

    /**
     *
     * @return
     *     The numberOfReviews
     */
    public Integer getNumberOfReviews() {
        return numberOfReviews;
    }

    /**
     *
     * @param numberOfReviews
     *     The numberOfReviews
     */
    public void setNumberOfReviews(Integer numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }

    /**
     *
     * @return
     *     The userRatings
     */
    public UserRatings getUserRatings() {
        return userRatings;
    }

    /**
     *
     * @param userRatings
     *     The userRatings
     */
    public void setUserRatings(UserRatings userRatings) {
        this.userRatings = userRatings;
    }

    @Override
    public String toString() {
        return "hotelInfo{" +
                "address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", starHotel=" + starHotel +
                ", numberOfReviews=" + numberOfReviews +
                ", userRatings=" + userRatings +
                '}';
    }
}