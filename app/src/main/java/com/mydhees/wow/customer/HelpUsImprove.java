package com.mydhees.wow.customer;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

public class HelpUsImprove extends AppCompatActivity {
    private Toolbar toolbar;
    Button send_btn;
    EditText feedbk_txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_us_improve);

        send_btn = (Button)findViewById(R.id.send_feedback);
        feedbk_txt = (EditText)findViewById(R.id.feedbk_txt);

        toolbar = (Toolbar)findViewById(R.id.toolbar_help);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.help_menu_appbar, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.send_feedback) {


            return true;
        }

        //for backbutton
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:

        }


        return super.onOptionsItemSelected(item);


    }

}

