package com.mydhees.wow.customer.objects;

/**
 * Created by anshu on 22/08/16.
 */
public class Review {
    private String review;
    private int rating;
    private String Name;

    public Review(String review, int rating, String name) {
        this.review = review;
        this.rating = rating;
        Name = name;
    }

    public String getReview() {
        return review;
    }

    public int getRating() {
        return rating;
    }

    public String getName() {
        return Name;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setName(String name) {
        Name = name;
    }
}
