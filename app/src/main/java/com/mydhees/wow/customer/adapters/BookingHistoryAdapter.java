package com.mydhees.wow.customer.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.customer.Constants;
import com.mydhees.wow.customer.Feedback;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.VolleySingleton;
import com.mydhees.wow.customer.objects.offer_details;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Abhishek on 23-Jun-16.
 */

public class BookingHistoryAdapter extends RecyclerView.Adapter<BookingHistoryAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<offer_details> History = new ArrayList<>();
    DateFormat parser2 = new SimpleDateFormat("dd-MM-yyyy");
    DateFormat parser3 = new SimpleDateFormat("HH:mm");
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;
    ArrayList<String> feedback_data = new ArrayList<>();

    public BookingHistoryAdapter (Context context){
        this.context=context;
        layoutInflater = layoutInflater.from(context);
        volleySingleton = VolleySingleton.getinstance(context);
        requestQueue = volleySingleton.getrequestqueue();
    }

    public void history(offer_details history){
        History.add(history);
        notifyItemInserted(History.size());
    }
    public void addAll(ArrayList<offer_details> list){
        History.clear();
        History.addAll(list);
        notifyDataSetChanged();
    }

    public ArrayList<offer_details> getHistorylist()
    {
        return History;
    }
    public void clearAll(){
        History.clear();
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.booking_history_row,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
        offer_details bookingHistory = History.get(position);
        holder.hotel_name.setText(bookingHistory.getHotel_name()+"");
        //holder.hotel_address.setText(bookingHistory.getAddress()+"");
        final Date checkIn = bookingHistory.getCheckin();
        Date checkOut = bookingHistory.getCheck_out();


        holder.checkin_date.setText(parser2.format(checkIn));
        holder.checkout_date.setText(parser2.format(checkOut));
        holder.offer_price.setText(bookingHistory.getOffer_price()+"");
        holder.checkin_time.setText(parser3.format(checkIn));
        holder.checkout_time.setText(parser3.format(checkOut));
        if(bookingHistory.getStatus().equals("CHARGED")){
            holder.status.setText("Success");
            holder.status.setTextColor(Color.GREEN);
        }
        else{
            holder.status.setText("Failed");
            holder.status.setTextColor(Color.RED);
        }
        holder.booking_id.setText(bookingHistory.getOrder_id());

        holder.write_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String review_details = feedback_data.get(position);
                Intent intent = new Intent(context, Feedback.class);
                String fin = "{ details:"+review_details+"}";
                intent.putExtra("details",fin);
                context.startActivity(intent);
            }
        });

        holder.address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog dialog = new ProgressDialog(context);
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        requestQueue.cancelAll("request");
                    }
                });
                dialog.show();
                StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_HOTEL_ADDRESS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.e("res",response);
                        try {
                            JSONObject resp = new JSONObject(response);
                            Log.e("address",resp.getString("address"));
                            String map = "http://maps.google.co.in/maps?q=" + resp.getString(Constants.ADDRESS);
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                            context.startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Toast.makeText(context,"Error",Toast.LENGTH_LONG).show();
                        Log.e("err",error.toString());
                    }
                }){
                    @Override
                    protected Map<String,String> getParams(){
                        Map<String,String> params = new HashMap<String, String>();
                        params.put(Constants.HOTELID,History.get(position).getHotelId());
                        return params;
                    }
                };

                request.setTag("request");
                request.setRetryPolicy(new DefaultRetryPolicy(
                        10000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                requestQueue.add(request);

            }
        });

    }

    @Override
    public int getItemCount() {
        return History.size();
    }

    public void addFeedbackData(ArrayList<String> feedback_data) {
        this.feedback_data.clear();
        Log.e("feedback_size",feedback_data.size()+"");
        this.feedback_data.addAll(feedback_data);
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView hotel_name,hotel_address,checkin_time,checkin_date,checkout_time,checkout_date,offer_price,status,booking_id,write_review;
        Button address;


        public RecyclerViewHolder(View itemView) {
            super(itemView);
            hotel_name = (TextView)itemView.findViewById(R.id.hotel_name);
          //  hotel_address = (TextView)itemView.findViewById(R.id.hotel_address);
            checkin_time = (TextView)itemView.findViewById(R.id.checkin_time);
            checkin_date = (TextView)itemView.findViewById(R.id.checkin_date);
            checkout_time = (TextView)itemView.findViewById(R.id.checkout_time);
            checkout_date = (TextView)itemView.findViewById(R.id.checkout_date);
            offer_price = (TextView)itemView.findViewById(R.id.offer_price2);
            status = (TextView)itemView.findViewById(R.id.status);
           address = (Button)itemView.findViewById(R.id.address);
            booking_id = (TextView)itemView.findViewById(R.id.booking_id);
            write_review = (TextView)itemView.findViewById(R.id.write_review);



        }
    }
}
