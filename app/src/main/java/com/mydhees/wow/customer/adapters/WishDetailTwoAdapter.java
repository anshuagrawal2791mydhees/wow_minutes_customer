package com.mydhees.wow.customer.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.WishDetailsTwo;
import com.mydhees.wow.customer.myWish.WishConfirmReply;
import com.mydhees.wow.customer.objects.WishDetailObjectTwo;

import java.util.ArrayList;


public class WishDetailTwoAdapter extends RecyclerView.Adapter<WishDetailTwoAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<WishDetailObjectTwo> DetailListtwo = new ArrayList<>();


    public WishDetailTwoAdapter(Context context){
        this.context=context;
        layoutInflater = layoutInflater.from(context);
    }

    public void history(WishDetailObjectTwo history){
        DetailListtwo.add(history);
        notifyItemInserted(DetailListtwo.size());
    }

    public ArrayList<WishDetailObjectTwo> getDetaillist()
    {
        return DetailListtwo;
    }
    public void clearAll(){
        DetailListtwo.clear();
    }
    public void addAll(ArrayList<WishDetailObjectTwo> list){
        DetailListtwo.clear();
        DetailListtwo.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wish_details_row_two,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final WishDetailObjectTwo wishList = DetailListtwo.get(position);
        holder.hotelName.setText(wishList.getHotelName()+"");
        holder.location.setText(wishList.getLocation()+"");
        holder.priceOffered.setText(wishList.getPrice()+"");
        holder.message.setText(wishList.getMessage()+"");
        holder.rtype.setText(wishList.getType()+"");

        if(wishList.getStatus().equals("Live")) {
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent start1 = new Intent(context, WishConfirmReply.class);

                    start1.putExtra("wishId", wishList.getWishid());
                    start1.putExtra("checkInDate", wishList.getCheckIn());
                    start1.putExtra("checkOutDate", wishList.getDate_check_out());
                    start1.putExtra("cOutTime", wishList.getOtime());
                    start1.putExtra("cInTime", wishList.getCtime());
                    start1.putExtra("hotelId", wishList.getHotelid());
                    start1.putExtra("hotelName", wishList.getHotelName());
                    start1.putExtra("replyId", wishList.getReplyid());
                    start1.putExtra("address", wishList.getLocation());
                    start1.putExtra("price", wishList.getPrice());
                    start1.putExtra("adults", wishList.getAdults());
                    start1.putExtra("child", wishList.getChild());
                    start1.putExtra("room", wishList.getRooms());
                    start1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(start1);

                }
            });

        }
        else{
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    holder.confirm.setText("Wish Expired");
                    holder.confirm.setBackgroundColor(Color.GRAY);
                }
                });
        }
    }

    @Override
    public int getItemCount() {
        return DetailListtwo.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{

       TextView hotelName,location;
        EditText priceOffered,message,rtype;
        Button confirm;

        public RecyclerViewHolder(View itemView) {
            super(itemView);

            hotelName = (TextView)itemView.findViewById(R.id.hotel_nametwo);
            location = (TextView)itemView.findViewById(R.id.hotel_addresstwo);
            rtype = (EditText) itemView.findViewById(R.id.rtype);
            priceOffered = (EditText)itemView.findViewById(R.id.price_two);
            message = (EditText)itemView.findViewById(R.id.message1);
            confirm = (Button) itemView.findViewById(R.id.confirm);





        }
    }
}
