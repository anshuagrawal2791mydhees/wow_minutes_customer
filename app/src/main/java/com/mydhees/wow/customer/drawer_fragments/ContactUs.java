package com.mydhees.wow.customer.drawer_fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.customer.Constants;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUs extends Fragment {
    EditText name,email,message;
    TextView tx2,tx3;
    SharedPreferences profile;
    Button submit;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;



    public ContactUs() {
        // Required empty public constructor
//        jk
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_contact_us, container, false);
        View v =inflater.inflate(R.layout.fragment_contact_us, container, false);
        hideKeyboard(getContext());
        name = (EditText)v.findViewById(R.id.nameCS);
        email = (EditText)v.findViewById(R.id.emailCS);
        message = (EditText)v.findViewById(R.id.message);
        submit = (Button)v.findViewById(R.id.submitCS);
        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,Context.MODE_PRIVATE);
        volleySingleton = VolleySingleton.getinstance(getContext());
        requestQueue = volleySingleton.getrequestqueue();


        name.setText(profile.getString(Constants.NAME," "));
        email.setText(profile.getString(Constants.EMAIL," "));

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.getText().toString().matches(" ")||email.getText().toString().matches(" ")||message.getText().toString().matches(" "))
                    Toast.makeText(getContext()," Fill in the missing details", Toast.LENGTH_LONG).show();
                else{
                    final ProgressDialog dialog = new ProgressDialog(getContext());
                    dialog.show();
                    dialog.setCancelable(false);
                    StringRequest request = new StringRequest(Request.Method.POST, Constants.CONTACT_US, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            dialog.dismiss();

                            Log.e("res",response);
                            try {
                                JSONObject resp = new JSONObject(response);

                                Toast.makeText(getContext(),resp.getString("response"),Toast.LENGTH_LONG).show();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            dialog.dismiss();
                            Log.e("Err",error.toString());
                            Toast.makeText(getContext(),"Error",Toast.LENGTH_LONG).show();

                        }
                    }){

                        @Override
                        protected Map<String,String> getParams(){
                            Map<String,String> params = new HashMap<String, String>();
                            params.put(Constants.EMAIL,email.getText().toString());
                            params.put(Constants.NAME,name.getText().toString());
                            params.put("ownerId",profile.getString(Constants.ID,"default"));
                            params.put("message",message.getText().toString());
                            return params;
                        }
                    };
                    request.setRetryPolicy(new DefaultRetryPolicy(
                            10000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(request);

                }
            }
        });



        tx2 = (TextView)v.findViewById(R.id.tx2);
        tx3 = (TextView)v.findViewById(R.id.tx3);


        String text = " <font color='@color/accent'>Address :</font><font color='@color/black'> Mydhees Technology Pvt. Ltd. \n" +
                "Ground Floor, Beech, E-1 Manyata Embassy Business Park, Outer Ring Rd, Nagawara, Bengaluru, \n" +
                "Karnataka-560045 </font>.";
        tx2.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        String text2 = " <font color='@color/accent'>Contact :</font><font color='@color/black'> 080 - 42764711</font>.";
        tx3.setText(Html.fromHtml(text2),TextView.BufferType.SPANNABLE);





        return v;
    }
    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
