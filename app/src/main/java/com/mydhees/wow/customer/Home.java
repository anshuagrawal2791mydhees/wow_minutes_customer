package com.mydhees.wow.customer;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.mydhees.wow.customer.drawer_fragments.Account;
import com.mydhees.wow.customer.drawer_fragments.BookingHistory;
import com.mydhees.wow.customer.drawer_fragments.ContactUs;
import com.mydhees.wow.customer.drawer_fragments.Credits;
import com.mydhees.wow.customer.drawer_fragments.DashBoard;
import com.mydhees.wow.customer.drawer_fragments.Profile;
import com.mydhees.wow.customer.drawer_fragments.WishReview;
import com.mydhees.wow.customer.registrationAndLogin.first_screen;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    Fragment fragment;
    TextView name,ema;
    ImageView prof_pic;
    View headerView;
    SharedPreferences profile;
    String title = "";
    SharedPreferences.Editor editor;
    int prev_id = R.id.dash_board; // varible to save the id of the previously selected item in the navigation view
    NavigationView navigationView;
    private Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences(
                Constants.LAUNCH_TIME_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor = prefs.edit();
        profile = getApplicationContext().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,Context.MODE_PRIVATE);
        if (!prefs.getBoolean(Constants.FIRST_TIME,false)) {
            // <---- run your one time code here
            Intent intent = new Intent(this,first_screen.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(intent);


        }



        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView,new DashBoard()).commit();



        mContext = this;




        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.inflateHeaderView(R.layout.nav_header_home);

        navigationView.setNavigationItemSelectedListener(this);
        name = (TextView)headerView.findViewById(R.id.name1);
        //prof_pic = (ImageView)headerView.findViewById(R.id.prof_pic);
//        companyName.setText("new_text");
        ema = (TextView)headerView.findViewById(R.id.email3);
        prof_pic = (ImageView)headerView.findViewById(R.id.prof_pic2);

//        email.setText("new text");


        name.setText(profile.getString(Constants.NAME,"Mydhees Technology Pvt Ltd"));
        ema.setText(profile.getString(Constants.EMAIL,"example@example.com"));
        if(!profile.getString(Constants.PROFILE_IMAGE_URI,"default").equals("default"))
            prof_pic.setImageBitmap(BitmapFactory.decodeFile(profile.getString(Constants.PROFILE_IMAGE_URI,"default")));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                name.setText(profile.getString(Constants.NAME,"Mydhees Technology Pvt Ltd"));
                ema.setText(profile.getString(Constants.EMAIL,"example@example.com"));
                if(!profile.getString(Constants.PROFILE_IMAGE_URI,"default").equals("default"))
                    prof_pic.setImageBitmap(BitmapFactory.decodeFile(profile.getString(Constants.PROFILE_IMAGE_URI,"default")));


            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        // added by Pranav - minor fix - shows dash board as selected initially
        navigationView.setCheckedItem(R.id.dash_board);
        title = "WOW Minutes";
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);



        return true;

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//              startActivity(new Intent(Home.this,OffersPage.class));
////            Toast.makeText(getApplicationContext(),"this is settings",Toast.LENGTH_LONG);
//            return true;
//        }


        if (id == R.id.shareapp) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Hey! Download an awesome app\n*WOW Minutes* and get exciting offers for Hotels,Bus,Restaurants,Paying Guest and may more.... Try it now by clicking here http://www.mydhees.com/index.php ";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
            return true;
        }
        if(id==R.id.help){
            startActivity(new Intent(Home.this,HelpUsImprove.class));
        }
        if(id==R.id.inviteEarn){
            startActivity(new Intent(Home.this,InviteAndEarn.class));

        }

        if(id==R.id.acion_se){
            editor.putBoolean(Constants.FIRST_TIME,false);
            editor.commit();

            Intent intent = new Intent(Home.this, first_screen.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        }
        return super.onOptionsItemSelected(item);






    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Log.e("df",id+"");


        if (id == R.id.dash_board) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new DashBoard()).commit();
            title="WOW Minutes";
            prev_id = id;
            Log.e("df",id+"");

        } else if (id == R.id.profile) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new Profile()).commit();
            title="Profile";
            prev_id = id;
        }

        else if (id == R.id.credits) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new Credits()).commit();
            title="Credits";
            prev_id = id;
        }

        else if (id == R.id.wish_conf) {
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new WishReview()).commit();
            title="Your Wish Review";
            prev_id = id;
        }


        else if (id == R.id.booking_history) {

            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new BookingHistory()).commit();
            title="Booking History";
            prev_id = id;


        }
        else if (id == R.id.back) {

            // Edited by Pranav
            /*mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new Account()).commit();
            title="Logout";*/
            final SharedPreferences[] profile = new SharedPreferences[1];
            final SharedPreferences[] firstTime = new SharedPreferences[1];
            new AlertDialog.Builder(mContext)
                    .setTitle("Confirm Logout")
                    .setMessage("Are you sure you wnat to log out ?")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // logout
                            firstTime[0] = getSharedPreferences(Constants.LAUNCH_TIME_PREFERENCE_FILE, Context.MODE_PRIVATE);
                            profile[0] = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor_firstTime = firstTime[0].edit();
                            SharedPreferences.Editor editor_profile = profile[0].edit();
                            editor_firstTime.putBoolean(Constants.FIRST_TIME,false);
                            editor_firstTime.apply();
                            editor_profile.clear();
                            editor_profile.apply();
                            LoginManager.getInstance().logOut();
                            Intent intent = new Intent(mContext,first_screen.class );
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // set the checked item to the current fragment selection
                            navigationView.setCheckedItem(prev_id);
                        }
                    })
                    .setCancelable(false)
                    .show();

        }
        else if (id == R.id.contactUs) {

            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new ContactUs()).commit();
            title="Contact us";
            prev_id = id;


        }
        else if(id == R.id.nav_share)
        {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Hey! Download an awesome app\n*WOW Minutes* and get exciting offers for Hotels,Bus,Restaurants,Paying Guest and may more.... Try it now by clicking here http://www.mydhees.com/index.php ";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
            prev_id = id;
        }
        getSupportActionBar().setTitle(title);

//        else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
