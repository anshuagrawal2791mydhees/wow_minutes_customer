package com.mydhees.wow.customer.retroModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anshu on 01/07/16.
 */
public class OfferInfo2 {

    @SerializedName("ownerId")
    @Expose
    private String ownerId;
    @SerializedName("offerName")
    @Expose
    private String offerName;
    @SerializedName("priceMRP")
    @Expose
    private Integer priceMRP;
    @SerializedName("checkIn")
    @Expose
    private String checkIn;
    @SerializedName("hotelName")
    @Expose
    private String hotelName;
    @SerializedName("wowPrice")
    @Expose
    private Integer wowPrice;
    @SerializedName("roomId")
    @Expose
    private String roomId;
    @SerializedName("noOfRooms")
    @Expose
    private Integer noOfRooms;
    @SerializedName("roomType")
    @Expose
    private String roomType;
    @SerializedName("expiresAt")
    @Expose
    private String expiresAt;
    @SerializedName("scheduledAt")
    @Expose
    private String scheduledAt;
    @SerializedName("offerId")
    @Expose
    private String offerId;
    @SerializedName("hotelId")
    @Expose
    private String hotelId;
    @SerializedName("checkOut")
    @Expose
    private String checkOut;

    /**
     *
     * @return
     * The ownerId
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     *
     * @param ownerId
     * The ownerId
     */
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    /**
     *
     * @return
     * The offerName
     */
    public String getOfferName() {
        return offerName;
    }

    /**
     *
     * @param offerName
     * The offerName
     */
    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    /**
     *
     * @return
     * The priceMRP
     */
    public Integer getPriceMRP() {
        return priceMRP;
    }

    /**
     *
     * @param priceMRP
     * The priceMRP
     */
    public void setPriceMRP(Integer priceMRP) {
        this.priceMRP = priceMRP;
    }

    /**
     *
     * @return
     * The checkIn
     */
    public String getCheckIn() {
        return checkIn;
    }

    /**
     *
     * @param checkIn
     * The checkIn
     */
    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    /**
     *
     * @return
     * The hotelName
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     *
     * @param hotelName
     * The hotelName
     */
    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    /**
     *
     * @return
     * The wowPrice
     */
    public Integer getWowPrice() {
        return wowPrice;
    }

    /**
     *
     * @param wowPrice
     * The wowPrice
     */
    public void setWowPrice(Integer wowPrice) {
        this.wowPrice = wowPrice;
    }

    /**
     *
     * @return
     * The roomId
     */
    public String getRoomId() {
        return roomId;
    }

    /**
     *
     * @param roomId
     * The roomId
     */
    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    /**
     *
     * @return
     * The noOfRooms
     */
    public Integer getNoOfRooms() {
        return noOfRooms;
    }

    /**
     *
     * @param noOfRooms
     * The noOfRooms
     */
    public void setNoOfRooms(Integer noOfRooms) {
        this.noOfRooms = noOfRooms;
    }

    /**
     *
     * @return
     * The roomType
     */
    public String getRoomType() {
        return roomType;
    }

    /**
     *
     * @param roomType
     * The roomType
     */
    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    /**
     *
     * @return
     * The expiresAt
     */
    public String getExpiresAt() {
        return expiresAt;
    }

    /**
     *
     * @param expiresAt
     * The expiresAt
     */
    public void setExpiresAt(String expiresAt) {
        this.expiresAt = expiresAt;
    }

    /**
     *
     * @return
     * The scheduledAt
     */
    public String getScheduledAt() {
        return scheduledAt;
    }

    /**
     *
     * @param scheduledAt
     * The scheduledAt
     */
    public void setScheduledAt(String scheduledAt) {
        this.scheduledAt = scheduledAt;
    }

    /**
     *
     * @return
     * The offerId
     */
    public String getOfferId() {
        return offerId;
    }

    /**
     *
     * @param offerId
     * The offerId
     */
    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    /**
     *
     * @return
     * The hotelId
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     *
     * @param hotelId
     * The hotelId
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    /**
     *
     * @return
     * The checkOut
     */
    public String getCheckOut() {
        return checkOut;
    }

    /**
     *
     * @param checkOut
     * The checkOut
     */
    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    @Override
    public String toString() {
        return "OfferInfo2{" +
                "ownerId='" + ownerId + '\'' +
                ", offerName='" + offerName + '\'' +
                ", priceMRP=" + priceMRP +
                ", checkIn='" + checkIn + '\'' +
                ", hotelName='" + hotelName + '\'' +
                ", wowPrice=" + wowPrice +
                ", roomId='" + roomId + '\'' +
                ", noOfRooms=" + noOfRooms +
                ", roomType='" + roomType + '\'' +
                ", expiresAt='" + expiresAt + '\'' +
                ", scheduledAt='" + scheduledAt + '\'' +
                ", offerId='" + offerId + '\'' +
                ", hotelId='" + hotelId + '\'' +
                ", checkOut='" + checkOut + '\'' +
                '}';
    }
}
