package com.mydhees.wow.customer.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.mydhees.wow.customer.objects.image;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by anshu on 16/06/16.
 */
public class task extends AsyncTask<Void,Void, Response> {
    OkHttpClient client = new OkHttpClient.Builder() .connectTimeout(800, TimeUnit.SECONDS)
            .writeTimeout(800, TimeUnit.SECONDS)
            .readTimeout(800, TimeUnit.SECONDS)
            .build();


    RequestBody requestBody;
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/jpeg");
    Context context;
    String url;
    HashMap<String, String> data = new HashMap<>();
    ArrayList<image> images = new ArrayList<>();

    Response response;
    public task(Context context, ArrayList<image> images, String url, HashMap<String,String> data) {
        this.context = context;
        this.images.addAll(images);
        this.url= url;
        this.data.putAll(data);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected Response doInBackground(Void... params) {
//        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
//        byte[] byteArray = stream.toByteArray();

        Log.e("data",data.toString());

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        for (HashMap.Entry<String, String> entry : data.entrySet())
        {
            builder.addFormDataPart(entry.getKey(),entry.getValue());

            //System.out.println(entry.getKey() + "/" + entry.getValue());
        }
        Log.e("data",data.toString());

//                MultipartBody.Builder builder2= new MultipartBody.Builder()
//                .setType(MultipartBody.FORM)
//                .addFormDataPart("ownerId", "US-BNG-560045-BytZL50N")
//                .addFormDataPart("hotelName","Taa")
//                .addFormDataPart("hotelType","cottage")
//                .addFormDataPart("noOfRooms","200")
//                .addFormDataPart("phoneNumber","00000000")
//                .addFormDataPart("mobileNumber","34343")
//                .addFormDataPart("address_line1","line1")
//                .addFormDataPart("address_line2","line_2")
//                .addFormDataPart("address","dkjfdlsf")
//                .addFormDataPart("pin","305001")
//                .addFormDataPart("longitude","12")
//                .addFormDataPart("latitude","13")
//                .addFormDataPart("city","Bangalore")
//                .addFormDataPart("facilities","{\n" +
//                        "      \"hasWifi\": true,\n" +
//                        "      \"hasDining\": true,\n" +
//                        "      \"hasTv\": false,\n" +
//                        "      \"hasGym\": true,\n" +
//                        "      \"hasBusinessService\": false,\n" +
//                        "      \"hasCoffeeShop\": true,\n" +
//                        "      \"hasFrontDesk\": false,\n" +
//                        "      \"hasSpa\": true,\n" +
//                        "      \"hasLaundry\": false,\n" +
//                        "      \"hasOutDoorgames\": false,\n" +
//                        "      \"hasParking\": false,\n" +
//                        "      \"hasRoomService\": true,\n" +
//                        "      \"hasSwimming\": true,\n" +
//                        "      \"hasTravelAssistance\": false\n" +
//                        "    }")
//                .addFormDataPart("starHotel","4")
//                        //.addFormDataPart("description","[\"screenshot\",\"don't know\",\"me\"]")
//                .addFormDataPart("policies","[\"dffdfd\",\"fdfdfd\"]");

        JSONArray descriptions = new JSONArray();
                for(int i=0;i<images.size();i++)
                {
                    try {
                        descriptions.put(i,images.get(i).getTitle());
                    } catch (JSONException e) {
                        Toast.makeText(context,"Problem in connecting",Toast.LENGTH_LONG).show();
                    }

                    builder.addFormDataPart("photos",i+"", RequestBody.create(MEDIA_TYPE_PNG,new File(images.get(i).getUrl())));
                }
        if(images.size()>0)
        builder.addFormDataPart("description",descriptions.toString());
        requestBody = builder.build();

        Request request = new Request.Builder()

                .url(url)
                .post(requestBody)
                .build();

        try {
            response = client.newCall(request).execute();
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

            //System.out.println(response.body().string());
            //Log.e("server response",response.body().string());
        }
        catch (Exception e){
            Log.e("dfd",e.toString());
        }
        return response;
    }


}
