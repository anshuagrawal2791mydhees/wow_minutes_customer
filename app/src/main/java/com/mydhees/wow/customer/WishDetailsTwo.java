package com.mydhees.wow.customer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.customer.adapters.WishDetailListAdapter;
import com.mydhees.wow.customer.adapters.WishDetailTwoAdapter;
import com.mydhees.wow.customer.objects.WishDetailObject;
import com.mydhees.wow.customer.objects.WishDetailObjectTwo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WishDetailsTwo extends AppCompatActivity {
    RecyclerView recycler;
    ArrayList<WishDetailListAdapter> wish = new ArrayList<>();
    WishDetailTwoAdapter myadapter;
    LinearLayoutManager manager;
    String wishId,cdate,odate,ctime,otime,adult,child,room,status;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_details_two);

        recycler = (RecyclerView)findViewById(R.id.recyclerWRT);
        myadapter=new WishDetailTwoAdapter(getApplicationContext());
        recycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recycler.setAdapter(myadapter);
        recycler.setVisibility(View.VISIBLE);
        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor = profile.edit();
        volleySingleton = VolleySingleton.getinstance(WishDetailsTwo.this);
        requestQueue = volleySingleton.getrequestqueue();
        Intent get=getIntent();
        wishId=get.getStringExtra("wishId");
        cdate=get.getStringExtra("checkInDate");
        odate=get.getStringExtra("checkOutDate");
        otime=get.getStringExtra("cOutTime");
        ctime=get.getStringExtra("cInTime");
        adult=get.getStringExtra("adults");
       child= get.getStringExtra("child");
        room=get.getStringExtra("room");
        status=get.getStringExtra("status");


        try {
            getreplies();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void getreplies() throws JSONException {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.GET_REPLY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        Log.e("response",respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);

                            Log.e("replies",response.toString());
                            // JSONArray data=response.getJSONArray("offers");
                            // Log.e("offers log",data.toString());

                            if ((response.getBoolean("res"))){
                                JSONArray data=response.getJSONArray("replies");
                                Log.e("offers log",data.toString());
                                myadapter.clearAll();
                                if (data.length()>0) {
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject rec = data.getJSONObject(i);
                                        myadapter.history(new WishDetailObjectTwo(rec.getString("hotelName"),rec.getString("hotelAddress"),rec.getString("comment"),rec.getString("price"),rec.getString("roomType"),room,adult,child,wishId,cdate,ctime,otime,odate,rec.getString("hotelId"),rec.getString("replyId"),status));
                                         }
                                }else{
                                    Toast.makeText(WishDetailsTwo.this, "No Replies Yet!!!", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(WishDetailsTwo.this,error.toString(),Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());

            }
        }
        ){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("wishId",wishId);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }


}
