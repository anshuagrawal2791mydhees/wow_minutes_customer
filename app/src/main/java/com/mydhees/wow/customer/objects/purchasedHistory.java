package com.mydhees.wow.customer.objects;

import android.media.Image;

/**
 * Created by Abhishek on 26-sept-16.
 */

public class purchasedHistory {
    String date,amount,orderId,transationID,status,paymentType;
    Image image;

    public purchasedHistory(String date, String amount, String orderId, String transationID, String status, String paymentType) {
        this.date = date;
        this.amount = amount;
        this.orderId = orderId;
        this.transationID = transationID;
        this.status = status;
        this.paymentType = paymentType;

    }

    public String getDate() {
        return date;
    }

    public String getAmount() {
        return amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getTransationID() {
        return transationID;
    }

    public String getStatus() {
        return status;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public Image getImage() {
        return image;
    }
}
