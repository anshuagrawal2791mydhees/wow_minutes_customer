
package com.mydhees.wow.customer.retroModels;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Review {

    @SerializedName("roomId")
    @Expose
    private String roomId;
    @SerializedName("review")
    @Expose
    private String review;
    @SerializedName("customerName")
    @Expose
    private String customerName;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("roomType")
    @Expose
    private String roomType;
    @SerializedName("customerId")
    @Expose
    private String customerId;
    @SerializedName("overallRating")
    @Expose
    private Integer overallRating;

    /**
     * 
     * @return
     *     The roomId
     */
    public String getRoomId() {
        return roomId;
    }

    /**
     * 
     * @param roomId
     *     The roomId
     */
    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    /**
     * 
     * @return
     *     The review
     */
    public String getReview() {
        return review;
    }

    /**
     * 
     * @param review
     *     The review
     */
    public void setReview(String review) {
        this.review = review;
    }

    /**
     * 
     * @return
     *     The customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * 
     * @param customerName
     *     The customerName
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The _id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The roomType
     */
    public String getRoomType() {
        return roomType;
    }

    /**
     * 
     * @param roomType
     *     The roomType
     */
    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    /**
     * 
     * @return
     *     The customerId
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * 
     * @param customerId
     *     The customerId
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    /**
     * 
     * @return
     *     The overallRating
     */
    public Integer getOverallRating() {
        return overallRating;
    }

    /**
     * 
     * @param overallRating
     *     The overallRating
     */
    public void setOverallRating(Integer overallRating) {
        this.overallRating = overallRating;
    }

    @Override
    public String toString() {
        return "Review{" +
                "roomId='" + roomId + '\'' +
                ", review='" + review + '\'' +
                ", customerName='" + customerName + '\'' +
                ", id='" + id + '\'' +
                ", roomType='" + roomType + '\'' +
                ", customerId='" + customerId + '\'' +
                ", overallRating=" + overallRating +
                '}';
    }
}
