package com.mydhees.wow.customer.objects;

import java.util.Date;

/**
 * Created by Abhishek on 24-Aug-16.
 */

public class WishDetailObject {

    String wishSerious,city;
    String priceRange;
    Date checkIn;
    String rooms,adults,child,wishid,status;

    Date date_check_out,create;

    public String getRooms() {
        return rooms;
    }

    public String getAdults() {
        return adults;
    }

    public String getChild() {
        return child;
    }

    public String getWishid() {
        return wishid;
    }

    public Date getCreate() {
        return create;
    }

    public Date getDate_check_out() {
        return date_check_out;
    }
    String minPrice, maxPrice;

    public WishDetailObject(String wishSerious, String city, String minPrice,String maxPrice,String priceRange, String room, String adults, String child, String WishId, Date chkIn, Date chkOut, Date created) {
        this.wishSerious = wishSerious;
        this.city = city;
        this.priceRange = priceRange;

        this.rooms=room;
        this.adults=adults;
        this.child=child;
        this.wishid=WishId;
        this.date_check_out=chkOut;
        this.create=created;

        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.checkIn = chkIn;
        if(new Date().after(chkIn))
        {

            this.status="Expired";


        }else{
            this.status="Live";
        }

    }

    public String getStatus() {
        return status;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSerious() {
        return wishSerious;
    }

    public String getCity() {
        return city;
    }

    public String getPriceRange() {
        return priceRange;
    }

    public Date getCheckIn() {
        return checkIn;
    }

    public void setWishSerious(String wishSerious) {
        this.wishSerious = wishSerious;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPriceRange(String priceRange) {
        this.priceRange = priceRange;
    }

    public void setCheckIn(Date checkIn) {
        this.checkIn = checkIn;
    }
}
