package com.mydhees.wow.customer.retroModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anshu on 01/07/16.
 */
public class ImagesS3_ {

    @SerializedName("name")
    @Expose
    private List<String> name = new ArrayList<String>();
    @SerializedName("description")
    @Expose
    private List<String> description = new ArrayList<String>();

    /**
     *
     * @return
     * The name
     */
    public List<String> getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(List<String> name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    public List<String> getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(List<String> description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ImagesS3_{" +
                "name=" + name +
                ", description=" + description +
                '}';
    }
}
