package com.mydhees.wow.customer.registrationAndLogin;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mydhees.wow.customer.Constants;
import com.mydhees.wow.customer.Home;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.RegistrationIntentService;
import com.mydhees.wow.customer.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;
import java.util.Calendar;

public class user_details extends AppCompatActivity {

    EditText name;
    EditText dob;
    EditText email;
    EditText password;
    EditText repeat_password;
    EditText mobile_number;
    Button submit;
    boolean error=false;
    Spinner city;
    SharedPreferences profile,launch_time;
    SharedPreferences.Editor editor;
    TextView tv_terms;
    TextView login_now;
    CheckBox check;
    SharedPreferences.Editor editor3;

    String emailFromFb="";
    String nameFromFb = "";
    String profilePicUrl="";

    DateFormat simpledateformat12= new SimpleDateFormat("dd-MM-yyyy");
    DateFormat simpledateformat1= new SimpleDateFormat("yyyy-MM-dd");
    int mYear;
    int mMonth;
    int mDay;
    String dobstring;
    Calendar c = Calendar.getInstance();


    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;


    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;
    private TextView mInformationTextView;
    private boolean isReceiverRegistered;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "HomeActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        //getSupportActionBar().setTitle("Hello world App");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if(intent.getExtras()!=null){
            emailFromFb = intent.getStringExtra("email");
            nameFromFb = intent.getStringExtra("name");
            profilePicUrl = intent.getStringExtra(Constants.FB_PROFILE_PIC_URL);

            Log.e("fb_data",emailFromFb+nameFromFb+profilePicUrl);
        }


        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,MODE_PRIVATE);
        editor=profile.edit();
        launch_time = getSharedPreferences(Constants.LAUNCH_TIME_PREFERENCE_FILE,MODE_PRIVATE);
        editor3 = launch_time.edit();
        name = (EditText)findViewById(R.id.name);
        dob = (EditText)findViewById(R.id.dob1);
        email = (EditText)findViewById(R.id.email);
        password= (EditText)findViewById(R.id.password);
        repeat_password = (EditText)findViewById(R.id.repeat_password);
        mobile_number = (EditText)findViewById(R.id.mobile);
        check = (CheckBox)findViewById(R.id.check);

        email.setText(emailFromFb);
        name.setText(nameFromFb);
       // city = (Spinner)findViewById(R.id.city);

        volleySingleton=VolleySingleton.getinstance(getApplicationContext());
        requestQueue = volleySingleton.getrequestqueue();

        tv_terms = (TextView)findViewById(R.id.tv_terms);

        tv_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(user_details.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                dialog.setContentView(R.layout.terms_condition_dialog);
                dialog.setCancelable(false);
                dialog.show();
                Button btn = (Button)dialog.findViewById(R.id.dialog_got_it);

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //dialog.dismiss();
                //                mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                Log.e("onreceive","received");
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(Constants.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
//                    mInformationTextView.setText(getString(R.string.gcm_send_message));
                    Log.e("onreceive","success");
                    Log.e("---intent token",intent.getStringExtra("token"));

                    try {
                        registerUser(intent.getStringExtra("token"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.e("onreceive","error");
                    Toast.makeText(user_details.this,"gcm token error",Toast.LENGTH_LONG).show();

//                    mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };



        submit = (Button)findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                error=false;
                if(name.getText().toString().equals("")) {
                    name.setError("Enter Name");
                    error=true;
                }

                if(dob.getText().toString().equals("")) {
                    dob.setError("Enter Date of Birth");
                    error=true;
                }

                if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText()).matches())
                {
                   email.setError("Invalid Email");
                    //email.setText("");
                    error = true;
                }
                if(email.getText().toString().equals(""))
                {
                    email.setError("Required");
                    error = true;
                }
                if(!repeat_password.getText().toString().equals(password.getText().toString()))
                {
                    repeat_password.setError("Passwords don't match");
                    repeat_password.setText("");
                    error = true;
                }
                if(password.getText().length()<5)
                {
                    password.setError("Minimum 5 characters");
                    password.setText("");
                    repeat_password.setText("");
                }
                if(mobile_number.getText().length()<10)
                {
                    mobile_number.setError("Invalid Mobile Number");
                    error=true;
                }

//                if(city.getSelectedItemPosition()==0)
//                {
//                    error=true;
//                    Toast.makeText(getApplicationContext(),"Select City",Toast.LENGTH_SHORT).show();
//
//                }
                if(!check.isChecked())
                {
                    check.setError("Required");
                    error=true;
                }
                if(error==false)
                {
                    //Toast.makeText(getApplicationContext(),city.getSelectedItem().toString(),Toast.LENGTH_LONG).show();
//                    editor3.putBoolean(Constants.FIRST_TIME,true);
//                    editor3.commit();

                    Log.e("gcm", String.valueOf(isReceiverRegistered));


                    registerReceiver();
                    Log.e("gcm2", String.valueOf(isReceiverRegistered));

                    if (checkPlayServices()) {
                        // Start IntentService to register this application with GCM.
                        Intent intent = new Intent(user_details.this, RegistrationIntentService.class);
                        startService(intent);
                    }



                }

            }
        });

        TextView login_now =(TextView)findViewById(R.id.login_now);
        login_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(user_details.this,login.class));
            }
        });

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getApplicationContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String month = String.format("%02d", monthOfYear + 1);
                                String day = String.format("%02d", dayOfMonth);

                                dobstring = year + "-" + month + "-" + day;

                                dob.setText(day + "-" + month + "-" + year);

                            }
                        },mYear,mMonth, mDay);

                datePickerDialog.show();
            }

        });
    }
    private void registerReceiver(){
        if(!isReceiverRegistered) {
            Log.e("registerReceiver","here");
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Constants.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.e(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    @Override
    protected void onResume() {
        registerReceiver();

        super.onResume();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }

//    private void checkUser() throws JSONException {
//        Toast.makeText(getApplicationContext(),"Registering...",Toast.LENGTH_LONG).show();
//.
//        StringRequest request = new StringRequest(Request.Method.POST, Constants.CHECK_USER_URL,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String respons) {
//                        Log.e("response",respons.toString());
//                        JSONObject response = null;
//                        try {
//                            response = new JSONObject(respons);
//                        } catch (JSONException e) {
//                            Log.e("error",e.toString());
//                        }
//                        try {
//                            if(response.getBoolean("res"))
//                            {
//                                editor.putString(Constants.NAME,name.getText().toString());
//                                editor.putString(Constants.EMAIL,email.getText().toString());
//                                editor.putString(Constants.PASSWORD,password.getText().toString());
//                                editor.putString(Constants.PHONE,mobile_number.getText().toString());
//                                editor.commit();
//
//                                try {
//                                    registerUser();
//                                } catch (JSONException e) {
//                                    Toast.makeText(getApplicationContext(),e.toString(), Toast.LENGTH_LONG).show();
//                                }
//
//                            }
//                            else
//                            {
//                                Toast.makeText(getApplicationContext(),response.getString("response"),Toast.LENGTH_LONG).show();
//                            }
//                        } catch (JSONException e) {
//                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
//                Log.e("vollley error",error.toString());
//            }
//        }){
//            @Override
//            protected Map<String,String> getParams(){
//                Map<String,String> params = new HashMap<String, String>();
////                params.put(Constants.NAME,name.getText().toString());
//                params.put(Constants.EMAIL,email.getText().toString());
////                params.put(Constants.PASSWORD, password.getText().toString());
////                params.put(Constants.PHONE, mobile_number.getText().toString());
////                params.put(Constants.CITY, city.getSelectedItem().toString());
//                return params;
//            }
//        };
//        requestQueue.add(request);
//    }

    private void registerUser(final String token) throws JSONException{
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.show();

        StringRequest request = new StringRequest(Request.Method.POST, Constants.REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                        try {
                            if(response.getBoolean("res"))
                            {

                                Log.e("jashan",response.toString());
                                editor.putString(Constants.ID,response.getString(Constants.ID));
                                Log.e("id",response.getString(Constants.ID));
                                editor.commit();
                                editor3.putBoolean(Constants.FIRST_TIME,true);
                                editor3.commit();
                                // Toast.makeText(getApplicationContext(),street_address.getText().toString().concat(street_address2.getText().toString()),Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), Home.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                editor.putString(Constants.NAME,name.getText().toString());
                                editor.putString(Constants.DOB,dob.getText().toString());
                                editor.putString(Constants.EMAIL,email.getText().toString());
                                editor.putString(Constants.PASSWORD,password.getText().toString());
                                editor.putString(Constants.PHONE,mobile_number.getText().toString());
                                JSONArray images = response.getJSONObject("imagesS3").getJSONArray("name");
                                if(images.length()>0)
                                    editor.putString(Constants.PROFILE_IMAGE_ID,images.getString(images.length()-1));
//                                editor.putString(Constants.CITY,city.getSelectedItem().toString());
                                editor.commit();
//                                startActivity(new Intent(getApplicationContext(),location_selection.class));

                                Toast.makeText(getApplicationContext(),response.getString("response"),Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(),response.getString("response"),Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),"Internet error",Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                                params.put(Constants.NAME,name.getText().toString());
                params.put(Constants.DOB,dob.getText().toString());
                params.put(Constants.EMAIL,email.getText().toString());
                params.put(Constants.PASSWORD, password.getText().toString());
                params.put(Constants.PHONE, mobile_number.getText().toString());
                params.put(Constants.GCM_TOKEN,token);
                if(!profilePicUrl.matches(""))
                    params.put(Constants.FB_PROFILE_PIC_URL,profilePicUrl);

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    //For back button in Appbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

