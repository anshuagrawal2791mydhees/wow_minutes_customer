package com.mydhees.wow.customer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.juspay.ec.sdk.api.Environment;
import in.juspay.ec.sdk.checkout.MobileWebCheckout;
import in.juspay.ec.sdk.util.PaymentInstrument;
import in.juspay.godel.analytics.GodelTracker;
import in.juspay.juspaysafe.BrowserCallback;

public class RoomBookConfirmation extends AppCompatActivity {

    TextView hotelName,address,adult,child,checkInDate,checkInTime,checkOutDate,checkOutTime,originalPrice,wowPrice,payPrice,disAmount;
    ImageView hotelPic;
    ProgressDialog dialog;
    Button addDetailsConfirm;
    LinearLayout linearLayoutUD;

    EditText fullNameUd,emailUd,mobileUd;
    Button viewDetails;
    Boolean isClick=true;

    int time=0;
    String order_id = "";
    LinearLayout pay;
    SharedPreferences profile;
    SharedPreferences.Editor editor;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    String hname,addres,adults,children,datein,timein,dateout,timeout,priceold,pricewow,offerId,status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_book_confirmation);
        pay= (LinearLayout) findViewById(R.id.selectService);
        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        volleySingleton = VolleySingleton.getinstance(getApplicationContext());
        requestQueue = volleySingleton.getrequestqueue();

        Environment.configure(Environment.SANDBOX, "anshuagrawal2791");
        hotelPic = (ImageView)findViewById(R.id.hotel_picc);
        hotelName = (TextView)findViewById(R.id.hotel_name_c);
        address = (TextView)findViewById(R.id.address_c);
        adult = (TextView)findViewById(R.id.adult_c);
        child = (TextView)findViewById(R.id.child_c);
        checkInDate = (TextView)findViewById(R.id.checkin_datec);
        checkInTime = (TextView)findViewById(R.id.checkin_timec);
        checkOutDate = (TextView)findViewById(R.id.checkout_datec);
        checkOutTime = (TextView)findViewById(R.id.checkout_timec);
        originalPrice = (TextView)findViewById(R.id.original_pricec);
        wowPrice= (TextView)findViewById(R.id.wow_pricec);
        payPrice = (TextView)findViewById(R.id.wow_price_con);
        disAmount = (TextView)findViewById(R.id.discountAmt);
       // addDetailsConfirm = (Button)findViewById(R.id.btn_add_details);

        fullNameUd = (EditText)findViewById(R.id.name_ud);
        emailUd = (EditText)findViewById(R.id.email_ud);
        mobileUd = (EditText)findViewById(R.id.mobile_ud);
        viewDetails = (Button)findViewById(R.id.btn_view_details);
        linearLayoutUD = (LinearLayout) findViewById(R.id.linearLayoutUD);

        emailUd.setText(profile.getString(Constants.EMAIL,"DEFAULT"));
        mobileUd.setText(profile.getString(Constants.PHONE,"DEFAULT"));
        fullNameUd.setText(profile.getString(Constants.NAME,"DEFAULT"));


        viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isClick){
                    linearLayoutUD.setVisibility(View.VISIBLE);
                    viewDetails.setVisibility(View.GONE);
                }
                else{
                    linearLayoutUD.setVisibility(View.GONE);
                }
            }
        });






        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent get=getIntent();
        hname=get.getStringExtra("hname");
        addres=get.getStringExtra("address");
        adults=get.getStringExtra("adults");
        children=get.getStringExtra("chld");
        datein=get.getStringExtra("chkindate");
        dateout=get.getStringExtra("chkoutdate");
        timein=get.getStringExtra("chkintime");
        timeout=get.getStringExtra("chkouttime");
        pricewow=get.getStringExtra("wowP");
        priceold=get.getStringExtra("oldP");
        offerId=get.getStringExtra("offerId");

        hotelName.setText(hname);
        address.setText(addres);
        adult.setText(adults);
        child.setText(children);
        checkInDate.setText(datein);
        checkOutDate.setText(dateout);
        checkInTime.setText(timein);
        checkOutTime.setText(timeout);
        wowPrice.setText(pricewow);
        originalPrice.setText(priceold);
        payPrice.setText(pricewow);
        int dis= Integer.parseInt(priceold)- Integer.parseInt(pricewow);
        disAmount.setText(dis+"");


        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( mobileUd.getText().toString().equals(""))
                {
                    mobileUd.setError("Enter Mobile Number");
                }
                else if(fullNameUd.getText().toString().equals(""))
                {
                    fullNameUd.setError("Enter Name");
                }
                 else if(emailUd.getText().toString().equals(""))
                {
                    emailUd.setError("Enter email ID");
                }
                else{
                    try {
                        buyoffer();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });



    }

    //For back button in toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void buyoffer() throws JSONException {
        dialog = new ProgressDialog(RoomBookConfirmation.this);
        dialog.show();
        dialog.setTitle("Please wait...");
        dialog.setMessage("Please wait...");
        editor = profile.edit();
        StringRequest request = new StringRequest(Request.Method.POST, Constants.CREATE_ORDER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        Log.e("response offers", respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                            order_id = response.getString("order_id");
                            MobileWebCheckout checkout = new MobileWebCheckout(
                                    response.getString("order_id"),
                                    new String[]{"http://www.mydhees.com/.*"},
                                    new PaymentInstrument[]{PaymentInstrument.CARD, PaymentInstrument.WALLET, PaymentInstrument.NB}
                            );

                            checkout.startPayment(RoomBookConfirmation.this, new BrowserCallback() {

                                @Override
                                public void endUrlReached(String s) {
//                                        Log.e("s",s);
//                                    if(time==1) {
                                    //GodelTracker.getInstance().trackPaymentStatus(order_id, GodelTracker.PaymentStatus.SUCCESS);
                                    //getTransactionId

                                        Uri url = Uri.parse(s);
                                        Log.e("url", url.toString());
                                        if (url.getQueryParameter("status").equals("CHARGED")) {
                                            status="Success";


                                                time++;
                                                dialog.show();


                                       }
                                    else {
                                            Toast.makeText(getApplicationContext(), url.getQueryParameter("status"), Toast.LENGTH_LONG).show();
                                            status="Failed";
//                                        }
                                    }
                                    Intent intent = new Intent(RoomBookConfirmation.this,TransactionSummary.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.putExtra("order_id",order_id);
                                    intent.putExtra("status",status);
                                    startActivity(intent);

                                }


                                @Override
                                public void ontransactionAborted() {
                                    Toast.makeText(getApplicationContext(), "aborted", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(RoomBookConfirmation.this, Home.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    //intent.putExtra("order_id",order_id);
                                    intent.putExtra("status","aborted");
                                    startActivity(intent);


                                }
                            });


                        } catch (JSONException e) {
                            Log.e("error", e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(RoomBookConfirmation.this, "Error-Internet", Toast.LENGTH_LONG).show();
                Log.e("vollley error", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
 params.put("customerId", profile.getString(Constants.ID,"default"));
    params.put("amount", pricewow);
    params.put("offerId", offerId);


   // params.put(Constants.EMAIL, emailUd.getText().toString());
    params.put("bookingContact", mobileUd.getText().toString());
//                    params.put(Constants.ADDRESS_LINE1, "abc");
//                    params.put(Constants.ADDRESS_LINE2, "abc");
//                    params.put(Constants.ADDRESS,"bangalore" );
//                    params.put(Constants.PIN, "560036");
    params.put("bookingName", fullNameUd.getText().toString());

//                    params.put("description", "Bought Points-wow wallet");
//
//                    params.put(Constants.CITY,"bangalore" );
    params.put("bookingEmail", emailUd.getText().toString());

            return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }




}
