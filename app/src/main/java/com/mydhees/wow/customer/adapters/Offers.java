package com.mydhees.wow.customer.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.mydhees.wow.customer.Home;
import com.mydhees.wow.customer.HotelView;
import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.VolleySingleton;
import com.mydhees.wow.customer.objects.offer_details;
import com.mydhees.wow.customer.retroModels.Offer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static java.lang.Math.ceil;

/**
 * Created by Abhishek on 13-Jun-16.
 */
public class Offers extends RecyclerView.Adapter<Offers.myviewholder>{
    private LayoutInflater layoutInflater;
    ArrayList<Offer> discount_offers = new ArrayList<>();
    Context context;
   DateFormat parser2 = new SimpleDateFormat("dd-MM-yyyy");
   DateFormat parser3 = new SimpleDateFormat("HH:mm");
    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    String checkinstring,scheduledstring,expirystring,checkoutstring;
    Date checkin,checkout,scheduled,expiry;
    VolleySingleton volleySingleton;
    ImageLoader imageLoader;




    public Offers(Context context)
    {
        volleySingleton = VolleySingleton.getinstance(context);
        imageLoader = volleySingleton.getimageloader();
        this.context=context;
        layoutInflater = LayoutInflater.from(context);

    }

    public void add_offer(Offer recievedoffer)
    {
        discount_offers.add(recievedoffer);
        notifyItemInserted(discount_offers.size());
    }
    public void add_offers(ArrayList<Offer> offers){
//        discount_offers.clear();
        discount_offers.addAll(offers);
        notifyDataSetChanged();
    }
    public ArrayList<Offer> getofferlist()
    {
        return discount_offers;
    }
    public void clearAll()
    {
        discount_offers.clear();
    }


    @Override
    public Offers.myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.activity_hotel_list,parent,false);
        myviewholder viewholder = new myviewholder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final Offers.myviewholder holder, final int position) {
        final Offer hotel_offer = discount_offers.get(position);
        final ImageView image ;

//        holder.discount.setText(hotel_offer.getDiscount()+"");
        holder.hotel_image.setImageResource(R.drawable.hotel_pic);
//        holder.hotel_star.setRating(hotel_offer.getHotel_star());
//
//        holder.hotel_name.setText(hotel_offer.getHotel_name());
//        holder.location.setText(hotel_offer.getAddress()+"");
//        holder.hotel_like.setText(hotel_offer.getHotel_like()+"");
//
//        holder.time_remaining.setText(hotel_offer.getTime_remaining()+"");
//        holder.checkin_date.setText(hotel_offer.getCheckin());
//        holder.checkin_time.setText(hotel_offer.getCheckin());
//
//
//        holder.offer_price.setText(hotel_offer.getOffer_price()+"");
//        holder.old_price.setText(hotel_offer.getOffer_price()+"");
//        holder.checkout_date.setText(hotel_offer.getCheck_out());
//        holder.checkout_time.setText(hotel_offer.getCheck_out());
        double discount = ((double) hotel_offer.offerInfo.priceMRP - (double)hotel_offer.offerInfo.wowPrice)/(double)hotel_offer.offerInfo.priceMRP;
        checkinstring = hotel_offer.offerInfo.checkIn;
        checkoutstring = hotel_offer.offerInfo.checkOut;
        scheduledstring = hotel_offer.offerInfo.scheduledAt;
        expirystring = hotel_offer.offerInfo.expiresAt;
        parser.setTimeZone(TimeZone.getTimeZone("IST"));
        try {
            checkin = parser.parse(checkinstring);
            checkout = parser.parse(checkoutstring);
            scheduled = parser.parse(scheduledstring);
            expiry = parser.parse(expirystring);
        } catch (ParseException e) {
            e.printStackTrace();
        }



        holder.discount.setText("-"+(int)(discount*100)+"%");
        holder.rate = hotel_offer.hotelInfo.getStarHotel();
        holder.hotel_name.setText(hotel_offer.offerInfo.hotelName);
        holder.location.setText(hotel_offer.hotelInfo.getAddressLine1());
//        holder.location.setText("aaa");
//        Log.e("addressLine1",hotel_offer.hotelInfo.addressLine1);
       // holder.hotel_like.setText("3.5/5");
//        int timereamaining = (int) (TimeUnit.MILLISECONDS.toSeconds(expiry.getTime()-System.currentTimeMillis())/60);


        int timereamaining = (int) (expiry.getTime()-System.currentTimeMillis());
        new CountDownTimer(timereamaining,1000){

            @Override
            public void onTick(long millisUntilFinished) {

                int seconds = (int) (millisUntilFinished / 1000) % 60 ;
                int minutes = (int) ((millisUntilFinished / (1000*60)) % 60);
                String text = String.format("%02d:%02d",minutes,seconds);

                holder.time_remaining.setText(text);
            }

            @Override
            public void onFinish() {
                discount_offers.remove(position);
                notifyItemRemoved(position);
                if(discount_offers.size()==0)
                    context.startActivity(new Intent(context,Home.class));
            }
        }.start();


//        holder.time_remaining.setText(timereamaining+" mins");
        holder.checkin_date.setText(parser2.format(checkin));
        holder.checkout_date.setText(parser2.format(checkout));
        holder.offer_price.setText(hotel_offer.offerInfo.wowPrice+"");
        holder.old_price.setText(hotel_offer.offerInfo.priceMRP+"");
        holder.checkin_time.setText(parser3.format(checkin));
        holder.checkout_time.setText(parser3.format(checkout));
        Log.e("+++++",hotel_offer.hotelInfo.toString());
        if(hotel_offer.hotelInfo.getNumberOfReviews()!=null) {
            if (hotel_offer.hotelInfo.getNumberOfReviews() != 0)
                holder.hotel_like.setText("\t" + hotel_offer.hotelInfo.getUserRatings().getOverall() + "/5" +
                        "");
        }

        if(hotel_offer.images.size()>0){
            String url = "https://s3-us-west-2.amazonaws.com/wowimagesupload/resized/middle/"+hotel_offer.images.get(0)+".jpg";
        imageLoader.get(url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                holder.hotel_image.setImageDrawable(null);
                holder.hotel_image.setImageBitmap(response.getBitmap());
            }

            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });
        }

        for(int i=0;i<ceil(holder.rate);i++)
        {
            holder.star = new ImageView(context.getApplicationContext());
            holder.star.setImageResource(R.drawable.ic_action_star);
            holder.linear.addView(holder.star);
            holder.scrollbar.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context.getApplicationContext(), HotelView.class);
                intent.putExtra("city",hotel_offer.hotelInfo.getCity());
                intent.putExtra("offerId",hotel_offer.offerInfo.offerId);
                context.startActivity(intent);
            }
        });











    }

    @Override
    public int getItemCount() {
        return discount_offers.size();
    }


    static class myviewholder extends RecyclerView.ViewHolder{
        private TextView discount;
        private ImageView hotel_image;
        private HorizontalScrollView scrollbar;
        private LinearLayout linear;
        private ImageView star;
        private int rate;

        private TextView hotel_name;
        private TextView location;
        private TextView hotel_like;

        private TextView time_remaining;
        private TextView checkin_date;
        private TextView checkin_time;

        private TextView offer_price;
        private TextView old_price;
        private TextView checkout_date;
        private TextView checkout_time;
        private CardView cardView;



        public myviewholder(View itemView) {
            super(itemView);
            discount = (TextView) itemView.findViewById(R.id.discount);
            hotel_image = (ImageView)itemView.findViewById(R.id.hotel_image);
            scrollbar = (HorizontalScrollView) itemView.findViewById(R.id.scroll);
            linear = (LinearLayout) itemView.findViewById(R.id.linear);
            star = (ImageView) itemView.findViewById(R.id.star);

            hotel_name = (TextView) itemView.findViewById(R.id.hotel_name);
            location = (TextView) itemView.findViewById(R.id.hotel_location);
            hotel_like = (TextView) itemView.findViewById(R.id.hotel_like);

            time_remaining = (TextView) itemView.findViewById(R.id.time_ramaining);
            checkin_date = (TextView) itemView.findViewById(R.id.checkin_date);
            checkin_time = (TextView) itemView.findViewById(R.id.checkin_time);

            offer_price = (TextView) itemView.findViewById(R.id.offer_price);
            old_price = (TextView)itemView.findViewById(R.id.old_price);
            checkout_date = (TextView) itemView.findViewById(R.id.checkout_date);
            checkout_time = (TextView) itemView.findViewById(R.id.checkout_time);
            cardView = (CardView)itemView.findViewById(R.id.card);





        }
    }
}
