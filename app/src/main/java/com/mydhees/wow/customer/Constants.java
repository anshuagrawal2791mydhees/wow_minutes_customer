package com.mydhees.wow.customer;


public class Constants {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PROFILE_IMAGE_ID = "profile_image_id";
    public static final String PROFILE_IMAGE_URI = "profile_image_uri";
    public static final String GCM_TOKEN = "gcm_token";
    public static final String ID = "id";
    public static final String CUSTOMER_ID = "customerId";
    public static final String LAUNCH_TIME_PREFERENCE_FILE = "launch_time";
    public static final String FIRST_TIME = "first_time";
    public static final String PROFILE_PREFERENCE_FILE = "profile";
    public static final String SERVICES = "services";
    public static final String NAME = "name";
    public static final String DOB = "dob";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String CITY = "city";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ADDRESS = "address";
    public static final String CODE = "code";
    public static final String NPASS = "npass";
    public static final String ADDRESS_LINE1 = "address_line1";
    public static final String ADDRESS_LINE2 = "address_line2";
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PIN = "pin";
    public static final String PACKAGE_NAME =
            "com.example.anshu.wow_minutes";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";



    public static final String CLEANLINESS = "cleanliness";
    public static final String CHECK_IN_PROCEDURE = "checkInProcedure";
    public static final String RESTAURANT_STAFF = "restaurantStaff";
    public static final String RESTAURANT_AMBIANCE = "restaurantAmbiance";
    public static final String FRONT_OFFICE_STAFF = "frontOfficeStaff";
    public static final String HOUSE_KEEPING = "houseKeeping";
    public static final String OVERALL = "overall";
    public static final String LIKELY_TO_STAY_AGAIN = "likelyToStayAgain";
    public static final String REVIEW = "review";
    public static final String CUSTOMERID = "customerId";
    public static final String HOTELID = "hotelId";
    public static final String ROOMID = "roomId";
    public static final String ROOM_TYPE = "roomType";
    public static final String CUSTOMER_NAME = "customerName";
    public static final String FB_PROFILE_PIC_URL = "fbProfilePicUrl";


    public static final String MAIN_SERVER_URL = "http://wowserver2.ap-south-1.elasticbeanstalk.com";
    public static final String CHECK_USER_URL = MAIN_SERVER_URL+"/customer/check_user";
    public static final String REGISTER_URL = MAIN_SERVER_URL+"/register/customer";
    public static final String LOGIN_URL = MAIN_SERVER_URL+"/login/customer";
    public static final String SEND_RESET_CODE_URL =MAIN_SERVER_URL+"/api/resetpass";
    public static final String RESET_PASS_URL = MAIN_SERVER_URL+"/api/resetpass/chg";
    public static final String CREATE_ORDER = MAIN_SERVER_URL+"/customer/order/create";
    public static final String STATUS = MAIN_SERVER_URL+"/customer/order/status_callback";
    public static final String CREATE_WISH = MAIN_SERVER_URL+"/wish/create";
    public static final String GET_WISH = MAIN_SERVER_URL+"/wish/get";
    public static final String DELETE_WISH = MAIN_SERVER_URL+"/wish/delete";
    public static final String FEEDBACK = MAIN_SERVER_URL+"/feedback/submit";
    public static final String GET_REPLY = MAIN_SERVER_URL+"/wish/replies";
    public static final String BOOKING_HISTORY = MAIN_SERVER_URL+"/customer/order/list";
    public static final String UPLOAD_IMAGE = MAIN_SERVER_URL+"/customer/upload_image";
    public static final String PAY_WISH = MAIN_SERVER_URL+"/customer/order/createwish";
    public static final String GET_HOTEL_ADDRESS = MAIN_SERVER_URL+"/hotel/get/address";
    public static final String CONFIRM_REPLY = MAIN_SERVER_URL+"/wish/confirmreply";
    public static final String HOTEL_LIST = MAIN_SERVER_URL+"/hotel/getlist";
    public static final String ADD_CREDITS = "  ";
    public static final String HOTEL_UPDATE_URL = MAIN_SERVER_URL+"/hotel/update";
    public static final String CONTACT_US = MAIN_SERVER_URL+"/contactus/send";


    public static final String GET_CITY_LIST =MAIN_SERVER_URL+"/get_city_list" ;
}
