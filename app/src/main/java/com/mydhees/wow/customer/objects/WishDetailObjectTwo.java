package com.mydhees.wow.customer.objects;

import java.util.Date;

/**
 * Created by Abhishek on 25-Aug-16.
 */

public class WishDetailObjectTwo {
    String hotelName,location,message;
  String price;
    String checkIn,ctime,otime,type,status;
    String rooms,adults,child,wishid,hotelid,replyid;

    public String getCheckIn() {
        return checkIn;
    }

    public String getRooms() {
        return rooms;
    }

    public String getAdults() {
        return adults;
    }

    public String getChild() {
        return child;
    }

    public String getWishid() {
        return wishid;
    }

    public String getHotelid() {
        return hotelid;
    }

    public String getReplyid() {
        return replyid;
    }

    public String getType() {
        return type;
    }

    public String getDate_check_out() {
        return date_check_out;
    }

    String date_check_out;

    public String getStatus() {
        return status;
    }

    public WishDetailObjectTwo(String hotelName, String location, String message, String price, String type, String room, String adults, String child, String WishId, String chkIn, String ctime, String otime, String chkOut, String hid, String Rid, String status) {
        this.hotelName = hotelName;
        this.location = location;
        this.message = message;
        this.price = price;
        this.rooms=room;
        this.type=type;
        this.adults=adults;
        this.child=child;
        this.wishid=WishId;
        this.date_check_out=chkOut;
        this.hotelid=hid;
        this.replyid= Rid;
        this.status=status;
        this.ctime=ctime;
        this.otime=otime;
        this.checkIn = chkIn;
    }

    public String getCtime() {
        return ctime;
    }

    public String getOtime() {
        return otime;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
