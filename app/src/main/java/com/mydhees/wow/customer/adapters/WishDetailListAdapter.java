package com.mydhees.wow.customer.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mydhees.wow.customer.R;
import com.mydhees.wow.customer.WishDetailsTwo;
import com.mydhees.wow.customer.objects.WishDetailObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class WishDetailListAdapter extends RecyclerView.Adapter<WishDetailListAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<WishDetailObject> DetailList = new ArrayList<>();
DateFormat parse=new SimpleDateFormat("dd-MM-yyyy");
DateFormat parse1=new SimpleDateFormat("HH:mm");
    String stats;

    public WishDetailListAdapter(Context context){
        this.context=context;
        layoutInflater = layoutInflater.from(context);
    }

    public void history(WishDetailObject history){
        DetailList.add(history);
        notifyItemInserted(DetailList.size());
    }

    public ArrayList<WishDetailObject> getDetaillist()
    {
        return DetailList;
    }
    public void clearAll(){
        DetailList.clear();
    }
    public void addAll(ArrayList<WishDetailObject> list){
        DetailList.clear();
        DetailList.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wish_details_row,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final WishDetailObject wishList = DetailList.get(position);
        holder.wishSerious.setText(wishList.getSerious()+"");
        holder.city.setText(wishList.getCity()+"");
        holder.priceRange.setText(wishList.getPriceRange()+"");
        holder.checkIn.setText(parse.format(wishList.getCheckIn())+"");
        holder.checkOut.setText(parse.format(wishList.getDate_check_out())+"");
        holder.adults.setText(wishList.getAdults()+"");
        holder.child.setText(wishList.getChild()+"");
        holder.rooms.setText(wishList.getRooms()+"");
        holder.createdDate.setText(parse.format(wishList.getCreate())+"");
        holder.status.setText(wishList.getStatus());
        if(System.currentTimeMillis() > wishList.getCheckIn().getTime())
        {

            stats="Expired";
            holder.status.setText(stats);

        }else{
            stats="Live";
        }
        holder.wish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent start= new Intent(context, WishDetailsTwo.class);
                start.putExtra("wishId",wishList.getWishid());
                start.putExtra("checkInDate",parse.format(wishList.getCheckIn())+"");
                start.putExtra("checkOutDate",parse.format(wishList.getDate_check_out())+"");
                start.putExtra("cOutTime",parse1.format(wishList.getDate_check_out())+"");
                start.putExtra("cInTime",parse1.format(wishList.getCheckIn())+"");
                start.putExtra("status",wishList.getStatus());

                start.putExtra("adults",wishList.getAdults());
                start.putExtra("child",wishList.getChild());
                start.putExtra("room",wishList.getRooms());
                context.startActivity(start);
            }
        });


    }

    @Override
    public int getItemCount() {
        return DetailList.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{

       TextView wishSerious,city,priceRange,checkIn, checkOut,adults,child,createdDate,rooms,status;
       CardView wish;
        public RecyclerViewHolder(View itemView) {
            super(itemView);

            wishSerious = (TextView)itemView.findViewById(R.id.wish_srs);
            city = (TextView)itemView.findViewById(R.id.wish_city);
            adults = (TextView)itemView.findViewById(R.id.wish_adults);
            child = (TextView)itemView.findViewById(R.id.wish_child);
            rooms = (TextView)itemView.findViewById(R.id.wish_rooms);
            priceRange = (TextView)itemView.findViewById(R.id.wish_price);
            checkIn = (TextView)itemView.findViewById(R.id.wish_checkin_date);
            checkOut = (TextView)itemView.findViewById(R.id.wish_checkout_date);
            createdDate = (TextView)itemView.findViewById(R.id.wish_date);
            status = (TextView)itemView.findViewById(R.id.status);
            wish= (CardView) itemView.findViewById(R.id.wish22);




        }
    }
}
