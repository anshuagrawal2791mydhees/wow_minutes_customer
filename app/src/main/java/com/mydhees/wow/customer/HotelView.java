package com.mydhees.wow.customer;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.mydhees.wow.customer.adapters.ReviewsAdapter;
import com.mydhees.wow.customer.objects.Review;
import com.mydhees.wow.customer.retroModels.offer_details;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HotelView extends AppCompatActivity {
    ViewPager mViewPagerHotel = null;
    ViewPager pagerRoomPic = null;
    RatingBar hotel_rate;
    TextView hotelName,hotelLike,address,discount,wowPrice,hotel_policy,timeLeft,oldPrice;
    TextView checkInDate,checkInTime,checkOutDate,checkOutTime,adult,child;
    LinearLayout selectService,people,room_photos;
    ProgressDialog dialog;
    VolleySingleton volleySingleton;
    ImageLoader imageLoader;
    ArrayList<Bitmap> images = new ArrayList<>();
    ArrayList<Bitmap> room_images = new ArrayList<>();
    private CollapsingToolbarLayout collapsingToolbarLayout = null;
    String offerId,city;
    CustomPagerAdapter mCustomPagerAdapter;
    DateFormat parser2 = new SimpleDateFormat("dd/MM/yyyy");
    DateFormat parser3 = new SimpleDateFormat("HH:mm");
    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    String checkinstring,scheduledstring,expirystring,checkoutstring;
    Date checkin,checkout,scheduled,expiry;
    int i;
    Animation animation;
    ImageView wifi,food,tv,gym;
    Button facilities1;

    AlertDialog.Builder builderDialog;
    AlertDialog alert;
    ArrayList<String> faciliites = new ArrayList<>();
    String[] extra_facilities;
    boolean[] extra_facilities_selected;
    LinearLayout more_facilities;
    TextView policy_hint;
    Boolean[] facilites_array = new Boolean[14];
    ArrayList<Review> reviews = new ArrayList<>();


    ArrayList<ReviewsAdapter> rev = new ArrayList<>();
   // Button cencel,back;
    RecyclerView recycler;
    ReviewsAdapter myadapter;
    LinearLayoutManager manager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_view);
        final Intent intent = getIntent();
        offerId = intent.getStringExtra("offerId");
        city = intent.getStringExtra("city");

        myadapter = new ReviewsAdapter(this);
        recycler = (RecyclerView) findViewById(R.id.recyclerReview);
        recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recycler.setAdapter(myadapter);
        //recycler.setVisibility(View.VISIBLE);




//        cencel = (Button)findViewById(R.id.cencel_view);
//        back = (Button)findViewById(R.id.back_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(Color.parseColor("#689F38"));
        }
        volleySingleton = VolleySingleton.getinstance(this);
        imageLoader = volleySingleton.getimageloader();

        //details hotel
        hotel_rate = (RatingBar)findViewById(R.id.hotel_stara);
        hotelName = (TextView)findViewById(R.id.hotel_namea);
        hotelLike = (TextView)findViewById(R.id.hotel_likea);
        address = (TextView)findViewById(R.id.addressa);
        discount = (TextView)findViewById(R.id.discounta);
        wowPrice = (TextView)findViewById(R.id.wow_pricea);
        oldPrice = (TextView)findViewById(R.id.oldPrice);
        checkInDate = (TextView)findViewById(R.id.checkin_datea);
        checkInTime = (TextView)findViewById(R.id.checkin_timea);
        checkOutDate = (TextView)findViewById(R.id.checkout_datea);
        checkOutTime = (TextView)findViewById(R.id.checkout_timea);

        hotel_policy=(TextView)findViewById(R.id.hpolicy_text);
        timeLeft = (TextView)findViewById(R.id.timeLeft);
       // facilities = (LinearLayout)findViewById(R.id.facilities_layout);
        selectService = (LinearLayout)findViewById(R.id.selectService);
        room_photos = (LinearLayout)findViewById(R.id.room_photos);
        people = (LinearLayout)findViewById(R.id.people);
        adult = (TextView)people.findViewById(R.id.adultsam);
        child = (TextView)people.findViewById(R.id.childrenam);

        //facilities
        wifi = (ImageView)findViewById(R.id.wifi) ;
        food = (ImageView)findViewById(R.id.restu) ;
        tv = (ImageView)findViewById(R.id.tv) ;
        gym = (ImageView)findViewById(R.id.zym) ;
       // facilities1 = (Button)findViewById(R.id.more_facilities);


        extra_facilities = new String []{"buisness services","coffee shop","front desk","spa","laundry","outdoor games","parking","room services","swimming","travel assistance"};
        extra_facilities_selected = new boolean[]{false,false,false,false,false,false,false,false,false,false};


        selectService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent go=new Intent(HotelView.this,RoomBookConfirmation.class);
                go.putExtra("hname",hotelName.getText().toString());
                go.putExtra("address",address.getText().toString());
                go.putExtra("adults",adult.getText().toString());
                go.putExtra("chld",child.getText().toString());
                go.putExtra("chkindate",checkInDate.getText().toString());
                go.putExtra("chkoutdate",checkOutDate.getText().toString());
                go.putExtra("chkintime",checkInTime.getText().toString());
                go.putExtra("chkouttime",checkOutTime.getText().toString());
                go.putExtra("wowP",wowPrice.getText().toString());
                go.putExtra("oldP",oldPrice.getText().toString());
                go.putExtra("offerId",offerId);
                startActivity(go);

            }
        });

//        facilities1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(HotelView.this);
//                builder.setMultiChoiceItems(extra_facilities,extra_facilities_selected,new DialogInterface.OnMultiChoiceClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
//
//                    }
//                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        dialog.dismiss();
//                    }
//                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                }).show();
//            }
//        });


        animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.blink);


        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);


//        dynamicToolbarColor();
        collapsingToolbarLayout.setContentScrimColor(getResources().getColor(R.color.primary));
        collapsingToolbarLayout.setStatusBarScrimColor(getResources().getColor(R.color.black));

        toolbarTextAppearance();





        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Fetching...");
        dialog.show();
        Log.e("dfda",city+offerId);
        MyApiEndpointInterface myApiEndpointInterface = MyApiEndpointInterface.Factory.getInstance(this);
        myApiEndpointInterface.displayofferdetails(city,offerId).enqueue(new Callback<offer_details>() {
            @Override
            public void onResponse(Call<offer_details> call, Response<offer_details> response) {
                Log.e("resp",response.code()+"");
                final offer_details offer = response.body();
                Log.e("offer_details",offer.toString());

                for(int i=0;i<offer.getHotelInfo().getNumberOfReviews()&&reviews.size()<=5;i++){
                    if(!offer.getHotelInfo().getReviews().get(i).getReview().matches(""))
                        reviews.add(new Review(offer.getHotelInfo().getReviews().get(i).getReview(),offer.getHotelInfo().getReviews().get(i).getOverallRating(),offer.getHotelInfo().getReviews().get(i).getCustomerName()));
                }
               // Log.e("reviews",reviews.get(0).getReview().toString());
                if(reviews.size()>0){
                    Log.e("reviews",reviews.size()+"");
                    myadapter.addAll(reviews);
                    recycler.setVisibility(View.VISIBLE);
                }
                collapsingToolbarLayout.setTitle(offer.getHotelInfo().getHotelName());
                hotelName.setText(offer.getHotelInfo().getHotelName());
                hotel_rate.setRating(offer.getHotelInfo().getStarHotel());
                address.setText(offer.getHotelInfo().getAddressLine1());

                if(offer.getHotelInfo().getNumberOfReviews()!=0)
                hotelLike.setText("\t"+offer.getHotelInfo().getUserRatings().getOverall()+"/5" +
                        "");

                address.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
                        String map = "http://maps.google.co.in/maps?q=" + offer.getHotelInfo().getAddress();
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                        startActivity(intent);
                    }
                });
                double discount2 = ((double) offer.getOfferInfo().getPriceMRP() - (double)offer.getOfferInfo().getWowPrice())/(double)offer.getOfferInfo().getPriceMRP();


                discount.setText("+"+(int)(discount2*100)+"%");

                checkinstring = offer.getOfferInfo().getCheckIn();
                checkoutstring = offer.getOfferInfo().getCheckOut();
                scheduledstring = offer.getOfferInfo().getScheduledAt();
                expirystring = offer.getOfferInfo().getExpiresAt();
                wowPrice.setText(offer.getOfferInfo().getWowPrice()+"");
                oldPrice.setText(offer.getOfferInfo().getPriceMRP()+"");

                parser.setTimeZone(TimeZone.getTimeZone("IST"));
                try {
                    checkin = parser.parse(checkinstring);
                    checkout = parser.parse(checkoutstring);
                    scheduled = parser.parse(scheduledstring);
                    expiry = parser.parse(expirystring);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                checkInDate.setText(parser2.format(checkin));
                checkOutDate.setText(parser2.format(checkout));
                checkInTime.setText(parser3.format(checkin));
                checkOutTime.setText(parser3.format(checkout));
                adult.setText(offer.getRoomInfo().getAdults()+"");
                child.setText(offer.getRoomInfo().getChildren()+"");

                if(offer.getRoomInfo().getFacilities().getHasWifi())
                    wifi.setColorFilter(Color.parseColor("#009688"));
                if(offer.getRoomInfo().getFacilities().getHasDining())
                    food.setColorFilter(Color.parseColor("#009688"));
                if(offer.getRoomInfo().getFacilities().getHasTv())
                    tv.setColorFilter(Color.parseColor("#009688"));
                if(offer.getRoomInfo().getFacilities().getHasGym())
                    gym.setColorFilter(Color.parseColor("#009688"));



                Log.e("expiry",offer.getOfferInfo().getExpiresAt());
                Log.e("expiry",expirystring);
                Log.e("expiry",expiry.toString());

                final int timereamaining = (int) (expiry.getTime()-System.currentTimeMillis());
                new CountDownTimer(timereamaining,1000){

                    @Override
                    public void onTick(long millisUntilFinished) {

                        int seconds = (int) (millisUntilFinished / 1000) % 60 ;
                        int minutes = (int) ((millisUntilFinished / (1000*60)) % 60);
                        String text = String.format("%02d:%02d",minutes,seconds);

                        timeLeft.startAnimation(animation);
                        timeLeft.setText(text);
                    }

                    @Override
                    public void onFinish() {
                        Intent intent = new Intent(getApplicationContext(),Home.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }.start();



                String policies="";
                for(int k=0;k<offer.getHotelInfo().getPolicies().size();k++)
                {
                    policies+= "-"+offer.getHotelInfo().getPolicies().get(k)+"\n";
                }
                hotel_policy.setText(policies);


                if(offer.getHotelInfo().getImagesS3().getName().size()==0) {
                    images.add(BitmapFactory.decodeResource(getResources(),R.drawable.hotel_pic));
                    Log.e("images klajd",images.toString());
                    mViewPagerHotel = (ViewPager)findViewById(R.id.pager_hotelPic);
                    mCustomPagerAdapter = new CustomPagerAdapter(getApplicationContext(),images);
                    mViewPagerHotel.setAdapter(mCustomPagerAdapter);
                    mCustomPagerAdapter.notifyDataSetChanged();
                    dialog.dismiss();
                }



                for(i =0 ;i<offer.getHotelInfo().getImagesS3().getName().size();i++)
                {
                    String url = "https://s3-us-west-2.amazonaws.com/wowimagesupload/resized/large/"+offer.getHotelInfo().getImagesS3().getName().get(i)+".jpg";
                    imageLoader.get(url, new ImageLoader.ImageListener() {
                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                            Log.e("res",response.toString());
                            if(response.getBitmap()!=null)
                                images.add(response.getBitmap());//                            if(isImmediate)

                            Log.e("images0",images.toString());


//                            mCustomPagerAdapter.notifyDataSetChanged();
                            if(images.size()==offer.getHotelInfo().getImagesS3().getName().size()) {
                                mViewPagerHotel = (ViewPager)findViewById(R.id.pager_hotelPic);

                                    mCustomPagerAdapter = new CustomPagerAdapter(getApplicationContext(),images);

                                Log.e("images",images.toString());
                                mViewPagerHotel.setAdapter(mCustomPagerAdapter);
//                                mCustomPagerAdapter.setimages(images);
                                mCustomPagerAdapter.notifyDataSetChanged();

                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.e("vol",error.toString());
                        }
                    });


                }
                if(offer.getRoomInfo().getImagesS3().getName().size()==0)
                    room_photos.setVisibility(View.GONE);
                for(int i=0;i<offer.getRoomInfo().getImagesS3().getName().size();i++)
                {
                    String url = "https://s3-us-west-2.amazonaws.com/wowimagesupload/resized/large/"+offer.getRoomInfo().getImagesS3().getName().get(i)+".jpg";
                    imageLoader.get(url, new ImageLoader.ImageListener() {
                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                            if(response.getBitmap()!=null) {
                                room_images.add(response.getBitmap());
                                final ImageView imageView = new ImageView(getApplicationContext());

                                imageView.setImageBitmap(response.getBitmap());
                                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                        180,
                                        180);
                                lp.setMargins(10,10,10,10);
                                imageView.setLayoutParams(lp);
//                                back = (Button)findViewById(R.id.back_view);
//                                cencel = (Button)findViewById(R.id.cencel_view);

                                imageView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
//                                        AlertDialog.Builder builder = new AlertDialog.Builder(HotelView.this);
//                                        AlertDialog dialog = builder.create();
//                                        LayoutInflater inflater = getLayoutInflater();
//                                        builder.setView(inflater.inflate(R.layout.hotel_image_custom_dialog,null)).
//                                                setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                                    @Override
//                                                    public void onClick(DialogInterface dialog, int which) {
//                                                        dialog.dismiss();
//                                                    }
//                                                });





                                        final Dialog dialog = new Dialog(HotelView.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                                        dialog.setTitle("Preview");
                                        dialog.setContentView(R.layout.hotel_image_custom_dialog);


                                        ImageView imageView1 = (ImageView)dialog.findViewById(R.id.dialog_image);
                                        imageView1.setImageBitmap(((BitmapDrawable)imageView.getDrawable()).getBitmap());
                                        Button back = (Button)dialog.findViewById(R.id.back_view);

                                        dialog.show();
                                        back.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                            }
                                        });

//                                        cencel.setOnClickListener(new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View v) {
//                                                dialog.dismiss();
//                                            }
//                                        });




                                    }
                                });

                                room_photos.addView(imageView);
                            }

                            Log.e("room_imaages",room_images.toString());
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {


                        }
                    });
                }

            }

            @Override
            public void onFailure(Call<offer_details> call, Throwable t) {
                Log.e("err",t.toString());
                dialog.dismiss();
            }
        });




    }

    //For back button in Appbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




//    private void dynamicToolbarColor() {
//
//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
//                R.drawable.hotel_pic);
//        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
//
//            @Override
//            public void onGenerated(Palette palette) {
//
//                collapsingToolbarLayout.setContentScrimColor(getResources().getColor(R.color.primary));
//                collapsingToolbarLayout.setStatusBarScrimColor(palette.getMutedColor(R.color.primary));
//            }
//        });
//    }

    private void toolbarTextAppearance() {
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
//        collapsingToolbarLayout.setex
    }


    //view pager
    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        ArrayList<Bitmap> img;

        public CustomPagerAdapter(Context context,ArrayList<Bitmap> images)  {
            img = new ArrayList<>();
            mContext = context;
            img.addAll(images);
//            img = new imageset().execute(images).get();
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return img.size();
        }


        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
            Log.e("img",img.toString());

            imageView.setImageBitmap(img.get(position));

            /////set on clicklistener here////////
            container.addView(itemView);

            return itemView;
        }



        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }


    }
    public class imageset extends AsyncTask<ArrayList<ImageLoader.ImageContainer>,Void,ArrayList<Bitmap>>{

        ArrayList<Bitmap> imgsbitmaps = new ArrayList<>();

        @Override
        protected ArrayList<Bitmap> doInBackground(ArrayList<ImageLoader.ImageContainer>... params) {
            for (int i=0;i<params[0].size();i++)
            {
                imgsbitmaps.add(params[0].get(i).getBitmap());
            }
            Log.e("imgbitmaps",imgsbitmaps.toString());
            return imgsbitmaps;
        }
    }






}
