package com.mydhees.wow.customer.myWish;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mydhees.wow.customer.R;

public class WishReplyDetails extends AppCompatActivity {
    TextView city,noRooms,adult,child,price,messages;
    EditText checkIn,checkInTime,checkOut,checkOutTime;
    Button disAgree,agree;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_reply_details);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        city = (TextView)findViewById(R.id.cityWishRD);
        noRooms = (TextView)findViewById(R.id.no_RoomsRD);
        adult = (TextView)findViewById(R.id.no_adultsRD);
        child = (TextView)findViewById(R.id.no_childrenRD);
        price = (TextView)findViewById(R.id.priceRD);
        messages = (TextView)findViewById(R.id.messageRD);

        checkIn = (EditText)findViewById(R.id.checkin_dateWishRD);
        checkInTime = (EditText)findViewById(R.id.checkin_timeWishRD);
        checkOut = (EditText)findViewById(R.id.checkout_dateWishRD);
        checkOutTime = (EditText)findViewById(R.id.checkin_timeWishRD);

        disAgree = (Button)findViewById(R.id.dis_RD);
        agree = (Button)findViewById(R.id.agree_RD);
    }
    //For back button in Appbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
